using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
public class GenericObjectPool<T> : MonoBehaviour where T : MonoBehaviour
{

    [SerializeField] private Transform inUseContainer;
    [SerializeField] private Transform storageContainer;
    public T prefab;
    public static GenericObjectPool<T> instance;
    public static ObjectPool<T> pool;
    [SerializeField]
    private bool isCollectionCheck;
    [SerializeField]
    private int currentAmount;
    [SerializeField]
    private int defaultMaxAmount;
    [SerializeField]
    private int flexibleMaxAmount;
    private void Awake()
    {
        instance = this;
        pool = new ObjectPool<T>(
            CreateObject,
            GetObject,
            ReleaseObject,
            DestroyObject,
            isCollectionCheck
            ,
            defaultMaxAmount
            ,
            flexibleMaxAmount
            );

    }
    public float GetPercentage()
    {
        int counter = 0;
        for (int i=0; i < inUseContainer.childCount;i++)
        {
            counter ++;
        }
        return (float)currentAmount / defaultMaxAmount;
    }
    T CreateObject()
    {

        if (inUseContainer != null)
        {
            var newObject = Instantiate(prefab, inUseContainer);
            newObject.gameObject.name = prefab.name + pool.CountActive.ToString();
            var newGenericObject = newObject.gameObject.GetComponent<T>();
            return newGenericObject;
        }
        else
        {
            Debug.LogError("ERROR: " + gameObject.name + " HAS NO CONTAINER");
            var newObject = Instantiate(prefab);
            newObject.gameObject.name = prefab.name + pool.CountActive.ToString(); 
            var newGenericObject = newObject.gameObject.GetComponent<T>();
            return newGenericObject;
        }

        //if (newGenericObject is PoolableObject)
        //{
        //    PoolableObject newPoolableObject = newGenericObject as PoolableObject;
        //    newPoolableObject.SetPool(this);
        //    return newPoolableObject;
        //}
        //else
        //{
        //    Debug.Log("ERROR - NON POOLABLE ");
        //    return null;
        //}


    }

    void GetObject(T p_desiredObject)
    {
        currentAmount++;
        if (inUseContainer != null)
        {
            
            p_desiredObject.transform.SetParent(inUseContainer, false);
        }
     
        //if (!isInactiveInContainer)
        //{

        //    p_desiredObject.gameObject.SetActive(true);
        //}
    }

    void ReleaseObject(T p_desiredObject)
    {
        Debug.Log("RELEASED");
        currentAmount--;
        if (storageContainer != null)
        {
            p_desiredObject.transform.SetParent(storageContainer,false);
            p_desiredObject.transform.position = storageContainer.position;
        }
        else if (inUseContainer != null)
        {
            
            p_desiredObject.transform.SetParent(inUseContainer, false);
            p_desiredObject.transform.position = inUseContainer.position;
        }


        //if (!isInactiveInContainer)
        //{
        //    p_desiredObject.gameObject.SetActive(false);
        //}


    }

    void DestroyObject(T p_desiredObject)
    {
        Destroy(p_desiredObject.gameObject);
    }
}
