using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PersistentManager;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/StageLevel", order = 1)]
public class StageLevelObject : ScriptableObject
{
    public LoaderType type = LoaderType.LoadSingle;
    public string levelName;
    public string levelDisplayName;
    public Sprite levelImage;
    
}
