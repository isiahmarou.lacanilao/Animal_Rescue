using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class IdlingData
{
    public IdlingType idlingType;
    //public float chance;
    public WeightedFloatDatas waitTimes;
    //MathHelpers.AddAll(potentialHoverTimes);
    //    MathHelpers.ChooseWeightedFloat(potentialHoverTimes);
}
[CreateAssetMenu(fileName = "New SO_PedestrianArchetype", menuName = "ScriptableObjects/Pedestrian Archetype", order = 1)]
public class SO_PedestrianArchetype : ScriptableObject
{
    public List<IdlingData> idlingOptions = new List<IdlingData>();
    [SerializeField] public int destinationStops;
    [SerializeField] public float minSpeed;
    [SerializeField] public float maxSpeed;

}
