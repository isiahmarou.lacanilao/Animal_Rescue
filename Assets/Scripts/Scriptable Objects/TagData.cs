using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

[CreateAssetMenu(fileName = "TagData", menuName = "ScriptableObjects/Data/TagData", order = 99)]
public class TagData : ScriptableObject
{
    [field: Header("Layers")]
    [field: SerializeField] public LayerMask Player_Layer { get; private set; }
    [field: SerializeField] public LayerMask TriggerDetector_Layer { get; private set; }
    [field: SerializeField] public LayerMask Platforms_Layer { get; private set; }
    [field: SerializeField] public LayerMask MovingPlatform_Layer { get; private set; }
    [field: SerializeField] public LayerMask Treats_Layer { get; private set; }
    
    [field: Header("Layers")]
    [field: TagField] [field: SerializeField] public string Player_Tag { get; private set; }
}
