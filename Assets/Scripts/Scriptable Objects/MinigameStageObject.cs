using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PersistentManager;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MinigameObject", order = 1)]
public class MinigameStageObject : ScriptableObject
{
    public LoaderType type = LoaderType.LoadSingle;
    public RawImage minigameVideo;

    [TextArea(15, 20)]
    public string minigameDescription;

    public string minigameName;
    public string minigameDisplayName;
}
