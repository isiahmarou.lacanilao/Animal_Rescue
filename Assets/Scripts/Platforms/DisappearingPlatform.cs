using System;
using System.Collections;
using UnityEngine;

namespace Platforms
{
    public class DisappearingPlatform : MonoBehaviour
    {
        [SerializeField] private float onCollisionDuration = 1;
        [SerializeField] private float disappearDuration = 1;
        [SerializeField] private GameObject platform;
        [SerializeField] private Collider2D triggerBound;
        
        private float collisionTimer;
        private bool isPlayerOnPlatform;

        private void OnEnable()
        {
            platform.SetActive(true);
            triggerBound.enabled = true;
        }

        private void Update()
        {
            if (!isPlayerOnPlatform) return;
            
            collisionTimer += Time.deltaTime;
            
            if (collisionTimer < onCollisionDuration) return;
            
            StartCoroutine(OnDisappear());
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.CompareTag("Player"))
            {
                isPlayerOnPlatform = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                isPlayerOnPlatform = false;
                collisionTimer = 0;
            }
        }

        private IEnumerator OnDisappear()
        {
            collisionTimer = 0; 
            platform.SetActive(false);
            triggerBound.enabled = false;

            yield return new WaitForSeconds(disappearDuration);
            
            platform.SetActive(true);
            triggerBound.enabled = true;
        }
    }
}
