using System;
using System.Collections;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

namespace Platforms
{
    public class WaypointFollower : MonoBehaviour
    {
        [SerializeField] private Transform[] waypoints;
        [SerializeField] private float speedDuration = 5;
        [SerializeField] private float waitTimeOnWaypoint = 2;
        [SerializeField] private Ease movementEase = Ease.Linear;
        [SerializeField] private LoopType loopType = LoopType.Yoyo;

        private Sequence sequence;
        private Vector3 defaultPos;

        private void Awake()
        {
            defaultPos = transform.position;
        }

        private void OnEnable()
        {
            transform.position = defaultPos;
            sequence = DOTween.Sequence();

            sequence.AppendInterval(waitTimeOnWaypoint);
            foreach (var t in waypoints)
            {
                sequence.Append(transform.DOMove(
                    t.position, 
                    speedDuration).SetEase(movementEase));
                sequence.AppendInterval(waitTimeOnWaypoint);
            }
            
            sequence.SetLoops(-1, loopType);
        }

        private void OnDisable()
        {
            sequence.Kill();
            sequence = null;
        }
    }
}
