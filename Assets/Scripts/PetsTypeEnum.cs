
[System.Serializable]
public enum PetType
{
    NULL = 0,
    Cat = 1,
    Dog = 2,
    Fish = 3,
    Seal = 4,
    Bird = 5,
}