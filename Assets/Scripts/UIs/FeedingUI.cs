using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
//using is = ImportantStuff;
public class FeedingUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler
{
    private RectTransform rt;
    [SerializeField] private Canvas canvas;
    //[SerializeField] GameObject mouth;
    [SerializeField]
    private GameObject foodBowl;
    private SpriteRenderer foodBowlSR;
    [SerializeField]
    private Sprite filledFoodBowl;
    [SerializeField]
    private Sprite emptyFoodBowl;
    // private FoodUI inifoodUI; //temp del later
    //script of the c$$anonymous$$ld to get the drag method
    private FoodUI foodUI;
    IEnumerator runningCoroutine;
    public Action<float> OnEatenEvent;
    private void Awake()
    {
        foodBowlSR = foodBowl.GetComponent<SpriteRenderer>();
        rt = GetComponent<RectTransform>();
    }
    //public void Selected()
    //{
    //    FoodUI foodUI = FoodPool.pool.Get();
    //    foodUI.Initialize(canvas,rt.anchoredPosition);

    //}
    public void Initialize()
    {
        gameObject.SetActive(true);
        //mouth.SetActive(true);
        foodBowlSR.sprite = emptyFoodBowl;
        foodBowl.SetActive(true);
    }
    public void Deinitialize()
    {
        gameObject.SetActive(false);
        //mouth.SetActive(false);
        //foodBowl.SetActive(false);
    }
    public void OnPointerDown(PointerEventData data)
    {
        print("mouse down");
        runningCoroutine = Instantiator();
        StartCoroutine(runningCoroutine);
    }

    //public void OnBeginDrag(PointerEventData data)
    //{

    //}
    public void OnDrag(PointerEventData data)
    {
        if (foodUI != null)
        {
            PointerEventData pointerData = data;
            Vector2 position;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                (RectTransform)canvas.transform,
                pointerData.position,
                canvas.worldCamera,
                out position);
        
            foodUI.OnDrag(canvas.transform.TransformPoint(position));
        }
    }
    public void OnEndDrag(PointerEventData data)
    {
  
        foodUI = null;
    }

    public void OnPointerUp(PointerEventData data)
    {
        if (foodUI != null)
        {
            foodUI.onEatenEvent -= OnEaten;
            FoodPool.pool.Release(foodUI);
            //if mouse Up is before 2 seconds, it will not instantiate
            if (runningCoroutine != null)
            {
                StopCoroutine(runningCoroutine);
            }
        }
    }
    IEnumerator Instantiator()
    {
        yield return new WaitForSeconds(0.25f);
   
        foodUI = FoodPool.pool.Get();
  
        foodUI.Initialize(rt.anchoredPosition);
        foodUI.onEatenEvent += OnEaten;
    }

    void OnEaten()
    {
        if (foodUI != null)
        {
            foodUI.onEatenEvent -= OnEaten;
            FoodPool.pool.Release(foodUI);
            foodUI = null;
            foodBowlSR.sprite = filledFoodBowl;
            OnEatenEvent.Invoke(1);
        }
    
      
    }
}



