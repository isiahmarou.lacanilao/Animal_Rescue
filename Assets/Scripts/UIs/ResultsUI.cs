using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Coffee.UIExtensions;
using HelperClass;
public class ResultsUI : MonoBehaviour
{
    [SerializeField] GameObject rootGameObject;
    [SerializeField] TMP_Text endScoreText;

    [SerializeField] GameObject retryButton;
    [SerializeField] GameObject nextButton;
    int scoreGoal;
    int resultsNextStar;
    List<StarUI> resultStars = new List<StarUI>();

    [SerializeField] TMP_Text[] resultStarGoals;
    [SerializeField] RectTransform[] resultStarSlots;

    [SerializeField] float scoreCounterDuration = 1f;

    [SerializeField] List<UIParticle> confettis = new List<UIParticle>();
    public void Hide()
    {

        rootGameObject.SetActive(false);
    }
    public void Show(bool p_isWin,int p_score, int p_scoreGoal)
    {
        scoreGoal = p_scoreGoal;
        for (int i = 0; i < resultStars.Count; i++)
        {
            StarUIPool.pool.Release(resultStars[i]);
        }
        resultStars.Clear();
        rootGameObject.SetActive(true);
        retryButton.SetActive(false);
        nextButton.SetActive(false);
        for (int i = 0; i < resultStarGoals.Length; i++)
        {
            int scoreForStar = ((i + 1) * (p_scoreGoal / 3));
            scoreForStar = MathHelpers.FlattenTheNumber(scoreForStar);
            resultStarGoals[i].text = scoreForStar.ToString();

        }
        resultsNextStar = p_scoreGoal / 3;
        resultsNextStar = MathHelpers.FlattenTheNumber(resultsNextStar);
        //Debug.Log("test");
        StartCoroutine(CountText(p_isWin,endScoreText, p_score, scoreCounterDuration));
    }

    IEnumerator CountText(bool p_isWin, TMP_Text p_modifyText, int p_newValue, float p_duration, string p_format = "", int p_Value = 0, int p_fps = 60)
    {
  
        WaitForSeconds wait = new WaitForSeconds(1f / p_fps);
        //Debug.Log("rrr " + 1f / p_fps);
        int previousValue = p_Value;
        int stepAmount;

        if (p_newValue - previousValue < 0)
        {
            stepAmount = Mathf.FloorToInt((p_newValue - previousValue) / (p_fps * p_duration));
        }
        else
        {
            stepAmount = Mathf.CeilToInt((p_newValue - previousValue) / (p_fps * p_duration));

        }

        //Animated Look where numbers roll like a slot machine for awhile
        int scoreForStarIndex = 0;
        if (previousValue < p_newValue)
        {
            //Back up counter

            while (previousValue < p_newValue)
            {
                //Debug.Log("zzz " + 1f / p_fps);
                previousValue += stepAmount;
                if (previousValue > p_newValue)
                {
                    previousValue = p_newValue;
                }
                CheckScore(p_modifyText,ref previousValue,ref resultsNextStar, ref scoreForStarIndex);

                yield return wait;
            }
        }
        else if (previousValue > p_newValue)
        {
            while (previousValue > p_newValue)
            {
                //Debug.Log("www");
                previousValue += stepAmount;
                if (previousValue < p_newValue)
                {
                    previousValue = p_newValue;
                }
                CheckScore(p_modifyText,ref previousValue, ref resultsNextStar,ref scoreForStarIndex, addScore:false);
                yield return wait;
            }
        }
        else if (previousValue == p_newValue)
        {
            endScoreText.text = previousValue.ToString(p_format);
        }
        //Debug.Log("qqq");
        yield return new WaitForSeconds(1f);
        //Debug.Log("nnn");
        //Go through each star ratings again for Falling Star FX Particles
        foreach (StarUI selectedStar in resultStars)
        {
            //Play Falling Star Particle FX
            StartCoroutine(selectedStar.Co_PlayFallingStarsFX());
        }

        //If passing
        if (resultStars.Count >= 1)
        {
            //backgroundImage.sprite = levelComplete;
            //levelText.text = "Level    Complete";
            //AudioManager.instance.playSound(4);

            //If perfect, do confetti
            if (resultStars.Count >= 3)
            {
                foreach (UIParticle selectedConfetti in confettis)
                {
                    selectedConfetti.Play();
                }
            }

        }


        else       //if fail
        {

            //levelText.text = "Level Failed";

            //resultDayText.text = "";
            //backgroundImage.sprite = failImage;
            //AudioManager.instance.playSound(5);

        }
        //Debug.Log("hhh");
        //Debug.Log(p_isWin);
        if (p_isWin)
        {
   
            nextButton.SetActive(true);
        }
        else
        {
            retryButton.SetActive(true);
        }

    }

    void CheckScore(TMP_Text p_modifyText, ref int previousValue, ref int resultsNextStar,ref int p_scoreForStarIndex, string p_format = "", bool addScore = true)
    {
 

        //Update Text to new Value
        p_modifyText.text = previousValue.ToString(p_format);
        //Calcualtes how many stars did the player get
        if (previousValue >= resultsNextStar)
        {
            //Makes sure that the star is within the minimum and maximum amount of stars that can be gained. (If it's more than maxStarAmount(5) stars, it'll become 5 stars)
            if (p_scoreForStarIndex < 3)
            {
                //Do UI UX Animation for that star
                StarUI newStarFill = CreateStarFill(resultStarSlots[p_scoreForStarIndex]);

                StartCoroutine(FitStarToSlot(newStarFill, resultStarSlots[p_scoreForStarIndex].sizeDelta));
                p_scoreForStarIndex++;
                //AudioManager.instance.playSound(7);
                if (addScore)
                {
                    resultsNextStar += scoreGoal / 3;
                    resultsNextStar = MathHelpers.FlattenTheNumber(resultsNextStar);

                }

            }
        }
    }
    IEnumerator FitStarToSlot(StarUI p_spawnedStarFill, Vector2 p_starSlotSize)
    {
        ////If spawned star's size is larger than the selected star slot's size, then shrink it and lessen transparency
        var pulseInSequence = DOTween.Sequence().Append(p_spawnedStarFill.imageRT.DOSizeDelta(p_starSlotSize, 1f));
        pulseInSequence.Join(p_spawnedStarFill.img.DOFade(1, 1f));
        pulseInSequence.Play();
        yield return pulseInSequence.WaitForCompletion();

    }

    StarUI CreateStarFill(RectTransform p_selectedStarSlot)
    {
        //Spawn new star
        StarUI spawnedStar = StarUIPool.pool.Get();
        spawnedStar.rootRT.rotation = p_selectedStarSlot.rotation;
       spawnedStar.rootRT.anchoredPosition = p_selectedStarSlot.anchoredPosition;
        resultStars.Add(spawnedStar);

        //Set the spawned star's size to 3x the selected star slot's size
        spawnedStar.imageRT.sizeDelta = p_selectedStarSlot.sizeDelta * 3f;

        //Set transparency/Opacity of new star
        var pulseInSequence = DOTween.Sequence().Append(spawnedStar.img.DOFade(1, 1f));
        pulseInSequence.Play();

        //Play particle fx
        StartCoroutine(spawnedStar.Co_PlayParticleFXs());
        
        return spawnedStar;
    }
}
