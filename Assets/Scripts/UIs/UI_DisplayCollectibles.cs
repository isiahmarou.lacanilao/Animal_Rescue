using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using PersistentManager;

public class UI_DisplayCollectibles : MonoBehaviour
{
    [SerializeField] private TextMeshPro collectibleCount;

    
    void Update()
    {
        collectibleCount.text = PlayerDataManager.Instance.TreatsAcquired.ToString();
    }
}
