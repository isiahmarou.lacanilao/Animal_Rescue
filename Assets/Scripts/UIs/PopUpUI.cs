using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System;


public class PopUpUI : MonoBehaviour
{
    RectTransform rootRT;
    TMP_Text scoreText;
    [SerializeField] float lifeTime = 0.35f;
    [SerializeField] float delay = 0.5f;
    [SerializeField] Image icon;
    [SerializeField] ScoreFloater scoreFloater;
    [SerializeField] RectTransform defaultScoreFloaterPositionRT;
    [SerializeField] TMP_Text text;

    [Header("Pop Up Shake Settings")]
    [SerializeField] float shakePositionDuration = 0.35f;
    [SerializeField] Vector3 shakePositionPower = new Vector3(15f, 5f);
    [SerializeField] int shakePositionVibrato = 10;
    [SerializeField] float shakePositionRandomRange = 10f;
    [SerializeField] bool shakePositionCanFade = true;

    public static Action<HitData, Vector2> OnPopUpEvent;
    List<Sequence> runningTweens = new List<Sequence>();
    private void Awake()
    {
        rootRT = GetComponent<RectTransform>();

        scoreText = scoreFloater.GetComponent<TMP_Text>();
        scoreFloater.popUpUI = this;

    }
    private void OnDestroy()
    {

    }
  
    public void PopIn(HitData p_hitData, Vector2 p_origin,ScoreUI p_scoreTarget = null)
    {

        for (int i = 0; i < runningTweens.Count; i++)
        {
            runningTweens[i].Kill();
        }
        float spawnY = UnityEngine.Random.Range(-50, 50);
        float spawnX = UnityEngine.Random.Range(-50, 50);
        rootRT.anchoredPosition = new Vector2(p_origin.x, p_origin.y);
        scoreFloater.scoreRT.SetParent(rootRT);
        scoreFloater.scoreRT.anchoredPosition = defaultScoreFloaterPositionRT.anchoredPosition;
     
        if (p_scoreTarget != null)
        {
            scoreFloater.transform.SetParent(transform);
            scoreFloater.Set(p_scoreTarget, p_hitData.baseScoreReward, p_scoreTarget.scoreTextRT.anchoredPosition);
        }


        text.text = p_hitData.hitType.ToString();
        text.color = new Color(0, 0, 0, 0);
        icon.color = new Color(0, 0, 0, 0);
        StartCoroutine(Co_PopUp(p_hitData));
        
    }


    IEnumerator Co_PopUp(HitData p_hitData)
    {
        var pulseOutSequence = DOTween.Sequence();
       
        pulseOutSequence.Append(text.DOBlendableColor(p_hitData.color, 1f));


        
        if (p_hitData.baseScoreReward!=0)
        {
     
            
            pulseOutSequence.Join(scoreText.DOBlendableColor(p_hitData.color, 1f));
            pulseOutSequence.Join(scoreText.DOFade(1, 1f));
        }
       
        icon.sprite = p_hitData.icon;

        pulseOutSequence.Join(icon.DOFade(1, 1f));
        pulseOutSequence.Join(text.DOFade(1, 1f));
 
        pulseOutSequence.Play();
        // yield return pulseOutSequence.WaitForCompletion();
        runningTweens.Add(pulseOutSequence);
        yield return new WaitForSeconds(0.15f);
        runningTweens.Remove(pulseOutSequence);
        var pulseOutSequencess = DOTween.Sequence();
        pulseOutSequencess.Append(rootRT.DOShakeAnchorPos(shakePositionDuration, shakePositionPower, shakePositionVibrato, shakePositionRandomRange, shakePositionCanFade));
        pulseOutSequencess.Play();
        runningTweens.Add(pulseOutSequencess);
        yield return pulseOutSequencess.WaitForCompletion();
        runningTweens.Remove(pulseOutSequencess);
        yield return new WaitForSeconds(lifeTime);
        var pulseOutSequences = DOTween.Sequence();
        pulseOutSequences.Append(text.DOFade(0, 1f));
        pulseOutSequences.Join(icon.DOFade(0, 1f));
        pulseOutSequences.Join(rootRT.DOAnchorPosY(rootRT.anchoredPosition.y-5f, 1f));
        //pulseOutSequences.Join(scoreText.DOFade(0, 1f));
      
        pulseOutSequences.Join(icon.DOFade(0, 1f));
      
        runningTweens.Add(pulseOutSequences);
        pulseOutSequences.Play();
    

        runningTweens.Remove(pulseOutSequences);
        if (p_hitData.baseScoreReward != 0)
        {
            yield return new WaitForSeconds(delay);
            scoreFloater.scoreRT.SetParent(rootRT);
            scoreFloater.StartCoroutine(scoreFloater.Homing());

        }
        else
        {
            yield return pulseOutSequences.WaitForCompletion();
        }
        //PopUpUIPool.pool.Release(this);

    }



}
