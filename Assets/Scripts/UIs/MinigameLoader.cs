using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

using PersistentManager;

public class MinigameLoader : MonoBehaviour
{
    [SerializeField] private MinigameStageObject minigameStage;

    [SerializeField] private RawImage videoObject;
    [SerializeField] private TMP_Text minigameName;
    [SerializeField] private TMP_Text minigameDescription;

    private void Start()
    {
        DisplayMinigame();
    }
    private void DisplayMinigame()
    {
        minigameName.text = minigameStage.minigameDisplayName;
        minigameDescription.text = minigameStage.minigameDescription;
    }

    public void PlayGame()
    {
        EventManager.GlobalEvents.OnLoadScene.Invoke(minigameStage.minigameName, minigameStage.type);
    }
}
