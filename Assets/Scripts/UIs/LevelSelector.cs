using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelSelector : MonoBehaviour
{
    [SerializeField] private List<StageLevelObject> levelList;
    [SerializeField] private Image levelDisplayImage;
    [SerializeField] private TMP_Text levelNameText;
    private int currentSelectedLevelIndex = 0;

    private void Start()
    {
        DisplayLevel();
    }

    private void DisplayLevel()
    {
        Debug.Log("Current Level: " + currentSelectedLevelIndex);
        levelNameText.text = levelList[currentSelectedLevelIndex].levelDisplayName;
        levelDisplayImage.sprite = levelList[currentSelectedLevelIndex].levelImage;
    }

    public void GoLevel()
    {
        EventManager.GlobalEvents.OnLoadScene.Invoke(levelList[currentSelectedLevelIndex].levelName, levelList[currentSelectedLevelIndex].type);
    }

    public void ShowNextLevel()
    {
        if (currentSelectedLevelIndex < levelList.Count  -1)
        {
            currentSelectedLevelIndex++;
            DisplayLevel();
        }
        else if (currentSelectedLevelIndex >= levelList.Count - 1)
        {
            currentSelectedLevelIndex = 0;
            DisplayLevel();
        }

        else return;
    }

    public void ShowPreviousLevel()
    {
        if (currentSelectedLevelIndex > 0)
        {
            currentSelectedLevelIndex--;
            DisplayLevel();
        }
        else if (currentSelectedLevelIndex == 0)
        {
            currentSelectedLevelIndex = levelList.Count - 1;
            DisplayLevel();
        }

        else return;
    }
}
