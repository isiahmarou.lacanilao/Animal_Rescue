using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

[RequireComponent(typeof(Slider))]
public class UI_SliderSoundControl : MonoBehaviour
{
    public AudioMixer mixer;
    [SerializeField] private string volumeName;
    [SerializeField] private Text volumeLabel;

    Slider slider
    {
        get { return GetComponent<Slider>(); }
    }

    void Start()
    {

        UpdateValueOnChange(slider.value);

        slider.onValueChanged.AddListener(delegate {

            UpdateValueOnChange(slider.value);
        });
    }

    public void UpdateValueOnChange(float value)
    {

        if (mixer != null)
            mixer.SetFloat(volumeName, Mathf.Log(value) * 20f);

        if (volumeLabel != null)
            volumeLabel.text = Mathf.Round(value * 100f).ToString();

    }
}
