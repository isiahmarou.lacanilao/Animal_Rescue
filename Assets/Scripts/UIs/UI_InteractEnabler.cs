using UnityEngine;
using EventManager;
using UnityEngine.SceneManagement;

public class UI_InteractEnabler : MonoBehaviour
{
    [SerializeField] private GameObject interact_BTN;

    private void Start()
    {
        SceneManager.activeSceneChanged += OnSceneChanged;
    }

    private void OnEnable()
    {
        GlobalEvents.OnPlayerNearInteractable.AddListener(ShowButton);
        GlobalEvents.OnPlayerInteracting.AddListener(OnInteracting);

    }

    private void OnDisable()
    {
        GlobalEvents.OnPlayerNearInteractable.RemoveListener(ShowButton);
        GlobalEvents.OnPlayerInteracting.RemoveListener(OnInteracting);
    }

    private void OnDestroy()
    {
        SceneManager.activeSceneChanged -= OnSceneChanged;
    }


    private void ShowButton(bool willShow, GameObject go) => interact_BTN.SetActive(willShow);
    private void OnInteracting(bool isInteracting) => ShowButton(!isInteracting, null);

    private void OnSceneChanged(Scene current, Scene next) => ShowButton(false, null);
}
