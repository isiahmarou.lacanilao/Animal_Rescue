using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using System;
using UnityEngine.UI;
public delegate bool IsWithinHealthCondition(int p_healthCeilingCondition, int p_healthFloorCondition);

[System.Serializable]
public class InstantColorData : UnityTransitionData
{

    [SerializeField] public Color32 color;
    public override void PerformTransition()
    {
        bar.color = color;

    }

}

[System.Serializable]
public class ColorTransitionData : DoTweenTransitionData
{
    [SerializeField] public Color32 amount;
    [SerializeField] public float transitionTime;
    public override Tween GetAndPerformTween()
    {
        Tween colorTransition = bar.DOColor(amount, transitionTime);
        return colorTransition;
    }

}
[System.Serializable]
public class FadeTransitionData : DoTweenTransitionData
{
    [SerializeField] public float amount;
    [SerializeField] public float transitionTime;
    public override Tween GetAndPerformTween()
    {
        Tween colorTransition = bar.DOFade(amount, transitionTime);
        return colorTransition;
    }
}

[System.Serializable]
public class FillTransitionData : DoTweenTransitionData
{
    [SerializeField] public float amount;
    [SerializeField] public float transitionTime;
    public override Tween GetAndPerformTween()
    {
        Tween colorTransition = bar.DOFillAmount(amount, transitionTime);
        return colorTransition;
    }
}

[System.Serializable]
public class DoTweenTransitionData : TransitionData
{
    [SerializeField] public bool joinToNextTransition;
    [SerializeField] public bool waitToFinish;
    [SerializeField] public float delayTimeToNextTransition;
    public virtual Tween GetAndPerformTween()
    {
        return null;
    }
}

[System.Serializable]
public class UnityTransitionData : TransitionData
{

    public override void PerformTransition()
    {

    }
}
[System.Serializable]
public enum TransitionType
{
    None,
    InstantColorData,
    FadeTransitionData,
    FillTransitionData,
    DoTweenTransitionData,
}
[System.Serializable]
public class TransitionData
{

    [SerializeField]
    public TransitionType transitionType;
    [SerializeField] public Image bar;


    public virtual void PerformTransition()
    {

    }
}

public enum HealthType
{
    good,
    moderate,
    bad
}
[System.Serializable]
public class HealthData
{
    public HealthType healthType;
    public Sprite icon;
    public Color color;
    public int maxRequirement;
    public int minRequirement;
}
public class HealthBarUI : MonoBehaviour
{
    public float currentHealth { get; private set; }
    [SerializeField] private float maxHealth = 100f;
    //public Action OnInitializeEvent;
    public delegate void InitializeEvent(float i = 1); // this 1 here can be any number. It does not seem to matter
                                                       //...
    public InitializeEvent OnInitializeEvent;
    public Action<int> OnModifyHealthEvent;
    public Action OnHealthDeathEvent;
    public Action OnSaveHealthEvent;
    public IsWithinHealthCondition OnIsWithinHealthConditionEvent;

    [SerializeField] public GameObject frame;
    [SerializeField] private Image realBarUI; //colored
    [SerializeField] private Image ghostBarUI; //white

    private bool isResetting = false;
    private bool isCurrentlyResettingCoroutine = false;
    [SerializeField] private float resetTransitionTime;

    [SerializeField] private List<HealthData> healthDatas = new List<HealthData>();

    [SerializeField]
    private Image healthIconImage;

    [SerializeField] private InstantColorData realBarColorFlash;
    [SerializeField] private List<ColorTransitionData> realBarColorTransitions = new List<ColorTransitionData>();

    [SerializeField] private Color32 fakeRealBarColor;
    Color32 defaultGhostBarColor;
    [SerializeField] private Color32 addGhostBarColor;
    [SerializeField] private Color32 subtractGhostBarColor;

    [SerializeField] private FadeTransitionData ghostBarFadeTransition;
    [SerializeField] private FillTransitionData ghostBarFillTransition;

    [SerializeField] private float delayTime = 0;
    public bool iconEnabled;
    private float current = 0;
    private float currentMax = 1;
    private float savedFill = 0;
    IEnumerator runningCoroutine;
    private void Awake()
    {

        OnInitializeEvent += OnInitialize;
        OnModifyHealthEvent += ModifyHealth;
        OnIsWithinHealthConditionEvent += IsWithinHealthCondition;

        realBarUI.fillAmount = 0f;
        ghostBarUI.fillAmount = 0f;
        defaultGhostBarColor = new Color(ghostBarUI.color.r, ghostBarUI.color.g, ghostBarUI.color.b, 1);
        currentHealth = maxHealth;
        InstantUpdateBar(currentHealth, maxHealth, maxHealth);
    }

    private void OnDestroy()
    {

        OnInitializeEvent -= OnInitialize;
        OnModifyHealthEvent -= ModifyHealth;
        OnIsWithinHealthConditionEvent -= IsWithinHealthCondition;

        StopAllCoroutines();
    }
    void OnInitialize(float p_maxHealth = 0)
    {

        maxHealth = maxHealth != 0 ? maxHealth : p_maxHealth;
        currentHealth = maxHealth;
        InstantUpdateBar(currentHealth, maxHealth, maxHealth);
        Close();
    }



    void SaveHealth()
    {


    }
    public void Open()
    {
        frame.SetActive(false);
    }

    public void Close()
    {
        frame.SetActive(true);
    }
    void ModifyHealth(int p_modifier)
    {

        currentHealth += p_modifier;
        SaveHealth();
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        else if (currentHealth <= 0)
        {
            currentHealth = 0;
            OnHealthDeathEvent?.Invoke();

        }

        if (gameObject.activeSelf && frame.activeSelf)
        {
            UpdateBar(currentHealth, maxHealth);
        }

    }

    bool IsWithinHealthCondition(int p_healthCeilingCondition, int p_healthFloorCondition)
    {
        if (currentHealth <= p_healthCeilingCondition &&
            currentHealth >= p_healthFloorCondition)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void InstantUpdateBar(float p_current = 0, float p_currentMax = 1, float p_max = 1)
    {
        ghostBarUI.color = fakeRealBarColor;
        StopAllCoroutines();
        isResetting = false;

        current = p_current;
        currentMax = p_max;
        float fill = p_current / p_max;
        savedFill = fill;

        if (realBarUI)
        {
            realBarUI.DOFillAmount(savedFill, 0.01f);
            HealthData healthData = healthDatas.Where(currentHealthData =>
                currentHealth <= currentHealthData.maxRequirement &&
                currentHealth > currentHealthData.minRequirement).FirstOrDefault() ?? healthDatas[healthDatas.Count - 1];
            realBarUI.color = healthData.color;
            if (iconEnabled)
            {
                healthIconImage.sprite = healthData.icon;
            }

        }

        if (ghostBarUI)
        {
            ghostBarUI.DOFillAmount(savedFill, 0.01f);
            ghostBarUI.color = new Color(ghostBarUI.color.r, ghostBarUI.color.g, ghostBarUI.color.b, 0);
        }


    }


    public void UpdateBar(float p_current = 0, float p_currentMax = 1)
    {

        current = p_current;
        currentMax = p_currentMax;

        float fill = current / currentMax;

        if (enabled)
        {
            if (runningCoroutine != null)
            {
                StopCoroutine(runningCoroutine);
                runningCoroutine = null;
            }
        }

        if (gameObject.activeSelf != false)
        {
            StartCoroutine(Co_UpdateBar(fill));
        }
    }
    IEnumerator Co_UpdateBar(float p_fill = 0)
    {
        bool add = false;
        Image currentBar;
        Image currentGhostBar;
        HealthData healthDataFound = healthDatas.Where(currentHealthData =>
            currentHealth <= currentHealthData.maxRequirement &&
            currentHealth > currentHealthData.minRequirement).FirstOrDefault() ?? healthDatas[healthDatas.Count - 1];
 
        if (savedFill > p_fill) //Decreased, go real bar go first
        {
            if (realBarColorFlash != null)
            {

                realBarUI.color = realBarColorFlash.color;
            }
            yield return new WaitForSeconds(0.1f);

            if (realBarColorFlash != null)
            {

                realBarUI.color = healthDataFound.color;
                if (iconEnabled)
                {
                    healthIconImage.sprite = healthDataFound.icon;
                }

            }
            yield return new WaitForSeconds(0.1f);
            if (realBarColorFlash != null)
            {
                realBarUI.color = realBarColorFlash.color;
            }
            yield return new WaitForSeconds(0.1f);
      
            yield return new WaitForSeconds(0.1f);
            if (realBarColorFlash != null)
            {
                realBarUI.color = healthDataFound.color;
                if (iconEnabled)
                {
                    healthIconImage.sprite = healthDataFound.icon;
                }
              
            }

            currentBar = realBarUI;
            currentGhostBar = ghostBarUI;



            ghostBarUI.color = subtractGhostBarColor;


        }
        else //Increase, fake bar go first
        {
            currentBar = ghostBarUI;
            currentGhostBar = realBarUI;
            ghostBarUI.color = addGhostBarColor;
            add = true;

        }
        currentBar.fillAmount = p_fill;


        yield return new WaitForSeconds(delayTime);

        Sequence s = DOTween.Sequence();
        if (ghostBarFadeTransition != null)
        {
            float secondaryBarFadeAmount = 0;
            if (ghostBarFadeTransition.amount < 0)
            {
                secondaryBarFadeAmount = p_fill;
            }
            else if (ghostBarFadeTransition.amount > 0)
            {
                secondaryBarFadeAmount = ghostBarFillTransition.amount;
            }

            if (ghostBarFadeTransition.amount < 900)
            {
                if (!add)
                {
                    s.Join(currentGhostBar.DOFade(secondaryBarFadeAmount, ghostBarFadeTransition.transitionTime));
                }

            }

        }
        if (ghostBarFillTransition != null)
        {
            if (ghostBarFillTransition.amount <= 0)
            {
                savedFill = p_fill;


            }
            else if (ghostBarFillTransition.amount > 0)
            {
                if (ghostBarFillTransition.amount < -1)
                {
                    savedFill = 0;
                }
                else
                {
                    savedFill = ghostBarFillTransition.amount;
                }

            }
            if (p_fill < -1)
            {

                p_fill = 0;

            }
            s.Join(currentGhostBar.DOFillAmount(p_fill, ghostBarFillTransition.transitionTime));

        }

        s.Play();
        yield return s.WaitForCompletion();

        ghostBarUI.color = fakeRealBarColor;

        realBarUI.color = healthDataFound.color;
        if (iconEnabled)
        {
            healthIconImage.sprite = healthDataFound.icon;
        }
      

    }
}
