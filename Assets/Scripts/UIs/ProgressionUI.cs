using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using System;

public class ProgressionUI : MonoBehaviour
{

    private RectTransform rt;
    [SerializeField] Image bar;
    [SerializeField] TMP_Text timeText;
    //[SerializeField] IconUI prefab;
    List<IconUI> iconUIs = new List<IconUI>();
    [SerializeField] RectTransform personIconRT;
    [SerializeField] float maxTime;
    float time;
    public float currentSteps { get; private set; }
    public float percent;
    [SerializeField] float maxSteps;

    public Action OnReachedGoal;

    IEnumerator runningCoroutine;

    bool canUse = true;
    bool next = false;
    void Awake()
    {
        rt =GetComponent<RectTransform>();
    }
    void ResetProgress()
    {
        time = maxTime;
        bar.fillAmount = 0;
        personIconRT.anchoredPosition = new Vector2(9.5f, personIconRT.anchoredPosition.y);
        currentSteps = 0;
        canUse = true;
        next = false;
    }
    public void Finished()
    {
        canUse = false;
    

    }
    public void InitializeStats(int p_maxSteps, int p_maxSegment ,List<Sprite> p_catImage, float p_maxTime = 0)
    {
        ResetProgress();
        maxSteps = p_maxSteps;
        float iconX = (rt.sizeDelta.x) / p_maxSegment; 
        if (p_maxTime != 0)
        {
            time = p_maxTime;
        }
        for (int i = 0; i < iconUIs.Count; i++)
        {
            IconUIPool.pool.Release(iconUIs[i]);
        }

        for (int i = 0; i < p_maxSegment; i++)
        {

            IconUI newInstance = IconUIPool.pool.Get();
            newInstance.icon.sprite = p_catImage[i];
            Debug.Log(rt.sizeDelta.x + " / " + p_maxSteps + " = " + iconX + " * " + i);
            newInstance.rt.anchoredPosition = new Vector2(iconX * (i + 1) - 11f, personIconRT.anchoredPosition.y);
        }
        runningCoroutine = Co_Countdown();
        StartCoroutine(runningCoroutine);
    }
    public void InitializeStats(int p_maxSteps, List<Sprite> p_catImage, float p_maxTime = 0)
    {
        ResetProgress();
        maxSteps = p_maxSteps;
        float iconX = (rt.sizeDelta.x) / p_maxSteps;
        if (p_maxTime != 0)
        {
            time = p_maxTime;
        }
        for (int i = 0; i < iconUIs.Count; i++)
        {
            IconUIPool.pool.Release(iconUIs[i]);
        }
           
        for (int i = 0; i < 3; i++)
        {
          
            IconUI newInstance = IconUIPool.pool.Get();
            newInstance.icon.sprite = p_catImage[i];
            Debug.Log(rt.sizeDelta.x + " / " + p_maxSteps  + " = " + iconX + " * " + i);
            newInstance.rt.anchoredPosition = new Vector2(iconX * (i + 1)-11f, personIconRT.anchoredPosition.y);
        }
        runningCoroutine = Co_Countdown();
        StartCoroutine(runningCoroutine);
    }

    public void InitializeStats(Sprite p_catImage, float p_maxTime = 0)
    {
        ResetProgress();
        maxSteps = 1;
        float iconX = (rt.sizeDelta.x)/ 1;
        if (p_maxTime != 0)
        {
            time = p_maxTime;
        }
        for (int i = 0; i < iconUIs.Count; i++)
        {
            IconUIPool.pool.Release(iconUIs[i]);
        }

       

        IconUI newInstance = IconUIPool.pool.Get();
        newInstance.icon.sprite = p_catImage;
        newInstance.rt.anchoredPosition = new Vector2(iconX - 11f, personIconRT.anchoredPosition.y);
        
        runningCoroutine = Co_Countdown();
        StartCoroutine(runningCoroutine);
    }
    public void UpdateProgress(float p_prog)
    {
        if (currentSteps < maxSteps)
        {
            currentSteps = p_prog;
            if (currentSteps >= maxSteps)
            {

                if (canUse)
                {
                    canUse = false;
                    next = true;
                }
            }
        }
     
        StartCoroutine(Co_Progress());
    }
    public void ModifyProgress(float p_prog)
    {
        if (currentSteps < maxSteps)
        {
            currentSteps += p_prog;
            if (currentSteps >= maxSteps)
            {

                if (canUse)
                {
                    canUse = false;
                    next = true;
                }
            }
        }
      
        StartCoroutine(Co_Progress());
    }
    public void Progress()
    {
        if (currentSteps < maxSteps)
        {
            currentSteps++;
            if (currentSteps >= maxSteps)
            {

                if (canUse)
                {
                    canUse = false;
                    next = true;
                }
            }
        }
      
        StartCoroutine(Co_Progress());
    }
    public void StopTime()
    {
        if (runningCoroutine != null)
        {
            StopCoroutine(runningCoroutine);
        }
    }
    IEnumerator Co_Progress()
    {

       
            percent = currentSteps / maxSteps;
        var pulseInNOutSequence = DOTween.Sequence();
        pulseInNOutSequence.Append(bar.DOFillAmount((float)currentSteps / (float)maxSteps, 1f));
        Debug.Log((rt.sizeDelta.x) + " = " + (float)currentSteps / (float)maxSteps);
        pulseInNOutSequence.Join(personIconRT.DOAnchorPosX(((rt.sizeDelta.x) * (float)currentSteps / (float)maxSteps), 1f));
        yield return new WaitForSeconds(0.1f);

        if (next && !canUse)
        {
            Debug.Log("wtep over");
                OnReachedGoal.Invoke();
               
            
         
        }
    }

    IEnumerator Co_Countdown()
    {
        if (canUse)
        {
            percent = currentSteps / maxSteps;
            yield return new WaitForSeconds(1);
            time--;

            Vector3 OriginalScale = timeText.GetComponent<RectTransform>().localScale;
            Vector3 modifiedScale;
            float sizeTweenSpeed;
            float colorTweenSpeed;
            if (time <= 10)
            {
                modifiedScale = new Vector3(OriginalScale.x + 0.5f, OriginalScale.y + 0.5f, OriginalScale.z + 0.5f);
                sizeTweenSpeed = 0.2f;
                colorTweenSpeed = 2f;
                timeText.DOColor(Color.white, colorTweenSpeed).From(Color.red);

            }
            else
            {
                modifiedScale = new Vector3(OriginalScale.x + 0.25f, OriginalScale.y + 0.25f, OriginalScale.z + 0.25f);
                sizeTweenSpeed = 0.25f;

            }
            timeText.GetComponent<RectTransform>().DORewind();
            var pulseInNOutSequence = DOTween.Sequence();
            pulseInNOutSequence.Append(timeText.GetComponent<RectTransform>().DOScale(modifiedScale, sizeTweenSpeed).SetEase(Ease.Linear));
            pulseInNOutSequence.Append(timeText.GetComponent<RectTransform>().DOScale(OriginalScale, 0.2f).SetEase(Ease.Linear));
            pulseInNOutSequence.Play();
            timeText.text = time.ToString();

            if (time > 0)
            {
                runningCoroutine = Co_Countdown();
                StartCoroutine(runningCoroutine);
            }
            else if (time == 0)
            {
                Debug.Log("time over");
                runningCoroutine = null;
                OnReachedGoal.Invoke();
            }
        }
       
    }
}
