using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System;
using System.Linq;
using HelperClass;


public enum HitType
{
    perfect,
    great,
    good,
    miss
}
[System.Serializable]
public class HitData
{
    public HitType hitType;
    public Color color;
    public Sprite icon;
    public int baseScoreReward;
    public int requiredBeatSize;
}

[System.Serializable]
public class WeightedFloatDatas
{
    [HideInInspector]public float maxChance;
    [SerializeField] public WeightedFloatData[] weightedFloat;

}

[System.Serializable]
public class WeightedFloatData
{
    public float value;
    public float chance;

}
public class Beat : MonoBehaviour
{
    [SerializeField] RectTransform spawnBox;
    RectTransform scriptRT;
    [SerializeField] GameObject root;
    RectTransform rootRT;
    UnityEngine.UI.Button beatButton;
    UnityEngine.UI.Image pulsingHaloImage;
    [SerializeField] RectTransform pulsingHaloRT;
    UnityEngine.UI.Image pulsingRadialImage;
    [SerializeField] RectTransform pulsingRadialRT;
    float lastTargetSize;
    Vector2 defaultSize;
    [SerializeField] RectTransform targetRT;
    float hitSize;
    float accuracySizeOffset;
    [SerializeField] WeightedFloatDatas potentialPulseTimes;
    [SerializeField] WeightedFloatDatas potentialHoverTimes;
    [SerializeField] WeightedFloatDatas potentialTargetSizes;


    List<UnityEngine.UI.Image> images = new List<UnityEngine.UI.Image>();

    IEnumerator runningCoroutine;

    List<Sequence> runningTweens = new List<Sequence>();
    public Action OnCorrectEvent;

    //public static event Action<int> Hit;
    bool hit = false;
    void Awake()
    {
    
        //rootRT = GetComponent<RectTransform>();
        pulsingHaloImage = pulsingHaloRT.GetComponent<UnityEngine.UI.Image>();
        pulsingRadialImage = pulsingRadialRT.GetComponent<UnityEngine.UI.Image>();
        beatButton = GetComponent<Button>();
        for (int i = 0; i < root.transform.childCount; i++)
        {
            if (root.transform.GetChild(i).TryGetComponent<UnityEngine.UI.Image>(out UnityEngine.UI.Image found))
            {
                images.Add(found);
            }
        }
        scriptRT = GetComponent<RectTransform>();
        hitSize = targetRT.sizeDelta.x;
        potentialPulseTimes.maxChance = MathHelpers.AddAll(potentialPulseTimes);
        potentialHoverTimes.maxChance = MathHelpers. AddAll(potentialHoverTimes);
        potentialTargetSizes.maxChance = MathHelpers.AddAll(potentialTargetSizes);
    }
    //private Vector2 WorldToCanvasPosition(RectTransform canvas, Vector3 position, Camera camera = null)
    //{
    //    if (camera == null)
    //    {
    //        camera = Camera.main;
    //    }
  

    //    var viewport_position = camera.WorldToViewportPoint(position);
    //    //canvas.localScale = new Vector3(1, 1, 1);
    //    return new Vector2((viewport_position.x * canvas.sizeDelta.x) - (canvas.sizeDelta.x * 0.5f),
    //                       (viewport_position.y * canvas.sizeDelta.y) - (canvas.sizeDelta.y * 0.5f));
    //}
    public void Initialize(float p_hitSize, Vector3 p_pos, float p_accuracySizeOffset)
    {
        //Debug.Log("iNITIALIZED");
        float accuracySizeOffset = p_accuracySizeOffset;
        scriptRT.anchoredPosition = p_pos;// WorldToCanvasPosition(scriptRT.parent.GetComponent<RectTransform>(),p_pos, Camera.main);// WorldToCanvasPosition(scriptRT,Camera.main, anchoredPosition);
        hitSize = p_hitSize;
       
        StartCoroutine(Co_ShowAll());
    }
    public IEnumerator Co_HideAll()
    {
        beatButton.interactable = false;
        if (runningCoroutine != null)
        {

            StopCoroutine(runningCoroutine);
            runningCoroutine = null;
        }
    
        for (int i=0; i < runningTweens.Count;i++)
        {
            runningTweens[i].Kill();
        }
        runningTweens.Clear();
        Sequence pulseInSequence = DOTween.Sequence();

        for (int i = 0; i < images.Count; i++)
        {
            pulseInSequence.Join(images[i].DOFade(0, 0.25f));
        }
        pulseInSequence.Play();
        yield return pulseInSequence.WaitForCompletion();
  
    }
    public IEnumerator Co_ShowAll()
    {
        ResetPulse();
        Sequence pulseInSequence = DOTween.Sequence();

        for (int i = 0; i < images.Count; i++)
        {
            if (images[i] != pulsingHaloImage || images[i] != pulsingRadialImage)
            {
                pulseInSequence.Join(images[i].DOFade(1, 0.25f));
            }
            else
            {
                if (images[i] == pulsingHaloImage)
                {
                    pulseInSequence.Join(images[i].DOFade(0.5f, 0.25f));
                }

            }

        }
        pulseInSequence.Play();
        yield return pulseInSequence.WaitForCompletion();
        runningCoroutine = Co_Pulse();
        StartCoroutine(runningCoroutine);
        beatButton.interactable = true;

    }


    void ResetPulse()
    {
        hit = false;
        //hitSize = UniversalLibrary.ChooseWeightedFloat(potentialStartingSizes); //Randomizing Target Size
        targetRT.sizeDelta = new Vector2(hitSize, hitSize);
        defaultSize = targetRT.sizeDelta + new Vector2(100, 100);
        pulsingHaloRT.sizeDelta = defaultSize;

        pulsingRadialRT.sizeDelta = new Vector2(hitSize,hitSize);
      
    }

    public void OnButtonTapped()
    {

        Debug.Log(scriptRT.anchoredPosition);
        StartCoroutine(Co_HideAll());
        if (pulsingHaloRT.sizeDelta.x <= hitSize + accuracySizeOffset)
        {
            hit = true;
            if (runningCoroutine != null)
            {

                StopCoroutine(runningCoroutine);
                runningCoroutine = null;
            }

            StartCoroutine(TemporaryCatMinigameManager.OnCorrectEvent(scriptRT.anchoredPosition,Vector3.Distance(pulsingHaloRT.sizeDelta, new Vector2(hitSize, hitSize))));
        }
        else
        {
            //HitData currentHitData = hitDatas.Where(currentHitData => currentHitData.hitType == HitType.miss).First();
            StartCoroutine(TemporaryCatMinigameManager.OnMistakeEvent(scriptRT.anchoredPosition));
            //TemporaryCatMinigameManager.OnMistakeEvent.Invoke(scriptRT.anchoredPosition);

        }
    }

    IEnumerator Co_Pulse()
    {

        float chosenTargetSize = MathHelpers.ChooseWeightedFloat(potentialTargetSizes, lastTargetSize); //Randomizing Target Size
        float chosenPulseSpeed = MathHelpers.ChooseWeightedFloat(potentialPulseTimes); //Randomizing Pulse Time
        float chosenHoverTime = MathHelpers.ChooseWeightedFloat(potentialHoverTimes); //Randomizing Hover Time
        lastTargetSize = chosenTargetSize;
        Vector2 hitSizeVector = new Vector2(hitSize, hitSize);
        Vector2 chosenTargetSizeVector;

      
        float newPos = Mathf.Clamp(chosenTargetSize + hitSize, hitSize, hitSize+25);
 
        chosenTargetSizeVector = new Vector2(newPos, newPos); 
        
        //Halo Pulse In
        Vector2 targetPos;
        targetPos = new Vector2(chosenTargetSize, chosenTargetSize);
        float time = MathHelpers.ConvertSpeedToTime(pulsingHaloRT.sizeDelta, targetPos, chosenPulseSpeed);

        var pulseInSequence = DOTween.Sequence().Append(pulsingHaloRT.DOSizeDelta(targetPos, time));
        pulseInSequence.Join(pulsingHaloImage.DOFade(1, time));

        //Radial Pulse Out
        //if (newRadialSize >= 0)
        //{
            //time = ConvertSpeedToTime(pulsingRadialRT.sizeDelta, chosenTargetSizeVector, chosenPulseSpeed);
            //pulseInSequence.Join(pulsingRadialRT.DOSizeDelta(chosenTargetSizeVector, time));
            //pulseInSequence.Join(pulsingRadialImage.DOFade(0.5f, time));
        //}
        pulseInSequence.Play();
        runningTweens.Add(pulseInSequence);

        yield return pulseInSequence.WaitForCompletion();
        runningTweens.Remove(pulseInSequence);

        //Hover

        chosenHoverTime /= 2;
        //Hover In
        targetPos = new Vector2(pulsingHaloRT.sizeDelta.x + 25, pulsingHaloRT.sizeDelta.y + 25);
        var pulseInNOutSequence = DOTween.Sequence();
        pulseInNOutSequence.Append(pulsingHaloRT.DOSizeDelta(targetPos, chosenHoverTime));
        targetPos = new Vector2(pulsingHaloRT.sizeDelta.x - 25, pulsingHaloRT.sizeDelta.y - 25);
        pulseInNOutSequence.Append(pulsingHaloRT.DOSizeDelta(targetPos, chosenHoverTime));
        //targetPos = chosenTargetSizeVector - new Vector2(25, 25);
        //var pulseInNOutSequence = DOTween.Sequence().Append(pulsingRadialRT.DOSizeDelta(targetPos, chosenHoverTime));
        // pulseInNOutSequence.Join(pulsingRadialImage.DOFade(0.35f, chosenHoverTime));
        //targetPos = new Vector2(pulsingHaloRT.sizeDelta.x + 25, pulsingHaloRT.sizeDelta.y + 25);
        //pulseInNOutSequence.Join(pulsingHaloRT.DOSizeDelta(targetPos, chosenHoverTime));
        //Hover Out
        //pulseInNOutSequence.Append(pulsingRadialRT.DOSizeDelta(chosenTargetSizeVector, chosenHoverTime));
        //pulseInNOutSequence.Join(pulsingRadialImage.DOFade(0.5f, chosenHoverTime));
        //targetPos = new Vector2(pulsingHaloRT.sizeDelta.x - 25, pulsingHaloRT.sizeDelta.y - 25);
        //pulseInNOutSequence.Join(pulsingHaloRT.DOSizeDelta(targetPos, chosenHoverTime));
        pulseInNOutSequence.Play();
        runningTweens.Add(pulseInNOutSequence);


        chosenHoverTime *= 2;
        yield return new WaitForSeconds(chosenHoverTime);
        //runningTweens.Remove(pulseInNOutSequence);
        //Halo Pulse Out
        time = MathHelpers.ConvertSpeedToTime(pulsingHaloRT.sizeDelta, defaultSize, chosenPulseSpeed);
        var pulseOutSequence = DOTween.Sequence()
            .Append(pulsingHaloRT.DOSizeDelta(defaultSize, time));
        pulseOutSequence.Join(pulsingHaloImage.DOFade(0.5f, time));

        //Radial Pulse In
        //if (newRadialSize >= 0)
        //{
            //targetPos = new Vector2(hitSize, hitSize);
            //time = ConvertSpeedToTime(pulsingRadialRT.sizeDelta, chosenTargetSizeVector, chosenPulseSpeed);
            //pulseOutSequence.Join(pulsingRadialRT.DOSizeDelta(chosenTargetSizeVector, time));
            //pulseOutSequence.Join(pulsingRadialImage.DOFade(0f, time));
        //}
        pulseOutSequence.Play();
        runningTweens.Add(pulseOutSequence);
        yield return pulseOutSequence.WaitForCompletion();
        runningTweens.Remove(pulseOutSequence);

     
        if (!hit)
        {
            DOTween.Kill(this);
            runningCoroutine = Co_Pulse();
            StartCoroutine(runningCoroutine);
        }

    }
}
