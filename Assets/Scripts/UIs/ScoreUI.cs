using DG.Tweening;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using HelperClass;
public class ScoreUI : MonoBehaviour
{

    public int currentPoints { get; private set; }
    [SerializeField] TMP_Text scoreText;
    public RectTransform scoreTextRT { get; private set; }
    [SerializeField] Color color;
    Color defaultColor;
    [SerializeField] int fpsCount = 60;
    [SerializeField] float duration = 1f;
    [SerializeField] string scoreFormat = "N0";
    void Awake()
    {
        defaultColor = scoreText.color;
        scoreTextRT = GetComponent<RectTransform>();
    }

    public void ResetScore()
    {
        currentPoints = 0;
        scoreText.color = defaultColor;
        scoreText.text = 0.ToString();
    }
    public void ModifyScore(int p_newValue)
    {
        currentPoints += p_newValue;
        scoreText.color = defaultColor;
        StartCoroutine(Co_Modified(scoreTextRT, p_newValue));
    }

    IEnumerator Co_Modified(RectTransform p_textRT, int p_newValue)
    {
        scoreText.DOBlendableColor(color,0.5f);
        StartCoroutine(UIHelpers.Co_BouncingExpandTextEffect(p_textRT));
        yield return StartCoroutine(UIHelpers.Co_SlotMachineTextEffect(scoreText, currentPoints, duration, scoreFormat, currentPoints - p_newValue, fpsCount));
        scoreText.DOBlendableColor(defaultColor, 0.5f);
       
    }
   
}
