using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using TMPro;

public class HealthUI : MonoBehaviour
{

    public float currentHealth { get; private set; }
    [SerializeField] private float maxHealth = 100f;
    //public Action OnInitializeEvent;
    public delegate void InitializeEvent(float i = 1); // this 1 here can be any number. It does not seem to matter
                                           //...
    public InitializeEvent OnInitializeEvent;
    public Action<int> OnModifyHealthEvent;
    public Action OnHealthDeathEvent;
    public Action OnSaveHealthEvent;
    public IsWithinHealthCondition OnIsWithinHealthConditionEvent;

    [SerializeField] public GameObject frame;


    private bool isResetting = false;
    private bool isCurrentlyResettingCoroutine = false;
    [SerializeField] private float resetTransitionTime;

    public TMP_Text text;
    [SerializeField] private float delayTime = 0;
    public bool iconEnabled;
    private float current = 0;
    private float currentMax = 1;
    private float savedFill = 0;
    IEnumerator runningCoroutine;
    private void Awake()
    {

        OnInitializeEvent += OnInitialize;
        OnModifyHealthEvent += ModifyHealth;
        OnIsWithinHealthConditionEvent += IsWithinHealthCondition;

   
        currentHealth = maxHealth;
        text.text = currentHealth.ToString();
        //InstantUpdateBar(currentHealth, maxHealth, maxHealth);
    }

    private void OnDestroy()
    {
  
        OnInitializeEvent -= OnInitialize;
        OnModifyHealthEvent -= ModifyHealth;
        OnIsWithinHealthConditionEvent -= IsWithinHealthCondition;
  
        StopAllCoroutines();
    }
    void OnInitialize(float p_maxHealth = 0)
    {

        maxHealth = maxHealth != 0 ? maxHealth : p_maxHealth;
        currentHealth = maxHealth;
        text.text = currentHealth.ToString();
        //InstantUpdateBar(currentHealth, maxHealth, maxHealth);
        Close();
    }
 

  
    void SaveHealth()
    {


    }
    public void Open()
    {
        frame.SetActive(false);
    }

    public void Close()
    {
        frame.SetActive(true);
    }
    void ModifyHealth(int p_modifier)
    {

        currentHealth += p_modifier;
        SaveHealth();
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        else if (currentHealth <= 0)
        {
            currentHealth = 0;
            OnHealthDeathEvent?.Invoke();

        }
     
        if (gameObject.activeSelf && frame.activeSelf)
        {
            text.text = currentHealth.ToString();
            //UpdateBar(currentHealth, maxHealth);
        }
      
    }

    bool IsWithinHealthCondition(int p_healthCeilingCondition, int p_healthFloorCondition)
    {
        if (currentHealth <= p_healthCeilingCondition &&
            currentHealth >= p_healthFloorCondition)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

   


}
