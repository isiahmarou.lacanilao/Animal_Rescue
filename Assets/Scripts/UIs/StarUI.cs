using Coffee.UIExtensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.ParticleSystem;

public class StarUI : MonoBehaviour
{
    public RectTransform rootRT;
    public RectTransform imageRT;
    [HideInInspector] public Image img;
    [SerializeField] float duration;
    [SerializeField] UIParticle fallingStarFX;
    [SerializeField] List<UIParticle> particleFX = new List<UIParticle>();

    void Awake()
    {
        img = imageRT.GetComponent<Image>();
    }
    public IEnumerator Co_PlayFallingStarsFX()
    {
        fallingStarFX.Play();
        yield return new WaitForSeconds(duration);
        fallingStarFX.Stop();
    }
    public IEnumerator Co_PlayParticleFXs()
    {
  
        for (int i = 0; i < particleFX.Count; i++)
        {
            particleFX[i].Play();
        }
        yield return new WaitForSeconds(duration);
        for (int i = 0; i < particleFX.Count; i++)
        {
            particleFX[i].Stop();
        }

    }
}
