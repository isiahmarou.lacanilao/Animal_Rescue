using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using HelperClass;
using DG.Tweening;
using System;
public class FoodUI : MonoBehaviour
{
    private RectTransform rootRT;

    public Action onEatenEvent;

    public string destinationTag = "DropArea";
    bool eaten = false;
    private void Awake()
    {
        rootRT = GetComponent<RectTransform>();
    }
    public void Initialize(Vector2 p_position)
    {
      
        rootRT.anchoredPosition= p_position;
        eaten = false;
   
    }

    public void OnDrag(Vector3 pos)
    {
        transform.position = pos;
        CheckHit();
    }

    void CheckHit()
    {
        if (!eaten)
        {
            //var rayOrigin = Camera.main.transform.position;
            var rayDirection = CameraHelpers.MouseWorldPosition(transform.position);// - Camera.main.transform.position;
            
            Debug.Log(rayDirection);
            if (CollisionHelpers.CheckRayHit(rayDirection, new Vector2(0.1f,0.1f), destinationTag))
            {
                eaten = true;
                onEatenEvent.Invoke();
            
            }
        }
      
    
    }


}
