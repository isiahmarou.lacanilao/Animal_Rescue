using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using HelperClass;

public class ScoreFloater : MonoBehaviour
{
    [HideInInspector] public PopUpUI popUpUI;
    [SerializeField] TMP_Text scoreText;
    [HideInInspector] public RectTransform scoreRT;

    [SerializeField] Color initialFloaterColor;
    Color defaultColor;

    Vector3 defaultScoreFloaterPos;

    Vector2 scoreUIPosition;
    int baseScoreReward;
    ScoreUI scoreTarget;
    [SerializeField] float lifeTime = 0.35f;
    [SerializeField] float popUpHomeSpeed = 1f;
    List<Sequence> runningTweens = new List<Sequence>();
    private void Awake()
    {
        scoreRT = scoreText.GetComponent<RectTransform>();
    }
    public void Set(ScoreUI p_scoreTarget,int p_reward, Vector2 pos)
    {
        baseScoreReward = p_reward;
        scoreTarget = p_scoreTarget;
      
        scoreText.text = baseScoreReward.ToString();
        scoreText.color = defaultColor;
        scoreUIPosition = pos;
    }
    public IEnumerator Homing()
    {
        float time = 0;
        time = MathHelpers.ConvertSpeedToTime(scoreRT.anchoredPosition, scoreUIPosition, popUpHomeSpeed);
        var pulseOutSequences = DOTween.Sequence();
        if (scoreUIPosition != Vector2.zero)
        {
            if (time >= 0.75f)
            {
                time -= 0.25f;
            }
            transform.SetParent(scoreTarget.transform.parent.transform);
            pulseOutSequences.Append(scoreRT.DOAnchorPos(scoreUIPosition, time));
            //pulseOutSequences.Join(scoreText.DOFade(0f, time));
            runningTweens.Add(pulseOutSequences);
            pulseOutSequences.Play();
            
            yield return new WaitForSeconds(time);
            //yield return pulseOutSequences.WaitForCompletion();
            runningTweens.Remove(pulseOutSequences);
            var pulseOutSequence = DOTween.Sequence();
            pulseOutSequence.Join(scoreText.DOFade(0f, 0.25f));
            scoreTarget.ModifyScore(baseScoreReward);
            runningTweens.Add(pulseOutSequence);
            pulseOutSequence.Play();
            yield return pulseOutSequence.WaitForCompletion();
            runningTweens.Remove(pulseOutSequence);
            StartCoroutine(ScoreFloating(2f,baseScoreReward));
        }
   
    }

    IEnumerator ScoreFloating(float p_duration, int p_gainedScore)
    {
        scoreText.color = initialFloaterColor;
        scoreText.text = "+ " + p_gainedScore;
        scoreRT.anchoredPosition = scoreUIPosition;
        var pulseOutSequences = DOTween.Sequence();
        //Move
        pulseOutSequences.Append(scoreRT.DOAnchorPos3D(new Vector3(scoreRT.anchoredPosition3D.x, scoreRT.anchoredPosition3D.y + 58f, scoreRT.anchoredPosition3D.z), p_duration + 0.5f));
        runningTweens.Add(pulseOutSequences);
        pulseOutSequences.Play();
        yield return pulseOutSequences.WaitForCompletion();
        runningTweens.Remove(pulseOutSequences);
        //FadeIn
        var pulseOutSequencess = DOTween.Sequence();
        //pulseOutSequencess.Append(scoreText.DOFade(0.0f, p_duration * 0.05f));
       
        //runningTweens.Add(pulseOutSequencess);
        //pulseOutSequencess.Play();
        //yield return pulseOutSequencess.WaitForCompletion();
        //runningTweens.Remove(pulseOutSequencess);
        //yield return new WaitForSeconds(p_duration * 0.5f);
        var pulseOutSequencessz = DOTween.Sequence();
        pulseOutSequencessz.Append(scoreText.DOColor(defaultColor, p_duration * 0.45f));
        pulseOutSequencessz.Join(scoreText.DOFade(0.0f, p_duration * 0.45f));
        runningTweens.Add(pulseOutSequencessz);
        pulseOutSequencessz.Play();
        yield return pulseOutSequencessz.WaitForCompletion();
        runningTweens.Remove(pulseOutSequencessz);
        PopUpUIPool.pool.Release(popUpUI);
    }


}
