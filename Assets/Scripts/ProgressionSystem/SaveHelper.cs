using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PersistentManager
{
    public static class SaveHelper
    {
        public static string SAVE_FOLDER => Application.persistentDataPath + "/Saves/"; 
        public static string SAVE_NAME => "/save.json";

        public static string SAVE_FILE_LOCATION => SAVE_FOLDER + SAVE_NAME;
        
        // TODO: Create actual function
        public static string PlayerDataToJson(PlayerData playerData)
        {
            return "";
        }
        
        // TODO: Create actual function
        public static PlayerData JsonToPlayerData(string saveJson)
        {
            return null;
        }
    }

}
