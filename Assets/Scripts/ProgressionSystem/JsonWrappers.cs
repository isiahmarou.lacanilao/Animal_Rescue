using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PersistentManager;

namespace JsonWrappers
{
    [System.Serializable]
    public class PetStatusJsonWrappers
    {
        public string petType;
        
        public string mg_isDone;
        public string mg_scoreList;
        
        public string cs_TimeLastAction;
        public string cs_scoreList;
        
        public string fs_TimeLastAction;
        public string fs_scoreList;
    }

    [System.Serializable]
    public class PlayerDataJsonWrappers
    {
        public string treatsAcquired;
        public string petStatusList;
    }
}


