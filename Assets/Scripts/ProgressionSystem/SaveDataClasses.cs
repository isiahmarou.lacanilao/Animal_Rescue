using System;
using System.Collections.Generic;

namespace PersistentManager
{
    #region Save Data

    [Serializable]
    public class PlayerData
    {
        public PlayerData()
        {
            PetStatus = new List<PetStatus>();
            TreatsAcquired = 0;
        }
        
        public int TreatsAcquired;
        public List<PetStatus> PetStatus;
    }

    #endregion

        
    [Serializable]
    public class MiniGameStatus
    {
        public bool isDone = false;
        public List<int> scores;

    }

    [Serializable]
    public class LobbyStatus
    {
        public DateTime timeLastAction = default;
        public List<int> scores;
    }
    

    #region Custom Classes for Save

    [Serializable]
    public class PetStatus
    {
        public PetStatus()
        {
            miniGameStatus = new MiniGameStatus();
            cleanStatus = new LobbyStatus();
            fedStatus = new LobbyStatus();
            
            miniGameStatus.scores = new List<int>(10);
            cleanStatus.scores = new List<int>(10);
            fedStatus.scores = new List<int>(10);
        }

        public PetType petType;
        public MiniGameStatus miniGameStatus;
        public LobbyStatus cleanStatus;
        public LobbyStatus fedStatus;
    }

    public static class PetStatusHelper
    {
        public static void AddScore(this List<int> scores, int scoreToAdd)
        {
            // TODO: make this more efficient
            
            int lowestScore = 0;
            
            foreach (int score in scores)
            {
                if (score <= lowestScore) lowestScore = score;
            }
            
            // if score didn't crack the top 10
            if(lowestScore >= scoreToAdd) return;

            scores.Remove(lowestScore);
            scores.Add(scoreToAdd);
            
            scores.Sort();
        }
    }
    #endregion
}
