using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using EventManager;

namespace PersistentManager
{
    [CreateAssetMenu(fileName = "ReferencesData", menuName = "ScriptableObjects/Data/ReferencesData", order = 99)]
    public class PlayerDataCache : ScriptableObject
    {
        private bool hasInitialized = false;
        
        // TODO: remove SerializeField property tag. Its for debugging only
        [SerializeField] private PlayerData _playerData;
        
        public PlayerData playerData
        {
            get
            {
                Initialized();
                return _playerData;
            }
        }
        
        void Initialized()
        {
            if(hasInitialized) return;
            
            _playerData = new PlayerData();
            
            // load player data //
            
            // set bool
            hasInitialized = true;
        }
        
        #region Unity Functions

        private void OnEnable()
        {
            Initialized();
            PlayerDataEvents.OnAddTreats.AddListener(OnAddTreats);
            PlayerDataEvents.OnCatchSuccess.AddListener(OnMiniGameDone);
            PlayerDataEvents.OnPetCleaned.AddListener(OnPetCleaned);
            PlayerDataEvents.OnPetFed.AddListener(OnPetCleaned);
        }

        private void OnDisable()
        {
            PlayerDataEvents.OnAddTreats.RemoveListener(OnAddTreats);
            PlayerDataEvents.OnCatchSuccess.RemoveListener(OnMiniGameDone);
            PlayerDataEvents.OnPetCleaned.RemoveListener(OnPetCleaned);
            PlayerDataEvents.OnPetFed.RemoveListener(OnPetCleaned);
        }

        #endregion

        #region Event Listeners

        void OnAddTreats(int value)
        {
            _playerData.TreatsAcquired += value;

            _playerData.TreatsAcquired = Mathf.Clamp(_playerData.TreatsAcquired, 0, int.MaxValue);
        }

        void OnMiniGameDone(PetType type, int score)
        {
            PetStatus petStatus = GetPetStatus(type);
            
            if (petStatus == null || !petStatus.miniGameStatus.isDone)
            {
                // if this is the first time to finish the mini game
                petStatus = new PetStatus();
                petStatus.petType = type;
                petStatus.miniGameStatus.isDone = true;
                
                _playerData.PetStatus.Add(petStatus);
            }
            
            petStatus.miniGameStatus.scores.AddScore(score);
        }

        void OnPetCleaned(PetType type, int score)
        {
            PetStatus petStatus = GetPetStatus(type);

            if (petStatus == null)
            {
                Debug.LogAssertion("no pet type on data, this should not happen");
                return;
            }

            petStatus.cleanStatus.timeLastAction = DateTime.Today;
            petStatus.cleanStatus.scores.AddScore(score);
        }

        void OnPetFed(PetType type, int score)
        {
            PetStatus petStatus = GetPetStatus(type);

            if (petStatus == null)
            {
                Debug.LogAssertion("no pet type on data, this should not happen");
                return;
            }

            petStatus.fedStatus.timeLastAction = DateTime.Today;
            petStatus.fedStatus.scores.AddScore(score);
        }

        #endregion

        #region Private Functions

        private int[] GetEmptyScoreBoard()
        {
            return new int[10]
            {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            };
        }

        #endregion

        #region Public Functions

        public PetType[] GetUnlockedPets()
        {
            return _playerData.PetStatus.Select(ps => ps.petType).ToArray();
        }

        public PetStatus GetPetStatus(PetType type)
        {
            return _playerData.PetStatus.FirstOrDefault(status => status.petType == type);
        }

        public bool IsPetUnlocked(PetType type)
        {
            PetStatus status = GetPetStatus(type);
            
            if (status == null) return false;
            
            return status.petType != PetType.NULL && 
                   status.miniGameStatus.isDone;
        }

        // TODO: combine this and HasCleanedPet func into one
        public bool HasFedPet(PetType type, out DateTime timeLastFed)
        {
            timeLastFed = default;
            
            PetStatus status = GetPetStatus(type);
            
            if (status == null)
            {
                Debug.LogAssertion("no pet type on data, this should not happen");
                return false;
            }

            timeLastFed = status.fedStatus.timeLastAction;
            
            return timeLastFed != default;
        }
        
        public bool HasCleanedPet(PetType type, out DateTime timeLastCleaned)
        {
            timeLastCleaned = default;
            
            PetStatus status = GetPetStatus(type);
            
            if (status == null)
            {
                Debug.LogAssertion("no pet type on data, this should not happen");
                return false;
            }

            timeLastCleaned = status.cleanStatus.timeLastAction;
            
            return timeLastCleaned != default;
        }

        public int[] GetScoreFromCatchMiniGame(PetType type)
        {
            PetStatus status = GetPetStatus(type);
            if (status == null) return GetEmptyScoreBoard();

            return status.miniGameStatus.scores.ToArray();
        }

        public int[] GetScoreFromFedMiniGame(PetType type)
        {
            PetStatus status = GetPetStatus(type);
            if (status == null) return GetEmptyScoreBoard();

            return status.fedStatus.scores.ToArray();
        }
        
        public int[] GetScoreFromCleanMiniGame(PetType type)
        {
            PetStatus status = GetPetStatus(type);
            if (status == null) return GetEmptyScoreBoard();

            return status.cleanStatus.scores.ToArray();
        }
        #endregion
    }
}
