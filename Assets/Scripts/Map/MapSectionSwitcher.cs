using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSectionSwitcher : MonoBehaviour
{
    [SerializeField] private GameObject player;

    public MapSection fromThisMap;
    
    //"Enable Gizmos. Make sure Map Switchers do not cross paths. "Map Switchers must be within current map collider.")]
    public MapSection mapDestination;
    
    
    private void OnTriggerStay2D(Collider2D collision)
    {
        /*if (ReferenceEquals(MapSection.CurrentMapSection, mapDestination)) return;
        if (MapSection.CurrentMapSection == mapDestination) return;
        
        if (collision.TryGetComponent(out Player player))
        {
            MapSection.GoToSection(mapDestination);
        }*/
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        SwitchSection(other.gameObject);
    }

    private void SwitchSection(GameObject enteringObject)
    {
        if (!player == enteringObject) return;

        if (MapSection.CurrentMapSection == fromThisMap)
        {
            if (mapDestination.isPlayerIn && !fromThisMap.isPlayerIn)
            {
                MapSection.GoToSection(mapDestination);
            }
        }

        else if (MapSection.CurrentMapSection == mapDestination)
        {
            if (fromThisMap.isPlayerIn && !mapDestination.isPlayerIn)
            {
                MapSection.GoToSection(fromThisMap);
            }
        }
    }

    public void ForceSwitchSection()
    {
        SwitchSection(player);
    }
}
