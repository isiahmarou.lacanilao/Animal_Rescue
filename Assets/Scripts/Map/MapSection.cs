using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;
using DG.Tweening;

public class MapSection : MonoBehaviour
{
    [Tooltip("Displayed when the player enters this section.")]
    public string sectionName = "Map Section";

    [Header("Camera")]
    //"When changing map collider, do not forget to invalidate the cache in the Virtual Camera.
    [SerializeField] private CinemachineVirtualCamera mapVirtualCamera;

    [TagField]
    [SerializeField] private string mainTargetGroup;

    [Header("Groups")]
    [SerializeField] private bool toggleDisable = false;
    [Tooltip("Everything here becomes disabled when player is not in this section.")]
    [SerializeField] private List<GameObject> disableWhenAway;

    public static MapSection CurrentMapSection { get; private set; }
    public static UnityEvent<MapSection> OnMapSectionEnabled = new();
    public static UnityEvent<MapSection> OnMapSectionDisabled = new();

    public UnityEvent<bool> OnSectionToggled = new();


    public bool isPlayerIn;

    private void Awake()
    {
        ResetCameraFollow();
        mapVirtualCamera.gameObject.SetActive(false);
        DisableMapSection();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            isPlayerIn = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            isPlayerIn = false;
        }
    }

    public CinemachineVirtualCamera GetVirtualCamera() => mapVirtualCamera;

    public void ResetCameraFollow()
    {
        mapVirtualCamera.Follow = GameObject.FindWithTag(mainTargetGroup).transform;
    }

    public static void GoToSection(MapSection mapSection, bool reset = false)
    {
        if (reset)
            CurrentMapSection = null;

        if (CurrentMapSection != null)
            CurrentMapSection.DisableMapSection();

        CurrentMapSection = mapSection;

        if (CurrentMapSection != null)
            CurrentMapSection.EnableMapSection();


    }

    private void EnableMapSection()
    {
        if (toggleDisable)
        {
            foreach (GameObject gameObjectDisable in disableWhenAway)
            {
                gameObjectDisable.SetActive(true);
            }
        }

        if (mapVirtualCamera)
            mapVirtualCamera.gameObject.SetActive(true);

        OnSectionToggled?.Invoke(true);
        OnMapSectionEnabled?.Invoke(this);
    }

    private void DisableMapSection()
    {
        if (toggleDisable)
        {
            foreach (GameObject gameObjectDisable in disableWhenAway)
            {
                if (gameObjectDisable)
                    gameObjectDisable.SetActive(false);
            }
        }

        if (mapVirtualCamera)
            mapVirtualCamera.gameObject.SetActive(false);

        OnSectionToggled?.Invoke(false);
        OnMapSectionDisabled?.Invoke(this);
    }


    //[Button("Set Camera to Solo")]
    private void SetCameraToSolo()
    {
        CinemachineBrain.SoloCamera = mapVirtualCamera;
    }
}
