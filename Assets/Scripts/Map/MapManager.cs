using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class MapManager : MonoBehaviour
{
    [SerializeField] private MapSection startingSection;
    public UnityEvent onMapStart = new();

    private void Start()
    {
        MapSection.GoToSection(startingSection, true);

    }

    public void StartMap()
    {
        onMapStart?.Invoke();
    }
}
