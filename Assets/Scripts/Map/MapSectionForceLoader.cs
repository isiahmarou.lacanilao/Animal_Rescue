using Interactable;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSectionForceLoader : MonoBehaviour
{
    [SerializeField] private MapSectionSwitcher mapSectionSwitcher;
    [SerializeField] private MapSection mapSection;
    [SerializeField] private MapSection currentMapSection;

    //private void Awake() => EventManager.GlobalEvents.OnPlayerClimbLadder.AddListener(StartSwitch);
    private void OnEnable() => EventManager.GlobalEvents.OnPlayerClimbLadder.AddListener(StartSwitch);
    private void OnDisable() => EventManager.GlobalEvents.OnPlayerClimbLadder.RemoveListener(StartSwitch);

    public void StartSwitch(Ladder_Interactable ladder)
    {
        if (ladder.gameObject != this.gameObject) return;

        if (!mapSection.isPlayerIn)
        {
            currentMapSection.isPlayerIn = false;

            mapSection.isPlayerIn = true;

            MapSection.GoToSection(mapSection);
        }
    }
}
