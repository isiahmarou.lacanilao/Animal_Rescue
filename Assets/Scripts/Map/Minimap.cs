using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using HelperClass;

public class Minimap : MonoBehaviour
{
    [SerializeField] private GameObject minimapParent;
    [SerializeField] private Camera camera;
    [SerializeField] private Transform LeftOffset, RightOffset;
    [SerializeField] private ScrollRect scrollRect;
    
    [SerializeField] private RectTransform minimap;
    [SerializeField] private RectTransform scrollDummy;

    private float dist;
    private Vector3 camPos
    {
        get => camera.transform.position;
        set => camera.transform.position = value;
    }
    
    private Transform player;
    private bool isSnapping;
    
    [ContextMenu("Test")]
    private void Awake()
    {
        dist = RightOffset.position.x - LeftOffset.position.x;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        scrollRect.horizontalScrollbar.onValueChanged.AddListener(OnValueChange);

        Vector2 size = camera.GetScreenWorldSize();
        float scale = dist / size.x;
        float dummyScaleX = minimap.sizeDelta.x * scale;
        scrollDummy.sizeDelta = minimap.sizeDelta.Replace_X(dummyScaleX * scale);

        scrollRect.horizontalScrollbar.size = minimap.sizeDelta.x / scrollDummy.sizeDelta.x;
    }

    private void OnValueChange(float val)
    {
        if(isSnapping) return;
        float newX = (dist * val) + LeftOffset.position.x;
        camPos = camera.transform.position.Replace_X(newX);
    }

    private bool isCoroutineRunning;
    
    public void MenuInput(InputAction.CallbackContext context)
    {
        if(!context.started) return;


        if (!minimapParent.activeSelf)
        {
            StartCoroutine(SnapToPlayer());
        }
        
        minimapParent.SetActive(!minimapParent.activeSelf);
    }

    private IEnumerator MenuCoroutine()
    {
        isCoroutineRunning = true;
        if (!minimapParent.activeSelf)
        {
            yield return SnapToPlayer();
        }
        
        minimapParent.SetActive(!minimapParent.activeSelf);
        isCoroutineRunning = false;
    }

    private IEnumerator SnapToPlayer()
    {
        float playerDist = player.position.x - LeftOffset.position.x;
        float distPer = Mathf.Clamp01(playerDist / dist);

        yield return null;
        
        scrollRect.horizontalScrollbar.value = distPer;
    }

    public void ToggleMapVisibility()
    {
        minimapParent.SetActive(!minimapParent.activeSelf);
    }
}
