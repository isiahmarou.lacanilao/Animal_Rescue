using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PersistentManager
{
    [DefaultExecutionOrder(-1)]

    public class GameManager : SingletonPersistent<GameManager>
    {
        protected override void CustomAwake()
        {

        }
    }
}
