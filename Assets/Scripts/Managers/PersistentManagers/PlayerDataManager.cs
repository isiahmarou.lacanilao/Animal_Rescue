using UnityEngine;

namespace PersistentManager
{
    public class PlayerDataManager : SingletonPersistent<PlayerDataManager>
    {
        [SerializeField] private PlayerDataCache PlayerDataCache;

        private PlayerData _playerData => PlayerDataCache.playerData;

        #region Unity Functions

        private void Reset()
        {
            SetReferenceData();
        }

        protected override void CustomAwake()
        {
            SetReferenceData();
        }

        #endregion

        #region Private Functions

        void SetReferenceData()
        {
            if (PlayerDataCache == null) PlayerDataCache = Resources.Load<PlayerDataCache>("PersistentData/PlayerDataCache");
        }

        #endregion

        #region Public Getters For Player Data
        
        /// <summary>
        /// returns treats acquired by the player
        /// How To Use Sample:
        /// int[] catMiniGameScores = PersistentManager.PlayerDataManager.Instance.CatchMiniGameScores(PetType.Cat)
        /// </summary>
        public int TreatsAcquired => _playerData.TreatsAcquired;


        /// <summary>
        /// Check if a specific Pet is Already Unlocked
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool IsPetUnlocked(PetType type) => PlayerDataCache.IsPetUnlocked(type);
        
        
        /// <summary>
        /// returns catching wild animals minigame scores (For Caching Only)
        /// How To Use Sample:
        /// int[] catMiniGameScores = PersistentManager.PlayerDataManager.Instance.CatchMiniGameScores(PetType.Cat);
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int[] CatchMiniGameScores(PetType type) => PlayerDataCache.GetScoreFromCatchMiniGame(type);
        
        
        /// <summary>
        /// returns cleaning pet minigame scores (For Caching Only)
        /// How To Use Sample:
        /// int[] cleanMiniGameScores = PersistentManager.PlayerDataManager.Instance.FedMiniGameScores(PetType.Cat);
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int[] FedMiniGameScores(PetType type) => PlayerDataCache.GetScoreFromFedMiniGame(type);
        
        
        /// <summary>
        /// returns feeding pet minigame scores (For Caching Only)
        /// How To Use Sample:
        /// int[] cleanMiniGameScores = PersistentManager.PlayerDataManager.Instance.GetScoreFromCleanMiniGame(PetType.Cat);
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int[] CleanMiniGameScores(PetType type) => PlayerDataCache.GetScoreFromCleanMiniGame(type);

        
        /// <summary>
        /// returns all unlocked pets
        /// </summary>
        public PetType[] UnlockedPets => PlayerDataCache.GetUnlockedPets();
        
        
        /// <summary>
        /// returns status of a specific pet
        /// </summary>
        public PetStatus GetPetStatus(PetType type) => PlayerDataCache.GetPetStatus(type);

        #endregion
    }
}
