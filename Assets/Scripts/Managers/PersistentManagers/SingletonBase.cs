using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class Singleton<T> : MonoBehaviour
    where T : Component
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance != null) return _instance;

            // get instance
            var objs = FindObjectsOfType(typeof(T)) as T[];

            if (objs.Length > 0)
            {
                _instance = objs[0];
                
                for (int i = 0; i < objs.Length; i++)
                {
                    if(_instance == objs[i]) continue;
                    Destroy(objs[i]);
                }
            }
            
            if (_instance != null) return _instance;
           
            // create instance if there is none
            GameObject obj = new GameObject();
            obj.hideFlags = HideFlags.HideAndDontSave;
            _instance = obj.AddComponent<T>();

            return _instance;
        }
        
        protected set => _instance = value;
    }
}


/// <summary>
/// Only use for Managers that will be included in DontDestroyOnLoad.
/// No More Awake on child classes, Use Custom Awake Instead
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class SingletonPersistent<T> : Singleton<T>
    where T : Component
{
    private void Awake ()
    {
        if (Instance != null)
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (Instance.gameObject.scene.buildIndex > -1)
            {
                DontDestroyOnLoad(this);
            }

            CustomAwake();
        }

        else if (Instance == null)
        {
            Instance = this as T;
        }
    }

    protected virtual void CustomAwake() { }
}
