using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PersistentManager
{
    public class CachedDataManager : SingletonPersistent<CachedDataManager>
    {
        [field: SerializeField] public TagData tagData { get; private set; }

        private void Reset()
        {
            SetReferenceData();
        }

        protected override void CustomAwake()
        {
            SetReferenceData();
        }

        void SetReferenceData()
        {
            if (tagData == null) tagData = Resources.Load<TagData>("PersistentData/TagData");
        }
        
    }
}
