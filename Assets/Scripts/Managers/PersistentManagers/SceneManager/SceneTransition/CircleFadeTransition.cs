using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

public class CircleFadeTransition : FadeTransition_Base
{
    protected override Task OnTransition()
    {
        float size = screenSize.x > screenSize.y ? screenSize.x : screenSize.y;
        size *= screenSize.x > screenSize.y ? (screenSize.x / screenSize.y) : (screenSize.y / screenSize.x);

        Vector2 bigSize = new Vector2(size * 2f, size * 2f );
        
        imgRect.sizeDelta = isStartScene ? bigSize : Vector2.one;
        
        Vector2 endSize = isStartScene ? Vector2.one : bigSize;
        
        Tweener tween = imgRect.DOSizeDelta(endSize, duration);
        tween.SetUpdate(true);
        
        return tween.AsyncWaitForCompletion();
    }
}
