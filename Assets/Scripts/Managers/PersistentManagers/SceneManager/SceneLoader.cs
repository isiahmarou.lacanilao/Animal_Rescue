using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PersistentManager
{
    
    public enum LoaderType
    {
        LoadAdditive = 0,
        LoadSingle = 1,
        Unload = 3,
        ReloadSingle = 4,
        ReloadAdditive = 5,
    }
    
    public class SceneLoader : SingletonPersistent<SceneLoader>
    {
       
        // transition 
        [SerializeField] private FadeTransition_Base[] prefabs;
        private FadeTransition_Base[] _transitionsList;
        private FadeTransition_Base currentFade;
        
        // sceneEnablers
        private List<SceneEnabler> sceneEnablerList = new List<SceneEnabler>();
        private SceneEnabler currentSceneEnabler;


        protected override void CustomAwake()
        {
            _transitionsList = new FadeTransition_Base[prefabs.Length];
            
            for (int i = 0; i < prefabs.Length; i++)
            {
                _transitionsList[i] = Instantiate(prefabs[i], transform, true);
                _transitionsList[i].gameObject.SetActive(false);
            }

            SceneEnabler sceneEnabler = GetSceneEnabler(SceneManager.GetActiveScene().name);
            
            sceneEnablerList.Add(sceneEnabler);

            currentFade = GetRandomFade();
        }

        private void Start()
        {
            EventManager.GlobalEvents.OnLoadScene.AddListener(ManageScene);
            SceneManager.sceneLoaded += OnOpeningTransition;
            SceneManager.sceneUnloaded += OnClosingTransition;
        }

        private void OnDestroy()
        {
            EventManager.GlobalEvents.OnLoadScene.RemoveListener(ManageScene);
            SceneManager.sceneLoaded -= OnOpeningTransition;
            SceneManager.sceneUnloaded -= OnClosingTransition;
        }

        private SceneEnabler GetSceneEnabler(string sceneName)
        {
            foreach (GameObject o in 
                     SceneManager.GetSceneByName(sceneName).GetRootGameObjects())
            {
                if (!o.TryGetComponent(out SceneEnabler sm)) continue;
                return sm;
            }
            return null;
        }

        private void ManageScene(string sceneName, LoaderType type)
        {
            switch (type)
            {
                case LoaderType.LoadAdditive:
                {
                    LoadScene(sceneName, true);
                    break;
                }
                case LoaderType.LoadSingle:
                {
                    LoadScene(sceneName, false);
                    break;
                }
                case LoaderType.Unload:
                {
                    UnloadScene(sceneName);
                    break;
                }
                case LoaderType.ReloadAdditive:
                {
                    ReloadScene(sceneName, true);
                    break;
                }
                case LoaderType.ReloadSingle:
                {
                    ReloadScene(sceneName, false);
                    break;
                }
                
            }
        }

        private async void LoadScene(string sceneToLoad, bool isAdditive)
        {
            Time.timeScale = 0;

            LoadSceneMode mode = isAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single;

            AsyncOperation load = SceneManager.LoadSceneAsync(sceneToLoad, mode);
            
            load.allowSceneActivation = false;

            currentFade = GetRandomFade();
            
            currentFade.gameObject.SetActive(true);
            
            await currentFade.StartTransition(false);

            load.allowSceneActivation = true;
        }

        private async void UnloadScene(string sceneToUnload)
        {
            Time.timeScale = 0;

            SceneEnabler enablerToRemove = sceneEnablerList.FirstOrDefault(s => s.scene.name == sceneToUnload);
            sceneEnablerList.Remove(enablerToRemove);
            
            if (sceneEnablerList.Count == 0)
            {
                // if no other scene is loaded, but this shouldn't happen
                var defaultScene = "MainLobby";
                Debug.LogError("No Other Scene???");
                Debug.Log(defaultScene);
                LoadScene(defaultScene, false);
                return;
            }

            currentFade = GetRandomFade();
            
            currentFade.gameObject.SetActive(true);
            
            await currentFade.StartTransition(false);

            AsyncOperation unload = SceneManager.UnloadSceneAsync(
                sceneToUnload,
                UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);

            foreach (SceneEnabler sceneEnabler in sceneEnablerList)
            {
                SceneManager.SetActiveScene(sceneEnabler.scene);
            }
        }

        private async void ReloadScene(string sceneToReload, bool isAdditive)
        {
            Time.timeScale = 0;
            
            var enablerToRemove = sceneEnablerList.FirstOrDefault(s => s.scene.name == sceneToReload);
            sceneEnablerList.Remove(enablerToRemove);

            try
            {
                AsyncOperation unload = SceneManager.UnloadSceneAsync(
                    sceneToReload,
                    UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);

                while (!unload.isDone)
                {
                    await Task.Delay(50);
                }
                LoadScene(sceneToReload, isAdditive);
            }
            catch (Exception e)
            {
                LoadScene(sceneToReload, false);
            }

            // LoadScene(sceneToReload, isAdditive);
        }

        private async void OnOpeningTransition(Scene scene, LoadSceneMode mode)
        {
            Time.timeScale = 0;

            SceneManager.SetActiveScene(scene);
            
            SceneEnabler sceneEnabler = GetSceneEnabler(scene.name);
            if (sceneEnabler == null)
            {
                GameObject newObject = new GameObject("Scene Enabler");
                SceneManager.MoveGameObjectToScene(newObject, scene);
                sceneEnabler = newObject.AddComponent<SceneEnabler>();
            }

            if (mode == LoadSceneMode.Additive)
            {
                if (!sceneEnablerList.Contains(sceneEnabler) && sceneEnabler != null)
                {
                    sceneEnablerList.Add(sceneEnabler);
                    currentSceneEnabler = sceneEnabler;
                }
            }
            else
            {
                if (sceneEnabler != null)
                {
                    sceneEnablerList = new List<SceneEnabler>();
                    sceneEnablerList.Add(sceneEnabler);
                    currentSceneEnabler = sceneEnabler;
                }
            }

            await currentFade.StartTransition(true);
            
            currentFade.gameObject.SetActive(false);
            
            Time.timeScale = 1;
        }
        
        private async void OnClosingTransition(Scene scene)
        {
            Time.timeScale = 0; 
            
            await currentFade.StartTransition(true);
            
            currentSceneEnabler = sceneEnablerList.FirstOrDefault();
            
            currentFade.gameObject.SetActive(false);
            
            Time.timeScale = 1;
        }

        private FadeTransition_Base GetRandomFade()
        {
            int index = UnityEngine.Random.Range(0, _transitionsList.Length);
            return _transitionsList[index];
        }
    }
}
