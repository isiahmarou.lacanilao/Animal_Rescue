using System;
using System.Collections;
using System.Collections.Generic;
using HelperClass;
using UnityEngine;


public class DryingMiniGame : MonoBehaviour
{
    Camera cam;
    [SerializeField]
    private List<ParticleSystem> sparkleParticles = new List<ParticleSystem>();
    private List<GameObject> sparkleParticlesGO = new List<GameObject>();
    [SerializeField]
    private ParticleSystem waterParticle;
    private GameObject waterParticleGO;
    ParticleSystem.EmissionModule em;
    ParticleSystem.ShapeModule shape;
    [SerializeField] private GameObject particleGO;
    private Transform particleTransform;
    private Vector2 mousePos;
    public bool inUse = false;

    private List<DirtSpot> dirtSpots = new List<DirtSpot>();

    public float dirtAmountTotal;

    float percentage;
    IEnumerator runningCoroutine;
    public Action loaded;
    public Action<float> onCleanEvent;


    float currentDirtAmount = 0;

    private void Awake()
    {
        cam = gameObject.scene.GetFirstMainCameraInScene();
        particleGO.SetActive(false);
        particleTransform = particleGO.transform;
        waterParticleGO = waterParticle.gameObject;
        em = waterParticle.emission;
        shape = waterParticle.shape;
    }

    public void Enable()
    {
        inUse = true;
        particleGO.SetActive(true);
        waterParticleGO.SetActive(true);
        waterParticle.Play();

        em.rateOverTime = 30;
        for (int i = 0; i < sparkleParticles.Count; i++)
        { 
            sparkleParticles[i].Stop();
            sparkleParticlesGO[i].SetActive(false);
        }
    }
    public void Disable()
    {
        inUse = false;
        particleGO.SetActive(false);
        StartCoroutine(Co_Disa());
    }

    IEnumerator Co_Disa()
    {
        for (int i = 0; i < sparkleParticles.Count; i++)
        {
            sparkleParticles[i].Play();
            sparkleParticlesGO[i].SetActive(true);
        }
        yield return new WaitForSeconds(3f);
        waterParticleGO.SetActive(false);
        waterParticle.Stop();
    }
    private void Update()
    {
        // Click left mouse button to turn particles on
        // and palce them at mouse position
        if (inUse)
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                particleGO.SetActive(true);
            }
            if (Input.GetMouseButton(0))
            {
                mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
                particleTransform.position = new Vector3(mousePos.x, mousePos.y, 0f);
                CheckHit();
            }
            else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                mousePos = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
                particleTransform.position = new Vector3(mousePos.x, mousePos.y, 0f);
                CheckHit();
            }

            // Left mouse button is released - Partiles Off
            if (Input.GetMouseButtonUp(0) || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                particleGO.SetActive(false);
            }
        }


    }
    void CheckHit()
    {

        var rayDirection = CameraHelpers.MouseWorldPosition(transform.position);
        RaycastHit2D hitInfo = Physics2D.Raycast(rayDirection, Vector3.forward);
        // Debug.Log("Checking if hit");
        if (hitInfo)
        {
            // Debug.Log("hit something " + hitInfo.collider.gameObject.name);
            if (hitInfo.collider.TryGetComponent<DirtSpot>(out DirtSpot ds))
            {
                // Debug.Log("got dirty spot");
                ds.OnHit(rayDirection);
            }



        }


    }

    public void Initialize(List<DirtSpot> p_dirtSpots, List<ParticleSystem> p_sparkleParticles, SpriteRenderer sprite)
    {
        inUse = true;
        dirtSpots = p_dirtSpots;
        sparkleParticles = p_sparkleParticles;
        shape.spriteRenderer = sprite;
        for (int i = 0; i < sparkleParticles.Count; i++)
        {
            sparkleParticlesGO.Add(sparkleParticles[i].gameObject);
            sparkleParticlesGO[i].SetActive(false);
        }
        for (int i = 0; i < dirtSpots.Count; i++)
        {
            dirtAmountTotal += dirtSpots[i].maxHealth;
            dirtSpots[i].gameObject.SetActive(true);
            dirtSpots[i].onCleanEvent += Refresh;

        }
        currentDirtAmount = dirtAmountTotal;
        loaded.Invoke();
    }

    public void EndGame()
    {
        for (int i = 0; i < dirtSpots.Count; i++)
        {
            dirtSpots[i].onCleanEvent -= Refresh;
            dirtSpots[i].gameObject.SetActive(false);
        }
    }

    void Refresh(int modifier)
    {
        currentDirtAmount -= modifier;
        float removed = dirtAmountTotal - currentDirtAmount;

        percentage = currentDirtAmount / dirtAmountTotal;
        em.rateOverTime = percentage * 30;
        //Debug.Log(modifier + " " + removed + " " + percentage + " " + dirtAmountTotal + " " + currentDirtAmount);
        onCleanEvent.Invoke((removed));

        if (percentage > 0.50)
        {

        }
        else
        {

        }
    }
}
