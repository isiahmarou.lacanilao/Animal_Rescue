using System;
using System.Collections;
using System.Collections.Generic;
using HelperClass;
using UnityEngine;
using static UnityEngine.ParticleSystem;
public class SwipeCleanMiniGame : MonoBehaviour
{
    Camera cam;
    [SerializeField]
    private ParticleSystem particles;
    private GameObject particleGO;
    private Vector2 mousePos;
    public bool inUse = false;

    private List<DirtSpot> dirtSpots = new List<DirtSpot>();

    public float dirtAmountTotal;

    float percentage;
    IEnumerator runningCoroutine;
    public Action loaded;
    public Action<float> onCleanEvent;


    float currentDirtAmount = 0;

    private void Awake()
    {
        particles.Stop();
        cam = gameObject.scene.GetFirstMainCameraInScene();
    }
    public void Enable()
    {
        inUse = true;
        particleGO = particles.gameObject;
        particleGO.SetActive(true);

    }
    public void Disable()
    {
        inUse = false;
        particles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        particleGO.SetActive(false);
    }

    private void Update()
    {
        // Click left mouse button to turn particles on
        // and palce them at mouse position
        if (inUse)
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                particles.Play();
            }
            if (Input.GetMouseButton(0))
            {
                mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
                particles.transform.position = new Vector3(mousePos.x, mousePos.y, 0f);
                CheckHit();
            }
            else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                mousePos = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
                particles.transform.position = new Vector3(mousePos.x, mousePos.y, 0f);
                CheckHit();
            }
            // Left mouse button is released - Partiles Off
            if (Input.GetMouseButtonUp(0) || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                particles.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            }
        }


    }
    void CheckHit()
    {

        var rayDirection = CameraHelpers.MouseWorldPosition(transform.position);
        RaycastHit2D hitInfo = Physics2D.Raycast(rayDirection, Vector3.forward);
        // Debug.Log("Checking if hit");
        if (hitInfo)
        {
            // Debug.Log("hit something " + hitInfo.collider.gameObject.name);
            if (hitInfo.collider.TryGetComponent<DirtSpot>(out DirtSpot ds))
            {
                // Debug.Log("got dirty spot");
                ds.OnHit(rayDirection);
            }



        }


    }

    public void Initialize(List<DirtSpot> p_dirtSpots)
    {
        inUse = true;
        dirtSpots = p_dirtSpots;
        for (int i = 0; i < dirtSpots.Count; i++)
        {
            dirtAmountTotal+= dirtSpots[i].maxHealth;
            dirtSpots[i].gameObject.SetActive(true);
            dirtSpots[i].onCleanEvent += Refresh;

        }
        currentDirtAmount = dirtAmountTotal;
        loaded.Invoke();
    }

    public void EndGame()
    {
        for (int i = 0; i < dirtSpots.Count; i++)
        {
            dirtSpots[i].onCleanEvent -= Refresh;
            dirtSpots[i].gameObject.SetActive(false);
        }
    }

    void Refresh(int modifier)
    {
        currentDirtAmount -= modifier;
        float removed = dirtAmountTotal - currentDirtAmount;

        percentage = removed / dirtAmountTotal;
        //Debug.Log(modifier + " " + removed + " " + percentage + " " + dirtAmountTotal + " " + currentDirtAmount);
        onCleanEvent.Invoke((removed));
   
        if (percentage > 0.50)
        {

        }
        else
        {

        }
    }
    
}
