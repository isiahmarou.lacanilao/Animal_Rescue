using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using HelperClass;

public class SlicingMiniGame : MonoBehaviour
{
    Camera cam;
    [SerializeField]
    private GameObject sliceSegmentHinterGO;
    private LineRenderer sliceSegmentHinterLR;
    private Transform sliceSegmentHinterTrans;
    [SerializeField] private GameObject toolGO;
    private Transform toolTrans;
    private Vector2 mousePos;
    public bool inUse = false;
    [SerializeField]
    private float distanceOffset;
    [SerializeField]
    private float directionAngleOffset;
    SliceSegment target;
    int currentSlicableIndex = 0;
    public List<SliceSegment> sliceSegments = new List<SliceSegment>();

    int currentSliceSegments = 0;
    float percentage;

    IEnumerator runningCoroutine;
    public Action loaded;

    private Vector2 startTouchPos;
    private Vector2 endTouchPos;


    private void Awake()
    {
        cam = gameObject.scene.GetFirstMainCameraInScene();
        toolGO.SetActive(false);
        toolTrans = toolGO.transform;
        sliceSegmentHinterLR = sliceSegmentHinterGO.GetComponent<LineRenderer>();
        sliceSegmentHinterTrans = sliceSegmentHinterGO.transform;
        Enable(); //temp
        Initialize(sliceSegments);//temp
    }

    public void Enable()
    {
        inUse = true;
        toolGO.SetActive(true);



    }
    public void Disable()
    {
        inUse = false;
        toolGO.SetActive(false);
        sliceSegmentHinterGO.SetActive(false);
        StartCoroutine(Co_Disa());
    }

    IEnumerator Co_Disa()
    {

        yield return new WaitForSeconds(3f);

    }
    private void Update()
    {
        // Click left mouse button to turn particles on
        // and palce them at mouse position
        if (inUse)
        {

            if (Input.GetMouseButtonDown(0))
            {
                toolGO.SetActive(true);
                startTouchPos = cam.ScreenToWorldPoint(Input.mousePosition);
            }
            else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                toolGO.SetActive(true);
                startTouchPos = Input.GetTouch(0).position;
            }
            if (Input.GetMouseButton(0))
            {
                mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
                toolTrans.position = new Vector3(mousePos.x, mousePos.y, 0f);
                CheckHit();
            }
            else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                mousePos = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
                toolTrans.position = new Vector3(mousePos.x, mousePos.y, 0f);
                CheckHit();
            }

            // Left mouse button is released - Partiles Off
            if (Input.GetMouseButtonUp(0))
            {
                endTouchPos = cam.ScreenToWorldPoint(Input.mousePosition);
                toolGO.SetActive(false);
                Refresh();
            }
            else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                endTouchPos = Input.GetTouch(0).position;
                toolGO.SetActive(false);
                Refresh();
            }
        }


    }
    void CheckHit()
    {

        var rayDirection = CameraHelpers.MouseWorldPosition(transform.position);
        RaycastHit2D hitInfo = Physics2D.Raycast(rayDirection, Vector3.forward);
        Debug.Log("Checking if hit");
        if (hitInfo)
        {
            Debug.Log("hit something " + hitInfo.collider.gameObject.name);
            if (hitInfo.collider.TryGetComponent<SliceSegment>(out SliceSegment ds))
            {
                if (ds == sliceSegments[currentSlicableIndex])
                {
                    target = ds;
                }


            }
            else
            {
                //target = null;
            }



        }


    }

    public void Initialize(List<SliceSegment> p_sliceSegment)
    {
        inUse = true;

        sliceSegments = p_sliceSegment;
        for (int i = 0; i < sliceSegments.Count; i++)
        {
            sliceSegments[i].Initialize();
        }
        currentSliceSegments = 0;
        sliceSegmentHinterTrans.position = sliceSegments[currentSlicableIndex].srTransform.position;
        SetLinePoints(sliceSegments[currentSlicableIndex].pointA.position, sliceSegments[currentSlicableIndex].pointB.position);
        sliceSegmentHinterGO.SetActive(true);
        //loaded.Invoke(); //temp add again
    }

    void SetLinePoints(Vector2 pointA, Vector2 pointB)
    {
        sliceSegmentHinterLR.positionCount = 0;
        sliceSegmentHinterLR.positionCount = 2;
        float dis = Vector2.Distance(pointA, pointB);
        Vector3[] points = new Vector3[2];
        points[0] = pointA;
        points[1] = pointB;
        sliceSegmentHinterLR.SetPositions(points);
    }
    public void EndGame()
    {
        for (int i = 0; i < sliceSegments.Count; i++)
        {

            sliceSegments[i].gameObject.SetActive(false);
        }
    }

    void Refresh()
    {
        Vector2 dir = (endTouchPos - startTouchPos).normalized;
        float dist = Vector2.Distance(startTouchPos, endTouchPos);
        Debug.Log("HUH 0 " + target);
        if (target != null)
        {
            Vector2 lowOffset = GetDirectionFromAngle(target.direction, directionAngleOffset);
            Vector2 highOffset = GetDirectionFromAngle(target.direction, -directionAngleOffset);
            Debug.Log(lowOffset + " , " + highOffset);
            Debug.Log("HUH 1 " + dist + " == " + target.distance + " + " + distanceOffset);
            if (dist < target.distance + distanceOffset)
            {
                Debug.Log("HUH 2 " + dir);
                if (IsBetween(lowOffset, highOffset, dir) ||
                IsBetween(-lowOffset, -highOffset, -dir))
                {
                    Debug.Log("HUH 3");
                    sliceSegments[currentSlicableIndex].Cut();
                    currentSliceSegments -= 1;
                    currentSlicableIndex++;
                    if (currentSlicableIndex >= sliceSegments.Count)
                    {

                    }
                    else
                    {
                        sliceSegmentHinterTrans.position = sliceSegments[currentSlicableIndex].srTransform.position;
                        SetLinePoints(sliceSegments[currentSlicableIndex].pointA.position, sliceSegments[currentSlicableIndex].pointB.position);
                        float removed = sliceSegments.Count - currentSliceSegments;

                        percentage = currentSliceSegments / sliceSegments.Count;

                        Debug.Log("HUH 4");

                        if (percentage > 0.50)
                        {

                        }
                        else
                        {

                        }
                    }

                }

            }

        }


    }
    public Vector2 GetDirectionFromAngle(Vector2 originalDirection, float angle)
    {
        // Get the rotation quaternion for the desired angle
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        // Rotate the Vector2.up direction by the quaternion to get the desired direction
        Vector3 direction3D = rotation * Vector3.up;

        // Convert the direction back to a Vector2 by dropping the z-coordinate
        Vector2 direction2D = new Vector2(direction3D.x, direction3D.y);

        return direction2D;
    }
    public bool IsBetween(Vector2 direction1, Vector2 direction2, Vector2 targetDirection)
    {
        // Calculate the angles between the directions
        float angle1 = Vector2.SignedAngle(Vector2.up, direction1);
        float angle2 = Vector2.SignedAngle(Vector2.up, direction2);
        float targetAngle = Vector2.SignedAngle(Vector2.up, targetDirection);

        // Make sure angles are in the range of 0 to 360 degrees
        if (angle1 < 0) angle1 += 360f;
        if (angle2 < 0) angle2 += 360f;
        if (targetAngle < 0) targetAngle += 360f;

        // Check if target direction is between direction1 and direction2
        if (angle1 <= targetAngle && targetAngle <= angle2)
        {
            return true;
        }

        // Check if target direction is between direction2 and direction1 (i.e. wraps around 0/360 degrees)
        if (angle2 < angle1 && (targetAngle >= angle1 || targetAngle <= angle2))
        {
            return true;
        }

        // Target direction is not between direction1 and direction2
        return false;
    }
}
