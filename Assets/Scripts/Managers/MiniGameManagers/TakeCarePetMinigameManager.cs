using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using HelperClass;

[System.Serializable]
public class SpriteList
{
    public List<Sprite> spriteList = new List<Sprite>();
}
public class TakeCarePetMinigameManager : MonoBehaviour
{
    public static TakeCarePetMinigameManager instance;
    Camera cam;
    float defaultCamSize;
    //[SerializeField] BeatUISpawner beatUISpawner;
    //[SerializeField] HealthUI healthUI;
    [SerializeField] FeedingUI feedingUI;


    [SerializeField] FactPopUpUI factPopUpUI;
    [SerializeField] ScoreUI scoreUI;
    [SerializeField] ProgressionUI progressionUI;
    [SerializeField] ResultsUI resultsUI;
    [SerializeField] SwipeCleanMiniGame cleanMinigame;
    [SerializeField] DryingMiniGame dryingMinigame;
 
    bool isWin = false;
    bool isPlaying = true;
    Vector3 defaultPlayerPosition;
    [SerializeField] private List<SpriteList> gameModeSpriteList = new List<SpriteList>();
    public static Action OnGameOver;
    private int currentGameEndStep = 0;
    [SerializeField] private int maxGameEndStep = 2;
    [SerializeField]
    Vector2 p_origin;
    [SerializeField] List<HitData> hitDatas = new List<HitData>(); //temp

    bool washingLoaded = false;
    bool dryinggLoaded = false;
    [SerializeField] List<Pet> pets = new List<Pet>();
    [SerializeField] Pet pet;
    [SerializeField] Transform petSpawn;
    public static string petName;
    void Awake()
    {
        instance = this;
        cam = gameObject.scene.GetFirstMainCameraInScene();

        defaultCamSize = cam.orthographicSize;
        defaultPlayerPosition = cam.transform.position;

 
    }
    void WashingMinigameLoaded()
    {
        washingLoaded = true;
    }

    void DryingMinigameLoaded()
    {
        dryinggLoaded = true;
    }
    void OnDestroy()
    {
        //OnMistakeEvent -= Co_Mistake;
        //OnCorrectEvent -= Co_MoveForwardCamera;

        //healthUI.OnHealthDeathEvent -= GameEnd;
        progressionUI.OnReachedGoal -= CheckGameEnd;
        OnGameOver -= progressionUI.Finished;

    }

    private void Start()
    {
        GameStart();

    }
  
    public void GameStart()
    {

        cleanMinigame.loaded += WashingMinigameLoaded;
        dryingMinigame.loaded += DryingMinigameLoaded;

        progressionUI.OnReachedGoal += CheckGameEnd;
        OnGameOver += progressionUI.Finished;
        cleanMinigame.onCleanEvent += progressionUI.UpdateProgress;
        dryingMinigame.onCleanEvent += progressionUI.UpdateProgress;
        feedingUI.OnEatenEvent += progressionUI.ModifyProgress;
        currentGameEndStep = 0;
        resultsUI.Hide();

        isWin = false;
        isPlaying = true;

        //healthUI.OnInitializeEvent.Invoke();

        scoreUI.ResetScore();

        cam.orthographicSize = defaultCamSize;
        cam.transform.position = defaultPlayerPosition;

        feedingUI.Deinitialize();
        if (!string.IsNullOrEmpty(petName))
        {
            for (int i = 0; i < pets.Count; i++)
            {
                if (petName == pets[i].name)
                {
                    pet = Instantiate(pets[i]);
                    pet.transform.position = petSpawn.position;
                    pet.transform.SetParent(petSpawn);
                }
            }
        }
        else
        {
            pet = Instantiate(pets[0]);
            pet.transform.position = petSpawn.position;
            pet.transform.SetParent(petSpawn);
 
        }
      
        StartCoroutine(StartWashingPhase());
    }
    void CheckGameEnd()
    {
        currentGameEndStep++;
        if (currentGameEndStep == 1)
        {
            StartCoroutine(EndWashingPhase());
        }
        else if (currentGameEndStep == 2) // TEMP
        {
            StartCoroutine(EndDryingPhase());
        }
        else if (currentGameEndStep == maxGameEndStep)
        {
            StartCoroutine(EndFeedingPhase());
        }
        else
        {
            StartCoroutine(Co_GameEndDelay());
        }

    }

    IEnumerator Co_GameEndDelay()
    {
        //StartCoroutine(beatUISpawner.currentBeat.Co_HideAll());
        yield return new WaitForSeconds(3f);
        resultsUI.Show(isWin, scoreUI.currentPoints, 400);

        cleanMinigame.loaded -= WashingMinigameLoaded;
        dryingMinigame.loaded -= DryingMinigameLoaded;

        progressionUI.OnReachedGoal -= CheckGameEnd;
        OnGameOver -= progressionUI.Finished;
        cleanMinigame.onCleanEvent -= progressionUI.UpdateProgress;
        dryingMinigame.onCleanEvent -= progressionUI.UpdateProgress;
        feedingUI.OnEatenEvent -= progressionUI.ModifyProgress;
    }
    void GameEnd()
    {
        isPlaying = false;
        OnGameOver?.Invoke();
        StartCoroutine(Co_GameEndDelay());


    }

    IEnumerator Co_Mistake()
    {

        HitData p_hitData = hitDatas[0]; // temp
        PopUpUI popUpUI = PopUpUIPool.pool.Get();
        popUpUI.PopIn(p_hitData, p_origin);
 
        yield return new WaitForSeconds(1f);


    }
    IEnumerator Evaluate()
    {
        if (progressionUI.percent >= 0.3)
        {

        }
        else
        {
            yield return StartCoroutine(CameraHelpers.Co_Shake(defaultPlayerPosition));
        }
        
        yield return new WaitForSeconds(1f);


    }
    IEnumerator StartWashingPhase()
    {
        if (isPlaying)
        {
            factPopUpUI.Initialize("Fun fact","Text here");
            cleanMinigame.Initialize(new List<DirtSpot>(pet.washingDirtSpots));

          
            while (!washingLoaded)
            {
                yield return new WaitForSeconds(0.1f);
            }
            cleanMinigame.gameObject.SetActive(true);
            cleanMinigame.Enable();
            progressionUI.InitializeStats((int)cleanMinigame.dirtAmountTotal,1,gameModeSpriteList[0].spriteList);
        
        }
    }
    IEnumerator EndWashingPhase()
    {
        progressionUI.StopTime();
        cleanMinigame.inUse = false;
        cleanMinigame.EndGame();
        cleanMinigame.Disable();
        progressionUI.StopTime();

        yield return StartCoroutine(Evaluate());
  
        StartCoroutine(StartDryingPhase());

    }
    IEnumerator StartDryingPhase()
    {

        if (isPlaying)
        {
            factPopUpUI.Initialize("Fun fact", "Text here");
            dryingMinigame.Initialize(new List<DirtSpot>(pet.dryingDirtSpots), new List<ParticleSystem>(pet.sparkleParticles),pet.GetComponent<SpriteRenderer>());
            while (!washingLoaded)
            {
                yield return new WaitForSeconds(0.1f);
            }
            dryingMinigame.gameObject.SetActive(true);
            dryingMinigame.Enable();
            progressionUI.InitializeStats((int)dryingMinigame.dirtAmountTotal, 1, gameModeSpriteList[0].spriteList);
        }
    }

  
    IEnumerator EndDryingPhase()
    {
        progressionUI.StopTime();
        dryingMinigame.inUse = false;
        dryingMinigame.EndGame();
        dryingMinigame.Disable();
        progressionUI.StopTime();

        yield return StartCoroutine(Evaluate());
        StartCoroutine(StartFeedingPhase());
    }

    IEnumerator StartFeedingPhase()
    {
        if (isPlaying)
        {
            //cleanMinigame.gameObject.SetActive(true);
            factPopUpUI.Initialize("Fun fact", "Text here");
            progressionUI.InitializeStats(1, gameModeSpriteList[2].spriteList,30);
            yield return new WaitForSeconds(0.1f);
            feedingUI.Initialize();
            
        }
    }
    IEnumerator EndFeedingPhase()
    {
        progressionUI.StopTime();
        cleanMinigame.gameObject.SetActive(false);
        feedingUI.Deinitialize();
        progressionUI.StopTime();
        
        yield return StartCoroutine(Evaluate());
        if (isPlaying)
        {
            isWin = true;

            GameEnd();
        }
    }
}
