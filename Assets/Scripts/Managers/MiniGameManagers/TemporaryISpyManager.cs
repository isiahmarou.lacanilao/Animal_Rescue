using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UnityEngine.UI;
using TMPro;
using System;
using HelperClass;
using Unity.VisualScripting;

public class TemporaryISpyManager : MonoBehaviour
{

    [SerializeField] List<HitData> hitDatas = new List<HitData>();

    public static TemporaryISpyManager instance;

    [SerializeField] List<PedestrianSpawner> pedestrianSpawner = new List<PedestrianSpawner>();
    [SerializeField]
    List<Transform> lostAnimalLocations = new List<Transform>();
    [SerializeField] WeightedFloatDatas locationIndex;
    [SerializeField] WeightedFloatDatas waitTimes;
    //[SerializeField] Collider2D spawnThreshold;

    [SerializeField] LostAnimal currentLostAnimal;

    //[SerializeField] HealthUI healthUI;
    [SerializeField] ScoreUI scoreUI;
    [SerializeField] ProgressionUI progressionUI;
    [SerializeField] ResultsUI resultsUI;

    Vector2 defaultPlayerPosition;
    Vector3 playerPosition;

    [SerializeField] Sprite spriteIcon;


    Camera cam;
    float defaultCamSize;

    [Header("Camera Shake Settings")]
    [SerializeField] float shakePositionDuration = 0.35f;
    [SerializeField] Vector3 shakePositionPower = new Vector3(15f, 5f);
    [SerializeField] int shakePositionVibrato = 10;
    [SerializeField] float shakePositionRandomRange = 10f;
    [SerializeField] bool shakePositionCanFade = true;

    [SerializeField] float delayTime = 1f;

    public delegate IEnumerator CorrectEvent(Vector2 p_origin, float p_distance);
    public static CorrectEvent OnCorrectEvent;
    //public static Action<Vector2,float> OnCorrectEvent;
    public delegate IEnumerator MistakeEvent(Vector2 p_origin);
    public static MistakeEvent OnMistakeEvent;

    bool isWin = false;
    bool isPlaying = true;
    public static Action OnGameOver;

    void Awake()
    {
        instance = this;
        //cam = Camera.main;
        cam = gameObject.scene.GetFirstMainCameraInScene();
        defaultPlayerPosition = cam.transform.position;
        defaultCamSize = cam.orthographicSize;


        OnMistakeEvent += Co_Mistake;
        //OnCorrectEvent += Co_MoveForwardCamera;
        currentLostAnimal.onClickEvent += Correct;
        //healthUI.OnHealthDeathEvent += GameEnd;
        progressionUI.OnReachedGoal += GameWon;
        OnGameOver += progressionUI.Finished;
        //background.localScale
    }
    void Correct()
    {
        GameWon();
    }
    private void Start()
    {
        GameStart();
    }

    void OnDestroy()
    {
        OnMistakeEvent -= Co_Mistake;
        //OnCorrectEvent -= Co_MoveForwardCamera;

        //healthUI.OnHealthDeathEvent -= GameEnd;
        progressionUI.OnReachedGoal -= GameWon;
        OnGameOver -= progressionUI.Finished;

    }

    public void GameStart()
    {
        resultsUI.Hide();

    
        isWin = false;
        isPlaying = true;

        //healthUI.OnInitializeEvent.Invoke();
        progressionUI.InitializeStats(spriteIcon);
        scoreUI.ResetScore();
        playerPosition = new Vector3(defaultPlayerPosition.x, defaultPlayerPosition.y,- 10f);

        cam.orthographicSize = defaultCamSize;
        cam.transform.position = playerPosition;

        for (int i=0; i < pedestrianSpawner.Count;i++)
        {
            pedestrianSpawner[i].SpawnPedestrian();
        }
        locationIndex.maxChance = MathHelpers.AddAll(locationIndex);
        waitTimes.maxChance = MathHelpers.AddAll(waitTimes);
        StartCoroutine(Co_RelocateLostAnimal());

    }
    void GameWon()
    {
        isWin = true;

        GameEnd();
    }
    IEnumerator Co_RelocateLostAnimal()
    {
        int wi = (int)MathHelpers.ChooseWeightedFloat(locationIndex);
        currentLostAnimal.transform.position = lostAnimalLocations[wi].position;
        float wf = MathHelpers.ChooseWeightedFloat(waitTimes);
        yield return new WaitForSeconds(wf);
        StartCoroutine(Co_RelocateLostAnimal());
    }
    IEnumerator Co_GameEndDelay()
    {
        yield return new WaitForSeconds(3f);
        resultsUI.Show(isWin, scoreUI.currentPoints, 400);
    }
    void GameEnd()
    {
        isPlaying = false;
        OnGameOver?.Invoke();
        StartCoroutine(Co_GameEndDelay());
        for (int i = 0; i < pedestrianSpawner.Count; i++)
        {
            pedestrianSpawner[i].StopSpawnPedestrian();
        }

    }



    IEnumerator Co_Mistake(Vector2 p_origin)
    {

        HitData p_hitData = hitDatas[1];
        PopUpUI popUpUI = PopUpUIPool.pool.Get();
        popUpUI.PopIn(p_hitData, p_origin);
        //healthUI.OnModifyHealthEvent.Invoke(-20);

        //INDIVIDUAL SHAKE
        var shakeSequence = DOTween.Sequence()
            .Append(cam.DOShakePosition(shakePositionDuration, shakePositionPower, shakePositionVibrato, shakePositionRandomRange, shakePositionCanFade));

        shakeSequence.Play();
        yield return shakeSequence.WaitForCompletion();
        cam.transform.position = playerPosition;

        yield return new WaitForSeconds(1f);
        

    }



}
