using System;
using System.Collections;
using System.Collections.Generic;
using HelperClass;
using UnityEngine;
using UnityEngine.Rendering.Universal.Internal;
using UnityEngine.UIElements;

public class CleanMinigame : MonoBehaviour
{
    Camera _camera;

    [SerializeField] Texture2D _dirtMaskBase;
    [SerializeField] Texture2D _brush;
    [SerializeField] SpriteRenderer sr;
    [SerializeField] Material _material;
    [SerializeField] Material _defaultmaterial;
    Texture2D _templateDirtMask;
    Texture2D _templatePetM0ask;
    Texture2D _templateDirtyPetMask;
         public float cleanAmountTotal;
     public float dirtAmountTotal;
    public float ogdirtAmountTotal;
    bool firstTime;
    float percentage;
    IEnumerator runningCoroutine;
    public Action loaded;
    public Action<float> onCleanEvent;

    public bool inUse;

    public void Initialize(Texture2D pet, Texture2D dirtyPet)
    {
        _camera = gameObject.scene.GetFirstMainCameraInScene();
        _templatePetM0ask = pet;
        _templateDirtyPetMask = dirtyPet;
        _material.SetTexture("_MainTex", _templatePetM0ask);
        _material.SetTexture("_DirtTexture", _templateDirtyPetMask);
        CreateTexture();
        firstTime = true;
        inUse = true;
        sr = GetComponent<SpriteRenderer>();
    }

    public void EndGame()
    {
        sr.material = _defaultmaterial;
    }
    void Update()
    {
        if (inUse)
        {
            if (Input.GetMouseButton(0))
            {
                //var rayOrigin = Camera.main.transform.position;
                //var rayDirection = CameraHelpers.MouseWorldPosition(transform.position);// - Camera.main.transform.position;
                //var rayOrigin = Camera.main.ScreenToWorldPoint(Input.mousePosition); // Camera.main.transform.position;
                //var rayDirection = Vector3.forward;// CameraHelpers.MouseWorldPosition(transform.position);// - Camera.main.transform.position;
                //var old = _camera.ScreenPointToRay(Input.mousePosition);

                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                Vector2 orgin = new Vector2(ray.origin.x, ray.origin.y);
                //RaycastHit2D hit = Physics2D.Linecast(orgin, -Vector2.up, 1 << LayerMask.NameToLayer("Supports"));
                RaycastHit2D hit = Physics2D.Raycast(_camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), Vector3.forward);

                //Debug.DrawRay(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), Vector3.forward, Color.green);
                //RaycastHit2D hit = Physics2D.Raycast(orgin, dir);
                if (hit)
                {
                    if (hit.collider.gameObject == gameObject)
                    {
                        Vector2 textureCoord = hit.point;//hit.point.transform.InverseTransformPoint(hit.point);
                        Bounds bound = hit.collider.bounds;

                        float withoutOffsetX = (bound.max.x - textureCoord.x) * -1 / bound.size.x;
                        float withoutOffsetY = (bound.max.y - textureCoord.y) * -1 / bound.size.y;
                        Vector2 uvpoint = (new Vector2(withoutOffsetX, withoutOffsetY));// + new Vector2(1, 1)) / 2;
                                                                                        // Vector2 uvpoint = (withoutOffset + (Vector2)(bound.size / 2)) / bound.max; //0 - - 0.5
                        int pixelX = ((int)((uvpoint.x) * _templateDirtMask.width) - _brush.width / 2);
                        int pixelY = ((int)((uvpoint.y) * _templateDirtMask.height) - _brush.height / 2);

                        ////Vector2 loc = hit.point 
                        //Vector2 textureCoord = hit.point;//hit.point.transform.InverseTransformPoint(hit.point);
                        //Bounds bound = hit.collider.bounds;
                        //Vector2 withoutOffset = (textureCoord);
                        //Vector2 uvpoint =(withoutOffset + (Vector2)(bound.size / 2)) / bound.size; //0 - - 0.5
                        //int pixelX = (int)((uvpoint.x - _brush.width / 2 )* _templateDirtMask.width);
                        //int pixelY = (int)((uvpoint.y - _brush.height / 2) * _templateDirtMask.height);

                        //Vector2 size = bound.max - bound.min;
                        //Vector2 uvpoint = (textureCoord - (Vector2)bound.min) / size; //0 - - 0.5
                        //int pixelX = (int)(uvpoint.x * _templateDirtMask.width);
                        //int pixelY = (int)(uvpoint.y * _templateDirtMask.height);

                        //Camera.main.WorldToScreenPoint(textureCoord);
                        // Vector2 pixel = uvpoint * _templateDirtMask;
                        //Debug.Log("SIZE = " + bound.size + " " + textureCoord + " = " + bound + " | " + bound.min + " | " + bound.max + " = " + uvpoint + " = " + pixelX + " , " + pixelY + " * [" + _templateDirtMask.width + " , " + _templateDirtMask.height + "] ");
                        for (int x = 0; x < _brush.width; x++)
                        {
                            for (int y = 0; y < _brush.height; y++)
                            {
                                Color pixelDirt = _brush.GetPixel(x, y);
                                Color pixelDirtMask = _templateDirtMask.GetPixel(pixelX + x, pixelY + y);

                                _templateDirtMask.SetPixel(pixelX + x,
                                    pixelY + y,
                                    new Color(0, pixelDirtMask.g * pixelDirt.g, 0));
                            }
                        }

                        _templateDirtMask.Apply();
                       // Refresh();

                    }


                }
            }
        }
       
    }
    IEnumerator RefreshRate()
    {
        yield return new WaitForSeconds(1f);
        cleanAmountTotal = 0f;
        
        for (int x = 0; x < _templateDirtMask.width; x++)
        {
            for (int y = 0; y < _templateDirtMask.height; y++)
            {
                cleanAmountTotal += _templateDirtMask.GetPixel(x, y).g;
            }
        }
        float removed = ogdirtAmountTotal - cleanAmountTotal;
        cleanAmountTotal = removed;
               percentage = (cleanAmountTotal) / dirtAmountTotal;
        Debug.Log(cleanAmountTotal);
        onCleanEvent.Invoke((cleanAmountTotal));
        if (firstTime)
        {
            firstTime = false;
            loaded.Invoke();
        }
        if(percentage > 1)
        {
           
        }
        else
        {
            runningCoroutine = RefreshRate();
            StartCoroutine(runningCoroutine);
        }
        //Refresh();


    }
    void Refresh()
    {
        cleanAmountTotal = 0f;

        for (int x = 0; x < _templateDirtMask.width; x++)
        {
            for (int y = 0; y < _templateDirtMask.height; y++)
            {
                cleanAmountTotal += _templateDirtMask.GetPixel(x, y).g;
            }
        }
        float removed = ogdirtAmountTotal - cleanAmountTotal;
        cleanAmountTotal = removed;
        percentage = (cleanAmountTotal) / dirtAmountTotal;
        Debug.Log(cleanAmountTotal);
        onCleanEvent.Invoke((cleanAmountTotal));
        if (firstTime)
        {
            firstTime = false;
            loaded.Invoke();
        }
        if (percentage > 0.50)
        {

        }
        else
        {

        }
    }
    void CreateTexture()
    {
        // Debug.Log(_dirtMaskBase.width + ","+ _dirtMaskBase.height);
        _templateDirtMask = new Texture2D(_dirtMaskBase.width, _dirtMaskBase.height);
        _templateDirtMask.SetPixels(_dirtMaskBase.GetPixels());
        _templateDirtMask.Apply();


        _material.SetTexture("_DirtMask", _templateDirtMask);
        ogdirtAmountTotal = 0f;
        for (int x = 0; x < _dirtMaskBase.width; x++)
        {
            for (int y = 0; y < _dirtMaskBase.height; y++)
            {
                ogdirtAmountTotal += _dirtMaskBase.GetPixel(x, y).g;
            }
        }
        //Refresh();
        runningCoroutine = RefreshRate();
        StartCoroutine(runningCoroutine);
        //_material.SetTexture("DirtTexture", _templateDirtMask);
    }
}
