using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using HelperClass;

[System.Serializable]
public class Cat
{
    public GameObject catPrefab;
    public List<Sprite> catProgressIcons = new List<Sprite>();
    public Texture2D cleanCat;
    public Texture2D dirtyCat;
}
public class TemporaryCatMinigameManager : MonoBehaviour
{
    public static TemporaryCatMinigameManager instance;

    //[SerializeField] Collider2D spawnThreshold;

    //[SerializeField] Beat currentBeat;
    [SerializeField] BeatUISpawner beatUISpawner;
    [SerializeField] HealthUI healthUI;
    [SerializeField] ScoreUI scoreUI;
    [SerializeField] ProgressionUI progressionUI;
    [SerializeField] ResultsUI resultsUI;

    [SerializeField] List<Transform> stepPositions = new List<Transform>();
    Vector2 defaultPlayerPosition;
    Vector2 playerPosition;



    [SerializeField]
    List<Cat> cats = new List<Cat>();
    Cat currentCat;
    GameObject currentCatGameObject;
    [SerializeField] Transform background;
    [SerializeField] Transform catSpawn;

    [SerializeField] Transform catContainer;
    Camera cam;
    float defaultCamSize;

    [Header("Camera Shake Settings")]
    [SerializeField] float shakePositionDuration = 0.35f;
    [SerializeField] Vector3 shakePositionPower = new Vector3(15f, 5f);
    [SerializeField] int shakePositionVibrato = 10;
    [SerializeField] float shakePositionRandomRange = 10f;
    [SerializeField] bool shakePositionCanFade = true;

    [Header("Camera Zoom In Settings")]
    [SerializeField] private float zoomSizeModifier;
    [SerializeField] private float zoomTime;

    //[Header("Camera Move Forward Settings")]
    //[SerializeField] float targetMoveUpPosition;
    //[SerializeField] float moveUpTime = 1f;
    //[SerializeField] float moveUpHoverTime = 1f;

    //[SerializeField] float targetMoveDownPosition;
    //[SerializeField] float moveDownTime = 1f;
    //[SerializeField] float moveDownHoverTime = 1f;

    //[SerializeField] float moveResetTime = 1f;

    [SerializeField] float delayTime = 1f;

    public delegate IEnumerator CorrectEvent(Vector2 p_origin, float p_distance);
    public static CorrectEvent OnCorrectEvent;
    //public static Action<Vector2,float> OnCorrectEvent;
    public delegate IEnumerator MistakeEvent(Vector2 p_origin);
    public static MistakeEvent OnMistakeEvent;

    bool isWin = false;
    bool isPlaying = true;
    public static Action OnGameOver;

    void Awake()
    {
        instance = this;
        //cam = Camera.main;
        cam = gameObject.scene.GetFirstMainCameraInScene();
        defaultPlayerPosition = cam.transform.position;
        defaultCamSize = cam.orthographicSize;
        //zoomTime = moveUpTime + moveUpHoverTime + moveDownTime;
        zoomTime = 0.85f;

        OnMistakeEvent += Co_Mistake;
        OnCorrectEvent += Co_MoveForwardCamera;

        healthUI.OnHealthDeathEvent += GameEnd;
        progressionUI.OnReachedGoal += GameWon;
        OnGameOver += progressionUI.Finished;
        //background.localScale
    }

    private void Start()
    {
        GameStart();
    }

    void OnDestroy()
    {
        OnMistakeEvent -= Co_Mistake;
        OnCorrectEvent -= Co_MoveForwardCamera;

        healthUI.OnHealthDeathEvent -= GameEnd;
        progressionUI.OnReachedGoal -= GameWon;
        OnGameOver -= progressionUI.Finished;

    }

    public void GameStart()
    {
        resultsUI.Hide();

        if (currentCat != null)
        {
            Destroy(currentCatGameObject);
        }
        currentCat = cats[UnityEngine.Random.Range(0, cats.Count)];
        currentCatGameObject = Instantiate(currentCat.catPrefab);
        currentCatGameObject.transform.position = catSpawn.position;
        currentCatGameObject.transform.parent = catContainer;

        isWin = false;
        isPlaying = true;

        healthUI.OnInitializeEvent.Invoke(3);
        progressionUI.InitializeStats(3, currentCat.catProgressIcons);
        scoreUI.ResetScore();
        playerPosition = defaultPlayerPosition;

        cam.orthographicSize = defaultCamSize;
        cam.transform.position = playerPosition;
        if (isPlaying)
        {
            beatUISpawner.GenerateBeatMap();
        }
    }
    void GameWon()
    {
        isWin = true;
        
        GameEnd();
    }

    IEnumerator Co_GameEndDelay()
    {
   
        StartCoroutine(beatUISpawner.currentBeat.Co_HideAll());
        yield return new WaitForSeconds(3f);
        resultsUI.Show(isWin, scoreUI.currentPoints,400);
    }
    void GameEnd()
    {
        isPlaying = false;
        OnGameOver?.Invoke();
        StartCoroutine(Co_GameEndDelay());
     
        
    }

    IEnumerator Co_MoveForwardCamera(Vector2 p_origin, float p_distance)
    {
   
        HitData p_hitData = beatUISpawner.DetermineHitData(p_distance);
        PopUpUI popUpUI = PopUpUIPool.pool.Get();
        popUpUI.PopIn(p_hitData, p_origin, scoreUI);
        progressionUI.Progress();
        //OnModifyOpacityState.Invoke();
        Vector2 previousPosition = playerPosition;
        playerPosition = stepPositions[(int)progressionUI.currentSteps - 1].position;
        StartCoroutine(CameraHelpers.Co_ZoomCamera(cam, zoomSizeModifier, zoomTime));
        //yield return StartCoroutine(UniversalLibrary.Co_MoveForwardCamera(cam.transform, 
        //    previousPosition, playerPosition, 
        //    targetMoveUpPosition, moveUpTime, moveUpHoverTime,
        //    targetMoveDownPosition, moveDownTime,moveDownHoverTime, 
        //    moveResetTime));
        yield return StartCoroutine(CameraHelpers.Co_MoveForwardCamera(cam.transform,
            previousPosition, playerPosition));
        yield return new WaitForSeconds(1f);
        if (isPlaying)
        {
            beatUISpawner.GenerateBeatMap();
        }
    }

    IEnumerator Co_Mistake(Vector2 p_origin)
    {
   
        HitData p_hitData = beatUISpawner.GetMistakeHitData();
        PopUpUI popUpUI = PopUpUIPool.pool.Get();
        popUpUI.PopIn(p_hitData, p_origin);
        healthUI.OnModifyHealthEvent.Invoke(-1);

        //INDIVIDUAL SHAKE
        var shakeSequence = DOTween.Sequence()
            .Append(cam.DOShakePosition(shakePositionDuration, shakePositionPower, shakePositionVibrato, shakePositionRandomRange, shakePositionCanFade));
    
        shakeSequence.Play();
        yield return shakeSequence.WaitForCompletion();
        cam.transform.position= playerPosition;


        //GROUP SHAKE
        //var sequence = DOTween.Sequence()
        //    .Append(rectTransforms[0].DOShakePosition(shakePositionDuration, shakePositionPower, shakePositionVibrato, shakePositionRandomRange, shakePositionCanFade));
        //for (int i = 1; i < rectTransforms.Count; i++)
        //{
        //    sequence.Join(rectTransforms[i].DOShakePosition(shakeRotationDuration, shakeRotationPower, shakeRotationVibrato, shakeRotationRandomRange, shakeRotationCanFade));
        //}
        //sequence.Play();
        //yield return sequence.WaitForCompletion();
        //for (int i = 0; i < rectTransforms.Count; i++)
        //{
        //    rectTransforms[i].anchoredPosition = savedPositions[i];
        //}

        yield return new WaitForSeconds(1f);
        if (isPlaying)
        {
            beatUISpawner.GenerateBeatMap();
        }
 
    }


}
