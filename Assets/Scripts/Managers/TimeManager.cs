using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager instance;

    [SerializeField] public static float timeModifier = 1.0f;
    public static bool IsPaused { get; set; }


    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    private void Update()
    {
        UpdateTimeStatus();
    }

    public static void UpdateTimeStatus()
    {
        if (PauseController.isPaused)
        {
            //Time.timeScale = 0;
            IsPaused = true;                             
            return;
        }

        Time.timeScale = 1 * timeModifier;
        IsPaused = false;
    }
}
