using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ObjectDisablerOnSceneChange : MonoBehaviour
{
    private void Awake()
    {
        SceneManager.activeSceneChanged += OnActiveSceneChanged;
    }

    private void OnDestroy()
    {
        SceneManager.activeSceneChanged -= OnActiveSceneChanged;
    }
    
    private void OnActiveSceneChanged(Scene current, Scene next)
    {
        if(current != gameObject.scene) return;
        
        gameObject.SetActive(false);
    }
}
