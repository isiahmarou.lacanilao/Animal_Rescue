using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PauseController : MonoBehaviour
{
    public static bool isPaused { get; private set; }

    private void Awake()
    {
        UnpauseGame();
        
    }

    public static void PauseGame()
    {
        isPaused = true;
    }
    public static void UnpauseGame()
    {
        isPaused = false;
    }
}
