using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PlatformerManager : MonoBehaviour
{
    [SerializeField] private Transform lastCheckpoint;
    [SerializeField] private GameObject player;
    [SerializeField] private float respawnWaitTime = 1f;
    private List<Transform> checkpointList = new List<Transform>();

    private void OnEnable()
    {
        EventManager.GlobalEvents.onCheckpointReached.AddListener(OnPlayerCheckpointPass);
        EventManager.GlobalEvents.OnPlayerRespawn.AddListener(OnPlayerRespawn);
    }

    private void OnDisable()
    {
        EventManager.GlobalEvents.onCheckpointReached.RemoveListener(OnPlayerCheckpointPass);
        EventManager.GlobalEvents.OnPlayerRespawn.RemoveListener(OnPlayerRespawn);
    }

    void OnPlayerCheckpointPass(Transform checkpoint)
    {
        if (!checkpointList.Contains(checkpoint))
        {
            checkpointList.Add(checkpoint);
        }

        lastCheckpoint = checkpoint;
    }

    void OnPlayerRespawn() => StartCoroutine(Respawn());

    IEnumerator Respawn()
    {
        player.SetActive(false);

        yield return new WaitForSeconds(respawnWaitTime);
        
        player.transform.position = lastCheckpoint.position;
        player.SetActive(true);
    }
}
