using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Particles
{
    public class GhostTrail : MonoBehaviour
    {
                
        [SerializeField] private int ClonesPerSecond = 30;
        [SerializeField] private Color desiredColor = new Color(0, 0, 0, 1f);
        [SerializeField] private SpriteRenderer runningSprite;
        [SerializeField] private SpriteRenderer playerSprite;

        private SpriteRenderer[] clones;
        private Rigidbody2D rb;
        private Transform cloneParent;
        private float timer = 0;
  

        private bool willFlipX => playerSprite.flipX;

        public void Setup(float dashDur) 
        { 
            DestroyAllClones();
            if(runningSprite == null) runningSprite = transform.GetComponentInChildren<SpriteRenderer>();
            
            rb = GetComponentInParent<Rigidbody2D>();

            cloneParent = new GameObject().transform;
            cloneParent.position = Vector3.zero;
            cloneParent.name = "CloneParent";

            int num = Mathf.RoundToInt(dashDur * ClonesPerSecond);
            clones = new SpriteRenderer[num];
            
            for (int i = 0; i < num; i++)
            {
                SpriteRenderer s = Instantiate(runningSprite, cloneParent, true);
                
                s.transform.localScale = playerSprite.transform.localScale;
                s.sortingOrder = runningSprite.sortingOrder - 1;
                clones[i] = s;
                s.gameObject.SetActive(false);
            }
        }

        void OnEnable()
        {
            StartCoroutine(OnTrail());
        }

        private void OnDisable()
        {
            StopCoroutine(OnTrail());
            DisableClones();
        }

        private void OnDestroy()
        {
            DestroyAllClones();
        }

        void Update()
        {
            timer += Time.deltaTime * 2;
            for (int i = 0; i < clones.Length - 1; i++)
            {
                clones[i].color = Color.Lerp(clones[i].color, desiredColor, timer);

                if (clones[i].color.a <= 0f || clones[i].transform.localScale == Vector3.zero)
                {
                    clones[i].gameObject.SetActive(false);
                    i--;
                }
            }
        }

        IEnumerator OnTrail()
        {
            for (int i = 0; i < clones.Length - 1; i++)
            {
                // if (rb.velocity == Vector2.zero) break;
                
                clones[i].transform.position = transform.position;
                clones[i].flipX = willFlipX;
                clones[i].gameObject.SetActive(true);
                
                yield return new WaitForSeconds(1f / ClonesPerSecond);
            }
            DisableClones();
        }

        void DisableClones()
        {
            foreach (SpriteRenderer c in clones)
            {
                c.gameObject.SetActive(false);
                c.color = Color.white;
            }
        }
        
        public void DestroyAllClones()
        {
            if (!cloneParent) return;
            Destroy(cloneParent.gameObject);
        }
    }
}