using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Temp Add or component ; can be replaced by adding this to movement script when merged is done or used as a component
public class MovementAnimator : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb;

    [SerializeField] private float boredWaitTime = 5f;
    private float boredTime;

    private float lastMoveVel; // For checking last input

    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        CheckBored();
        FaceLastDirection();
        animator.SetBool("isRunning", !Mathf.Approximately(rb.velocity.x, 0));

        animator.SetFloat("ySpeed", rb.velocity.y);
    }

    void FaceLastDirection()
    {
        // Saves previous moved x direction
        if (rb.velocity.x != 0) lastMoveVel = rb.velocity.x;

        if (lastMoveVel > 0) spriteRenderer.flipX = false;
        else spriteRenderer.flipX = true;
    }

    void CheckBored()
    {
        if (rb.velocity.x == 0 && rb.velocity.y == 0)
        {
            boredTime -= Time.deltaTime;

            if (boredTime <= 0)
            {
                Debug.Log("She is bored");
                //Play Animation

                // Repeat Bored Time after short delay of half the initial wait time
                if (boredTime <= 0) boredTime = boredWaitTime;
            }
        }
        else boredTime = boredWaitTime;
    }
}
