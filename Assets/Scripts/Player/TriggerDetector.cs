using System.Collections.Generic;
using UnityEngine;
using EventManager;
using Interactable;

namespace Player
{
    public class TriggerDetector : MonoBehaviour
    {
        [SerializeField] private LayerMask interactableLayers;
        [SerializeField] private LayerMask checkpointLayer;
        [SerializeField] private LayerMask obstacleLayer;
        [SerializeField] private LayerMask treatsLayer;
        
        private Collider2D _collider;
        
        public GameObject nearestInteractable { get; set; }
        
        private void EnableCollider(bool isInteracting) => _collider.enabled = !isInteracting;

        #region Unity Functions

        private void Awake()
        {
            _collider = GetComponent<Collider2D>();
        }

        private void OnEnable()
        {
            _collider.enabled = true;
            GlobalEvents.OnPlayerInteracting.AddListener(EnableCollider);
            nearestInteractable = null;
        }

        private void OnDisable()
        {
            _collider.enabled = false;
            GlobalEvents.OnPlayerInteracting.RemoveListener(EnableCollider);
        }

        #endregion

        private void OnTriggerEnter2D(Collider2D col)
        {
            GameObject collidedGameObject = col.gameObject;
            
            // for interaction
            if (((1 << collidedGameObject.layer) & interactableLayers) != 0)
            {
                // int prevObjID = interactableObject ? interactableObject.GetInstanceID() : 0;
                //
                // if (prevObjID == collidedGameObject.GetInstanceID()) return;

                if (!collidedGameObject.GetComponent<InteractableBase>()) return;

                nearestInteractable = collidedGameObject;
                GlobalEvents.OnPlayerNearInteractable.Invoke(true, nearestInteractable);
                return;
            }

            // for checkpoint
            if (((1 << collidedGameObject.layer) & checkpointLayer) != 0)
            {
                GlobalEvents.onCheckpointReached.Invoke(collidedGameObject.transform);
                return;
            }
            
            // for obstacle
            if (((1 << collidedGameObject.layer) & obstacleLayer) != 0)
            {
                GlobalEvents.OnPlayerRespawn.Invoke();
            }
            
            //for treats
            if (((1 << collidedGameObject.layer) & treatsLayer) != 0)
            {
                PlayerDataEvents.OnAddTreats.Invoke(1);
                Destroy(collidedGameObject);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            GameObject exitedObject = other.gameObject;
            if (((1 << exitedObject.layer) & interactableLayers) == 0) return;
            
            if(!exitedObject.TryGetComponent(out InteractableBase interactable)) return;
            
            GlobalEvents.OnPlayerNearInteractable.Invoke(false, nearestInteractable);
            nearestInteractable = null;
        }

        // public GameObject GetNearestInteractable()
        // {
        //     GameObject closest = null;
        //     float closestSqrDist = 0f;
        //
        //     // early returns
        //     if (interactableList.Count == 0) return null;
        //     if (interactableList.Count == 1) return interactableList[0];
        //
        //     foreach(var gameObject in interactableList) 
        //     {
        //         //sqrMagnitude because it's faster to calculate than magnitude
        //         float sqrDist = (gameObject.transform.position - transform.position).sqrMagnitude; 
        //
        //         if (!closest || sqrDist < closestSqrDist) 
        //         {
        //             closest = gameObject;
        //             closestSqrDist = sqrDist;
        //         }
        //     }
        //     
        //     return closest;
        // }
        //
        // public System.Collections.IEnumerator CheckNearest()
        // {
        //     while (enabled && gameObject.activeSelf)
        //     {
        //         yield return JerryLibrary.TimeHelpers.GetWait(0.1f);
        //         OnTick();
        //     }
        // }
        //
        // private void OnTick()
        // {
        //     GameObject tempNearest = GetNearestInteractable();
        //     
        //     // early return if it is not new
        //     if (tempNearest == nearestInteractable)
        //     {
        //         return;
        //     }
        //         
        //     nearestInteractable = tempNearest;
        //         
        //     if(nearestInteractable == null) return;
        //
        //     GlobalEvents.OnPlayerNearInteractable.Invoke(true, nearestInteractable);
        // }
    }
}
