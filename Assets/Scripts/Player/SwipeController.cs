using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum SwipeDirection
{
	Up = 0,
	Down = 1,
	Left = 2,
	Right = 3,
}


public class SwipeController : MonoBehaviour
{
	private Vector2 startTouchPos;
	private Vector2 endTouchPos;

	public UnityEvent<SwipeDirection> OnSwipe;

    private void Update()
    {
		ReadSwipe();
    }

    private SwipeDirection ReadSwipe()
    {
		SwipeDirection swipeDirection = new SwipeDirection();

		// Start Swipe
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
			startTouchPos = Input.GetTouch(0).position;
        }

		// Swipe Check
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {

			endTouchPos = Input.GetTouch(0).position;

			// Vertical //

			// Swipe Up
			if (endTouchPos.y > startTouchPos.y)
			{
				swipeDirection = GetSwipe(SwipeDirection.Up);
			}
			// Swipe Down
			if (endTouchPos.y < startTouchPos.y)
			{
				swipeDirection = GetSwipe(SwipeDirection.Down);
			}

			// Horizontal //

			// Swipe Left
			if (endTouchPos.x < startTouchPos.x)
            {
				swipeDirection = GetSwipe(SwipeDirection.Left);	
            }
			// Swipe Right
			if (endTouchPos.x > startTouchPos.x)
			{
				swipeDirection = GetSwipe(SwipeDirection.Right);
			}

		}
		// Return and invoke Swipe direction
		OnSwipe.Invoke(swipeDirection);
		return swipeDirection;
    }

	private SwipeDirection GetSwipe(SwipeDirection direction)
	{
		return direction;
	}
}
