using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
// using Baracuda.Monitoring;
using DG.Tweening;
using HelperClass;

namespace Player
{
    [System.Serializable]
    public struct ControllerInput
    {
        public float X, Y;
        public int RawX, RawY;
    }

    public enum PlayerState
    {
        Idle = 0, 
        Running = 1,
        Jumping = 2,
        Falling = 3,
        Dashing = 4,
        Climbing = 5,
        Interacting = 6
    }

    // will delete [Monitor] properties
    public class PlayerController : MonoBehaviour
    {
        /*[Monitor]*/ public PlayerState currentState { get; private set; }

        #region Components
        
        [Header("Components")]
        
        [SerializeField] private PlayerGrounded groundedCol;
        [SerializeField] private Particles.GhostTrail ghostTrail;
        [SerializeField] private TriggerDetector triggerDetector;
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private Transform leftSide;
        [SerializeField] private Transform rightSide;

        private Animator animator;
        private ControllerInput inputs;
        private Collider2D _collider2D;

        #endregion

        #region General Movement Properties

        [Header("Movement Properties")] 
        [SerializeField] private float movementSpeed = 8;
        [SerializeField] private float acceleration = 6;
        [SerializeField] private float boredWaitTime = 5f;

        private float currentMoveLerpSpeed = 100;
        /*[Monitor]*/ private bool facingLeft;
        /*[Monitor] */private bool canMove;
        private float boredTime;
        private float defaultMoveLerpSpeed;

        /*[Monitor]*/ private bool isGrounded => groundedCol.isGrounded;
        /*[Monitor]*/ private bool prevIsGrounded;
        /*[Monitor]*/ private Vector2 playerVel
        {
            get => rb.velocity;
            set => rb.velocity = value;
        }
        
        #endregion

        #region Jump Properties

        [Header("Jump Physics Properties")] 
        
        [SerializeField] private float jumpForce = 16;
        [SerializeField] private float fallMult = 4;
        [SerializeField] private float jumpVelFallOff = 8;

        [Header("Jump General Properties")] 
        [SerializeField] private float jumpBufferTime = 0.1f;
        [SerializeField] private float coyoteTime = 0.1f;
        [SerializeField] private float jumpTimeDelay = 0.1f;

        private float timeJumpPressed;
        /*[Monitor]*/ private float timeLastLanded;
        /*[Monitor]*/ private float timeLastGrounded;
        /*[Monitor]*/ private float timeLastJump;
        /*[Monitor]*/ private bool hasLanded;

        #endregion

        #region Dash Properties

        [Header("Dash Properties")] 
        [SerializeField] private float dashSpeed = 30f;
        [SerializeField] private float dashDuration = 0.1f;
        [SerializeField] private float dashCooldown = 0.5f;

        private bool hasDashed;
        private float timeLastDashed;
        private float defaultGravityScale;
        private Vector2 dashDir;

        #endregion

        #region Particles
        
        [Header("Particles")]
        [SerializeField] private ParticleSystem runParticles;
        [SerializeField] private ParticleSystem jumpParticles;
        [SerializeField] private ParticleSystem dashParticle;
        
        #endregion

        #region Animation string Hash

        private int _ySpeedHash;
        private int _groundedHash;
        private int _isRunningHash;

        #endregion

        private IEnumerator playerRoutine;
        
        #region for Debugging, to Delete

        /*[Monitor]*/ private Vector2 modifiedInput => new Vector2(inputs.X, inputs.Y);
        /*[Monitor]*/ private Vector2 RawInput => new Vector2(inputs.RawX, inputs.RawY);

        #endregion

        #region Unity Functions

        private void Awake()
        {
            if(rb == null) rb = GetComponent<Rigidbody2D>();
            if(groundedCol == null) groundedCol = GetComponentInChildren<PlayerGrounded>();
            if(triggerDetector == null) triggerDetector = GetComponentInChildren<TriggerDetector>();
            if(spriteRenderer == null) spriteRenderer = GetComponentInChildren<SpriteRenderer>();
            if(ghostTrail == null) ghostTrail = GetComponentInChildren<Particles.GhostTrail>();
            _collider2D = GetComponent<Collider2D>();
            animator = GetComponent<Animator>();
            
            // set default values
            canMove = true;
            defaultGravityScale = rb.gravityScale;
            boredTime = boredWaitTime;
            defaultMoveLerpSpeed = currentMoveLerpSpeed;

            // get hash
            _isRunningHash = Animator.StringToHash("isRunning");
            _ySpeedHash = Animator.StringToHash("ySpeed");
            _groundedHash = Animator.StringToHash("Grounded");
        }
        private void OnEnable()
        {
            StopPlayer(false);
            EventManager.GlobalEvents.OnPlayerClimbLadder.AddListener(OnClimbLadder);
            ghostTrail.Setup(dashDuration);
            // to delete
            /*Baracuda.Monitoring.Monitor.StartMonitoring(this);*/
        }

        private void OnDisable()
        {
            EventManager.GlobalEvents.OnPlayerClimbLadder.RemoveListener(OnClimbLadder);
            StopPlayer(true);
            ghostTrail.DestroyAllClones();
            // to delete
            /*Baracuda.Monitoring.Monitor.StopMonitoring(this);*/
        }

        private void Update()
        {
            if(!canMove) return;
            MoveUpdate();
            JumpUpdate();
            FallUpdate();
            UpdateValues();
            AnimationUpdate();
        }
        
        #endregion

        #region General Functions

        private void ChangeState(PlayerState state)
        {
            if (isGrounded && state == PlayerState.Idle)
            {
                state = inputs.RawX == 0 ? PlayerState.Idle : PlayerState.Running;
            }
            
            if(currentState == state) return;
            currentState = state;
        }
        
        private void ResetPlayerMovement()
        {
            // reset inputs;
            inputs.X = 0;
            inputs.Y = 0;
            inputs.RawX = 0;
            inputs.RawY = 0;

            // reset velocity
            rb.velocity = Vector2.zero;
            
            // reset player state
            ChangeState(PlayerState.Idle);
            
            StopPlayerRoutine();
            playerRoutine = null;
        }
        
        private void StopPlayer(bool willStop)
        {
            canMove = !willStop;
            _collider2D.enabled = !willStop;
            rb.gravityScale = willStop ? 0 : defaultGravityScale;
            
            ResetPlayerMovement();
        }

        private void StopPlayerRoutine()
        {
            if(playerRoutine != null) StopCoroutine(playerRoutine);
        }
        
        #endregion
        
        #region Movement Inputs Functions
        
        public void RunInput(InputAction.CallbackContext context)
        {
            if(!canMove) return;
            
            inputs.RawX = (int) context.ReadValue<Vector2>().x;
            inputs.RawY = (int) context.ReadValue<Vector2>().y;
            inputs.X = context.ReadValue<Vector2>().x;
            inputs.Y = context.ReadValue<Vector2>().y;

            bool isPrevLeft = facingLeft;
            facingLeft = inputs.X.IsApproximatelyTo(0) ? facingLeft : inputs.X < 0;
        
            // if change of orientation
            if(isPrevLeft != facingLeft && isGrounded) runParticles.Play();
            
            spriteRenderer.flipX = facingLeft;
        }

        public void JumpInput(InputAction.CallbackContext context)
        {
            // Early returns
            if(!context.started) return;
            if(!canMove) return;

            timeJumpPressed = Time.time;
        }

        public void DashInput(InputAction.CallbackContext context)
        {
            // Early returns
            if(!canMove) return;
            if(!context.performed) return;
            if(hasDashed) return;
            if(Time.time < timeLastDashed + dashCooldown) return;
            if (currentState != PlayerState.Idle &&
                currentState != PlayerState.Running &&
                currentState != PlayerState.Jumping) return;

            StopPlayerRoutine();
            playerRoutine = OnDash();
            StartCoroutine(playerRoutine);
        }
        
        public void InteractInput(InputAction.CallbackContext context)
        {
            if(!context.started) return;
            GameObject interObj = triggerDetector.nearestInteractable;
            
            if (interObj != null &&
                interObj.TryGetComponent(out Interactable.InteractableBase interact))
            {
                interact.OnInteract();
                return;
            } 
        
            EventManager.GlobalEvents.OnPlayerNearInteractable.Invoke(false, interObj);
        }

        #endregion
        
        #region Additional Events
        
        void OnClimbLadder(Interactable.Ladder_Interactable ladderInteractable)
        {
            rb.position = new Vector2(ladderInteractable.transform.position.x, rb.position.y);
            float speed = movementSpeed * ladderInteractable.SpeedModifier;
            StopPlayer(true);
            EventManager.GlobalEvents.OnPlayerInteracting.Invoke(true);
            
            Tweener tween = rb.
                DOMove(ladderInteractable.EndPoint.position, speed).
                SetSpeedBased().
                SetEase(Ease.Linear);
            
            tween.onKill += () =>
            {
                StopPlayer(false);
                ChangeState(PlayerState.Idle);
            
                EventManager.GlobalEvents.OnPlayerInteracting.Invoke(false);
            };
        }

        #endregion

        #region Movement Update

        private void UpdateValues()
        {
            if (isGrounded)
            {
                if(!prevIsGrounded) timeLastLanded = Time.time;
                timeLastGrounded = Time.time;
                currentMoveLerpSpeed = defaultMoveLerpSpeed;
                hasDashed = false;
                
                if (currentState != PlayerState.Jumping && 
                    !groundedCol.isOnMovingPlatform)
                {
                    playerVel = new Vector2(playerVel.x, 0);
                }
                
                if ((currentState == PlayerState.Jumping && 
                     Time.time > timeLastJump + jumpTimeDelay) ||
                    currentState == PlayerState.Dashing)
                {
                    if(!groundedCol.isOnMovingPlatform) playerVel = new Vector2(playerVel.x, 0);
                    ChangeState(PlayerState.Idle);
                }
            }

            prevIsGrounded = isGrounded;
            OnMovingPlatform();
            playerVel = new Vector2(rb.velocity.x, Mathf.Clamp(playerVel.y, -50, 50));
        }
        
        private void OnMovingPlatform()
        {
            
            if (!groundedCol.isOnMovingPlatform)
            {
                // if (rb.interpolation != RigidbodyInterpolation2D.Interpolate)
                // {
                //     rb.interpolation = RigidbodyInterpolation2D.Interpolate;
                // }

                transform.SetParent(null);
                return;
            }

            // if (rb.interpolation != RigidbodyInterpolation2D.Extrapolate)
            // {
            //     rb.interpolation = RigidbodyInterpolation2D.Extrapolate;
            // }


            transform.SetParent(
                currentState == PlayerState.Idle && 
                rb.velocity == Vector2.zero ? 
                groundedCol.platformCollider.transform : null);
        }
        
        private void MoveUpdate()
        {
            float newAcceleration = isGrounded ? acceleration : acceleration / 2f;
            
            switch (inputs.X)
            {
                case < 0:
                {
                    if (playerVel.x > 0) inputs.X = 0;
                    inputs.X = Mathf.MoveTowards(inputs.X, -1, newAcceleration * Time.deltaTime);
                    break;
                }
                case > 0:
                {
                    if (playerVel.x < 0) inputs.X = 0;
                    inputs.X = Mathf.MoveTowards(inputs.X, 1, newAcceleration * Time.deltaTime);
                    break;
                }
                default:
                {
                    inputs.X = Mathf.MoveTowards(inputs.X, 0, newAcceleration * 2 * Time.deltaTime);
                    break;
                }
            }

            Vector3 newVelocity = new Vector3(inputs.X * movementSpeed, playerVel.y);
            rb.velocity = Vector2.MoveTowards(playerVel, newVelocity, currentMoveLerpSpeed * Time.deltaTime);
            
            if(!isGrounded || currentState == PlayerState.Jumping) return;
            ChangeState(PlayerState.Idle);
        }

        private void JumpUpdate()
        {
            if(Time.time > timeJumpPressed + jumpBufferTime) return;
            if (Time.time > timeLastGrounded + coyoteTime) return;
            
            if (currentState != PlayerState.Idle &&
                currentState != PlayerState.Running) return;
            
            playerVel = new Vector2(playerVel.x, jumpForce);
            ChangeState(PlayerState.Jumping);
            timeLastJump = Time.time;
            jumpParticles.Play();
        }
        
        
        private void FallUpdate()
        {
            if(isGrounded) return;
            if(currentState == PlayerState.Dashing) return;
            
            if (rb.velocity.y < jumpVelFallOff || rb.velocity.y > 0)
            {
                rb.velocity += Vector2.up * (fallMult * Physics2D.gravity.y * Time.deltaTime);
            }
        }

        private void AnimationUpdate()
        {
            animator.SetBool(_isRunningHash, !playerVel.x.IsApproximatelyTo(0));
            animator.SetFloat(_ySpeedHash, playerVel.y);
            animator.SetBool(_groundedHash, isGrounded);
            
            
            // check if bored
            if (currentState == PlayerState.Idle)
            {
                boredTime -= Time.deltaTime;
                if (boredTime <= 0)
                {
                    //Debug.Log("She Bored");
                    //Play Animation

                    // Repeat Bored Time after short delay of half the initial wait time
                    //if (boredTime <= 0) boredTime = boredWaitTime;
                }
            }
            else boredTime = boredWaitTime;
        }
        
        #endregion

        #region Coroutines

        IEnumerator OnDash()
        {
            ChangeState(PlayerState.Dashing);
            dashDir = new Vector2(inputs.X, 0).normalized;
            if (dashDir == Vector2.zero) dashDir = facingLeft ? Vector2.left : Vector2.right;

            // Dash
            rb.gravityScale = 0;
            ghostTrail.enabled = true;
            playerVel = dashDir * dashSpeed;
            hasDashed = true;
            
            // Particles
            Transform particleTransform = dashParticle.transform;
            
            particleTransform.position = facingLeft
                ? leftSide.position
                : rightSide.position;
            
            Quaternion dashParticleRot = particleTransform.rotation;
            
            particleTransform.rotation = facingLeft
                ? new Quaternion(dashParticleRot.x, dashParticleRot.y, 270, dashParticleRot.w)
                : new Quaternion(dashParticleRot.x, dashParticleRot.y, 90, dashParticleRot.w);
            
            dashParticle.Play();

            yield return TimeHelpers.GetWait(dashDuration);

            // After Dash
            ghostTrail.enabled = false;
            
            float yValue = Mathf.Clamp(rb.velocity.y, -10, 10);
            playerVel = new Vector2(rb.velocity.x, yValue / 2);
            rb.gravityScale = defaultGravityScale;
            timeLastDashed = Time.time;
            
            ChangeState(PlayerState.Idle);
        }
        #endregion
    }
}
 