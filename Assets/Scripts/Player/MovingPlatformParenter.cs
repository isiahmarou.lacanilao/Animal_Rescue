using Baracuda.Monitoring;
using UnityEngine;

namespace Player
{
    public class MovingPlatformParenter : MonoBehaviour
    {
        [SerializeField] private LayerMask movingLayer;
        private Transform player;
        private Rigidbody2D rb;
        private Player.PlayerGrounded grounded;
        private Collider2D col;
        [Monitor] private bool onMovingPlatform;
        private bool isTouchingPlayer;

        private void Awake()
        {
            player = this.transform;
            rb = player.GetComponent<Rigidbody2D>();
            grounded = GetComponentInChildren<Player.PlayerGrounded>();
            onMovingPlatform = false;
            col = GetComponent<Collider2D>();
            Monitor.StartMonitoring(this);
        }

        private void Update()
        {
            if (!grounded.isGrounded)
            {
                if (!onMovingPlatform) return;
                player.SetParent(null);
                onMovingPlatform = false;
                return;
            }

            Collider2D bottomObject = grounded.platformCollider; 
            
            
            if (!bottomObject) return;
            

            isTouchingPlayer = Physics2D.IsTouching(col, bottomObject);
            if (!isTouchingPlayer)
            {
                player.SetParent(null);
                onMovingPlatform = false;
                return;
            }

            bool isIdle = Mathf.Approximately(rb.velocity.x, 0);

            if (bottomObject.gameObject.CompareTag("MovingPlatform") && isIdle)
            {
                player.SetParent(bottomObject.transform);
                onMovingPlatform = true;
            }
            else if (onMovingPlatform || !isIdle)
            {
                player.SetParent(null);
                onMovingPlatform = false;
            }
        }
    }
}
