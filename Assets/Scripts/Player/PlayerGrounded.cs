using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Player
{
    public class PlayerGrounded : MonoBehaviour
    {
        [SerializeField] private LayerMask platformLayers;
        [SerializeField] private LayerMask movingPlatLayer;
        [SerializeField] private Vector2 boxSize;
        
        public Collider2D platformCollider { get; private set; }
        
        public bool isGrounded { get; private set; }
        public bool isOnMovingPlatform { get; private set; }
        public bool showBox;

        #region Ground Detection
        

        void Update()
        {
            CheckGrounded();
        }
        
        private void CheckGrounded()
        {
            Collider2D col = Physics2D.OverlapBox(transform.position, boxSize, 0,platformLayers);

            if (!col)
            {
                isOnMovingPlatform = false;
                isGrounded = false;
                platformCollider = null;
                return;
            }
            
            isGrounded = true;
            
            if (((1 << col.gameObject.layer) & movingPlatLayer) != 0)
            {
                isOnMovingPlatform = true;
                platformCollider = col;
            }
            else
            {
                isOnMovingPlatform = false;
                platformCollider = null;
            }
        }
        
        void OnDrawGizmos()
        {
            if(!showBox) return;
            
            Gizmos.color = Color.red;
            Gizmos.DrawCube(transform.position, boxSize);
        }

        #endregion

    }
}
