using HelperClass;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Timeline;

public class PedestrianSpawner : MonoBehaviour
{
    [SerializeField]
    protected List<SO_PedestrianArchetype> archetypeList = new List<SO_PedestrianArchetype>();

    [SerializeField] protected Transform destination;
    [SerializeField] protected Transform minDestination;

    [SerializeField] protected List<WeightedFloatDatas> waitTimes = new List<WeightedFloatDatas>();
    protected Coroutine handle;
    private void Awake()
    {
  
    }
    public void SpawnPedestrian()
    {
        if (handle != null) StopCoroutine(handle);
        for (int i=0; i < waitTimes.Count; i++)
        {
            waitTimes[i].maxChance = MathHelpers.AddAll(waitTimes[i]);
        }
  
        handle = StartCoroutine(Co_SpawnPedestrian());
        //StartCoroutine(Co_GenerateBeatMap());
    }
    public void StopSpawnPedestrian()
    {
        if (handle != null) StopCoroutine(handle);


    }
    protected virtual IEnumerator Co_SpawnPedestrian()
    {

        Pedestrian pedestrian = PedestrianPool.pool.Get();
        List<Vector2> destinations = new List<Vector2>();
        destinations.Add(destination.position);
     
        int rnd = Random.Range(0, archetypeList.Count);
        SO_PedestrianArchetype archetype = archetypeList[rnd];
        Debug.Log(gameObject.name + " - " + archetype.name);
        if (archetype.idlingOptions.Count > 0)
        {
            Debug.Log(gameObject.name + " - " + archetype.idlingOptions.Count);
            int rndD = Random.Range(1, archetype.idlingOptions.Count);
            if (rndD > 0)
            {
                Debug.Log(gameObject.name + " - " + rndD);
                for (int i = 0; i < rnd; i++)
                {
                    Debug.Log(gameObject.name + " - " + i);
                    float vecX = Random.Range(destination.position.x, minDestination.position.x);
                    float vecY = Random.Range(destination.position.y, minDestination.position.y);

                    Vector2 vec = new Vector2(vecX, vecY);
                    Debug.Log(gameObject.name + " - " + i + " + " + vec);
                    destinations.Add(vec);
                }
            }
        }
        float ran = Random.Range(archetype.minSpeed, archetype.maxSpeed);
        destinations.Reverse();
      
        pedestrian.Initialize(ran, destinations, new List<IdlingData>(archetype.idlingOptions),transform.position);
        float wf = 0;
        if (PedestrianPool.instance.GetPercentage() > 0 && PedestrianPool.instance.GetPercentage() <= 0.5)
        {
            wf = MathHelpers.ChooseWeightedFloat(waitTimes[0]);
            
        }
        else if (PedestrianPool.instance.GetPercentage() > 0.5 && PedestrianPool.instance.GetPercentage() <= 0.75)
        {
            wf = MathHelpers.ChooseWeightedFloat(waitTimes[1]);
        }
        else if (PedestrianPool.instance.GetPercentage() > 0.75 && PedestrianPool.instance.GetPercentage() <= 1)
        {
            wf = MathHelpers.ChooseWeightedFloat(waitTimes[2]);
        }
        yield return new WaitForSeconds(wf);
        StartCoroutine(Co_SpawnPedestrian());

    }
   
}
