using HelperClass;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class BeatUISpawner : MonoBehaviour
{
    [SerializeField] Vector2 spawnPositionOffset;
   // [SerializeField] Vector2 spawnThreshold;

    [SerializeField] public Beat currentBeat;
    [SerializeField] WeightedFloatDatas potentialStartingSizes;
    [SerializeField] float accuracySizeOffset;
    [SerializeField] List<HitData> hitDatas = new List<HitData>();
    [SerializeField] RectTransform canvas;

    [SerializeField] float delay;
//#if UNITY_EDITOR

//    private void OnDrawGizmos()
//    {
//        Gizmos.color = Color.red;
//        float x = (canvas.sizeDelta.x - spawnThreshold.x) * canvas.localScale.x;
//        float y = (canvas.sizeDelta.y - spawnThreshold.y) * canvas.localScale.y;
//        Vector2 cameraPos = canvas.position;
//        Gizmos.DrawWireCube(new Vector2(cameraPos.x+ spawnPositionOffset.x,cameraPos.y+ spawnPositionOffset.y),  new Vector2(x,y));
       
//    }
//    #endif
    public void GenerateBeatMap()
    {

        StartCoroutine(Co_GenerateBeatMap());
    }
    IEnumerator Co_GenerateBeatMap()
    {
        yield return new WaitForSeconds(delay);
        //float x = (canvas.sizeDelta.x - spawnThreshold.x) * canvas.localScale.x;
        //float y = (canvas.sizeDelta.y - spawnThreshold.y) * canvas.localScale.y;
        float x = canvas.rect.width;
        float y = canvas.rect.height;
        float chosenStartingSize = MathHelpers.ChooseWeightedFloat(potentialStartingSizes);
        float spawnX = UnityEngine.Random.Range((-x / 2) + chosenStartingSize, (x / 2) - chosenStartingSize);
        float spawnY = UnityEngine.Random.Range((-y / 2) + chosenStartingSize, (y / 2) - chosenStartingSize);
        //float spawnX = UnityEngine.Random.Range((-x / 2), (x / 2));
        //float spawnY = UnityEngine.Random.Range((-y / 2), (y / 2));


        Vector2 randomPositionOnScreen = new Vector2(spawnX, spawnY);

        currentBeat.Initialize(chosenStartingSize, randomPositionOnScreen, accuracySizeOffset);
    }
    public HitData DetermineHitData(float p_distance)
    {
        return hitDatas.Where(currentHitData => p_distance >= currentHitData.requiredBeatSize).LastOrDefault();
       
    }

    public HitData GetMistakeHitData()
    {
        return hitDatas[hitDatas.Count - 1];
    }
}
