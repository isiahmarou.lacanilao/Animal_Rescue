using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Taken Online but modified
public class Debug_MouseTouch : MonoBehaviour
{
    [SerializeField] private SwipeController swipeController;
    private Vector2 firstPressPos;
    private Vector2 secondPressPos;
    private Vector2 currentSwipe;
    private void Awake()
    {
        swipeController = GetComponent<SwipeController>();
    }

    private void Update()
    {
        Swipe();
    }

    public SwipeDirection Swipe()
    {
        SwipeDirection direction = new SwipeDirection();

        if (Input.GetMouseButtonDown(0))
        {
            //save began touch 2d point
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            //save ended touch 2d point
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //create vector from the two points
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            //normalize the 2d vector
            currentSwipe.Normalize();
        
            if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                direction = SwipeDirection.Up;
            }
            if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                direction = SwipeDirection.Down;
            }
            if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                Debug.Log("mouse left swipe");
                direction = SwipeDirection.Left;
            }
            if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                Debug.Log("mouse right swipe");
                direction = SwipeDirection.Right;
            }
        }

        swipeController.OnSwipe.Invoke(direction);

        return direction;
    }
}