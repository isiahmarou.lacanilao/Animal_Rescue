using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if (UNITY_EDITOR)

public class DebugScript : MonoBehaviour
{
    [Header("Finish MiniGame Debug")]
    [SerializeField] private PetType _petType = PetType.Cat;
    [SerializeField] private int score = 400;

    [Header("Treats Debug")] 
    [SerializeField] private int treatsAmount;

    [ContextMenu("Finish MiniGame")]
    void FinishMiniGame()
    {
        EventManager.PlayerDataEvents.OnCatchSuccess.Invoke(_petType, score);
    }
    
    [ContextMenu("Add Treats")]
    void AddTreats()
    {
        EventManager.PlayerDataEvents.OnAddTreats.Invoke(treatsAmount);
    }
    
    [ContextMenu("Remove Treats")]
    void RemoveTreats()
    {
        EventManager.PlayerDataEvents.OnAddTreats.Invoke(treatsAmount * -1);
    }
}

#endif
