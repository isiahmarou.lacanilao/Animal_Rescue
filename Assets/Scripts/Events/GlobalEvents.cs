namespace EventManager
{
    public static class GlobalEvents
    {
        // PlayerEvents
        public static readonly Evt OnGameOver = new Evt();
        public static readonly Evt OnPlayerDies = new Evt();
        public static readonly Evt<UnityEngine.Transform> onCheckpointReached = new Evt<UnityEngine.Transform>();
        public static readonly Evt OnPlayerRespawn = new Evt();

        // General Interaction Events
        public static readonly Evt<bool, UnityEngine.GameObject> OnPlayerNearInteractable = new Evt<bool, UnityEngine.GameObject>();
        public static readonly Evt<bool> OnPlayerInteracting = new Evt<bool>();
 

        // Specific Interaction Events
        public static readonly Evt<Interactable.Ladder_Interactable> OnPlayerClimbLadder = new Evt<Interactable.Ladder_Interactable>();

        public static readonly Evt<string, PersistentManager.LoaderType> OnLoadScene = new Evt<string, PersistentManager.LoaderType>();
        
        
        // Camera Events
        public static readonly Evt<Cinemachine.CinemachineBrain> OnCameraSwitchStart = new Evt<Cinemachine.CinemachineBrain>();
        public static readonly Evt<Cinemachine.CinemachineBrain> OnCameraSwitchEnd = new Evt<Cinemachine.CinemachineBrain>();
    }
}
