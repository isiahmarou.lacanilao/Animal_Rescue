
using System;

namespace EventManager
{
    public static class PlayerDataEvents
    {
        #region Treats Events
        
        
        /// <summary>
        /// You can use this to remove treats. just use negative int
        /// </summary>
        public static readonly Evt<int> OnAddTreats = new Evt<int>();

        
        #endregion

        #region Mini Game Events

        /// <summary>
        /// Called after finishing a minigame for catching a pet in a level (Call only once)
        /// PetType = What kind of pet that minigame is for
        /// int = score for the minigame
        /// </summary>
        public static readonly Evt<PetType, int> OnCatchSuccess = new Evt<PetType, int>();

        /// <summary>
        /// Called after cleaning a pet
        /// PetType = What kind of pet that was cleaned
        /// int = score
        /// </summary>
        public static readonly Evt<PetType, int> OnPetCleaned = new Evt<PetType, int>();

        /// <summary>
        /// Called after feeding a pet
        /// PetType = What kind of pet that was fed
        /// int = score
        /// </summary>
        public static readonly Evt<PetType, int> OnPetFed = new Evt<PetType, int>();


        #endregion
    }
}
