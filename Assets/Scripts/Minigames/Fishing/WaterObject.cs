using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterObject : MonoBehaviour
{
    [SerializeField] private float objectSpeed;
    private Rigidbody2D rb;
    private bool isCollisionEnabled = false;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody2D>();

        FishingGameManager.instance.OnGameEnd += () =>
        {
            Destroy(this.gameObject);
        };
    }

    protected void Start()
    {

        rb.velocity = new Vector2(0, objectSpeed);
        StartCoroutine(DelayCollisionCheck());
    }


    private IEnumerator DelayCollisionCheck() // triggers at the start of game start, not sure why
    {
        yield return new WaitForSeconds(1f);
        isCollisionEnabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isCollisionEnabled) CollisionAction(collision.gameObject);
    }

    protected virtual void CollisionAction(GameObject gameObject)
    {
        Debug.Log("Base Class: " + gameObject);
    }
}
