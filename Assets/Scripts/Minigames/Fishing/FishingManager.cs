using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// Net ManageR?
public class FishingManager : MonoBehaviour
{
    [SerializeField]    private List<Transform> columnPoints; // The points where the net will appear within the rows.
    [SerializeField]    private SwipeController playerSwipeController;
    [SerializeField]    private GameObject      playerObject;

    private int currentColumn = 1; // Start Center; Point 2

    private void Awake()
    {
        playerSwipeController.OnSwipe.AddListener(MoveNet);
    }

    private void MoveNet(SwipeDirection direction)
    {
        if (direction == SwipeDirection.Left)
        {
            MoveToPosition(playerObject.transform, -1);
        }

        else if (direction == SwipeDirection.Right)
        {
            MoveToPosition(playerObject.transform, 1);
        }
    }

    public void MoveToPosition(Transform objectToMove,int posChange)
    {
        Debug.Log("Current Index:" + currentColumn + " New Index:" + (currentColumn + posChange));

        // if it goes above or below
        if (currentColumn + posChange > columnPoints.Count - 1 || currentColumn + posChange < 0) return;

        currentColumn += posChange;

        objectToMove.SetParent(columnPoints[currentColumn]);
        objectToMove.localPosition = new Vector2(0, 0); // Reset Pos to 0 
    }

    public GameObject GetPlayerObject()
    {
        return playerObject;
    }
}
