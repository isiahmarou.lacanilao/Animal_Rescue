using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SpriteRandomizer : MonoBehaviour
{
    [SerializeField] private List<Sprite> spriteList;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }


    void Start()
    {
        spriteRenderer.sprite = spriteList[Random.Range(0, spriteList.Count)];
    }
}
