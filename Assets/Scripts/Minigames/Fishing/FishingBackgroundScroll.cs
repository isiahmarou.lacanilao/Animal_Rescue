using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingBackgroundScroll : MonoBehaviour
{
    [SerializeField] private float targetYPosition;
    private Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        rb.velocity = new Vector2(0, ComputeSpeed());
    }

    private void Update()
    {

        if (this.transform.position.y >= targetYPosition || PauseController.isPaused)
        {
            rb.velocity = Vector2.zero;
        }
        else
        {
            rb.velocity = new Vector2(0, ComputeSpeed());
        }
    }

    private float ComputeSpeed()
    {
        float newSpeed = (targetYPosition /  FishingGameManager.instance.TotalGameTime ) * 2;

        return newSpeed;
    }


}
