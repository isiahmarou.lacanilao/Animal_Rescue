using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//ComponentScript
public class CollisionDetection : MonoBehaviour
{
    private Collider2D collider;

    [SerializeField] private List<GameObject> objectsFound;
    public UnityEvent OnObjectEnter;

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject objectFound = collision.gameObject;

        objectsFound.Add(objectFound);

        OnObjectEnter.Invoke();
    }

    public GameObject GetObject(int index)
    {
        return objectsFound[index];
    }

    //Clears List
    public void DestroyAllObjects()
    {
        foreach(GameObject gameObject in objectsFound)
        {
            Destroy(gameObject);
        }
    }

}
