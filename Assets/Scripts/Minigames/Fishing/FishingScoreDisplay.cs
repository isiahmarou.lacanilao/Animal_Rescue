using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FishingScoreDisplay : MonoBehaviour
{
    [SerializeField] private TMP_Text scoreText;
    // Update is called once per frame
    void Update()
    {
        DisplayScore();
    }

    private void DisplayScore()
    {
        scoreText.text = FishingGameManager.instance.GameScore.ToString();
    }
}
