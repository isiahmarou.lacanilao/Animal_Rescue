using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScore : WaterObject
{
    [SerializeField] private int scoreWorth;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void CollisionAction(GameObject gameObject)
    {
        if (gameObject == FishingGameManager.instance.PlayerNet) FishingGameManager.instance.AddScore(scoreWorth);
    }
}
