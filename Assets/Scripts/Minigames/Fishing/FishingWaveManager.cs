using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FishingWaveManager : MonoBehaviour
{
    [SerializeField] private List<Transform> spawnPoints;
    [SerializeField] private float waveSpawnDelayTime;
    [SerializeField] private List<WaveData> waveDataList;
    [SerializeField] private int maxWaveCount;
    [SerializeField] private WaveData WinWave;
    private int currentWaveCount;

    private void Awake()
    {

    }

    private void Start()
    {
        SpawnWave(waveDataList[Random.Range(0, waveDataList.Count)]);
        StartCoroutine(WavespawnDelay());
    }

    IEnumerator WavespawnDelay()
    {
        
        yield return new WaitForSeconds(waveSpawnDelayTime);

            if (currentWaveCount < maxWaveCount)
            {
                SpawnWave(waveDataList[Random.Range(0, waveDataList.Count)]);
                StartCoroutine(WavespawnDelay());
            }

            //Spawn Win Wave
            else
            {
                SpawnWave(WinWave);
            }
     
    }

    private void SpawnWave(WaveData waveData)
    {
        currentWaveCount++;

        for (int i = 0; i < waveData.waveObjects.Count; i++)
        {
            if (waveData.waveObjects[i] != null) CreateWaveObject(waveData.waveObjects[i], spawnPoints[i]);
        }
    }

    private GameObject CreateWaveObject(GameObject prefab, Transform columnParent)
    {
        GameObject spawnedObject = Instantiate(prefab, columnParent);

        //spawnedObject.transform.SetParent(columnParent, false);
        spawnedObject.transform.position = columnParent.position;

        return spawnedObject;
    }

    public float CalculateTotalGameTime() // Based on Wave count and delay
    {
                                // + 1 for Win Wave inclusion
        float totalGameTime = (maxWaveCount + 1) * waveSpawnDelayTime;
        float bonusTime = waveSpawnDelayTime / 2;       // Bonus Time after final wave spawns and before game will end
        totalGameTime += bonusTime;

        return totalGameTime;
    }
}
