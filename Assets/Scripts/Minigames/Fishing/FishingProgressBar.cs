using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class FishingProgressBar : MonoBehaviour
{
    [SerializeField] private Slider FillBar;
    public UnityEvent OnBarFilled;
    private float totalTime;
    private float currentTime;
    private bool isFinished = false;

    private void Awake()
    {

    }

    private void Start()
    {
        currentTime = 0;

        isFinished = false;
    }

    private void Update()
    {
        if (!PauseController.isPaused)
            UpdateBar();
        
        if (currentTime >= totalTime &&
             !isFinished)
        {
            isFinished = true;
            OnBarFilled.Invoke();
        }
    }

    private void UpdateBar()
    {
        FillBar.value = currentTime;
        // Leaving in update if we ever think about bonus timer power ups
        FillBar.maxValue = totalTime;

        currentTime += Time.deltaTime;
    }

    public void SetTime(float time)
    {
        totalTime = time;
    }
}
