using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SocialPlatforms.Impl;

public class FishingGameManager : MonoBehaviour
{
    public static FishingGameManager instance;

    [SerializeField] private FishingWaveManager waveManager;
    [SerializeField] private FishingManager fishingManager;
    [SerializeField] private FishingProgressBar fishingProgressBar;
    [SerializeField] private GameObject victoryScreen;
    [SerializeField] private GameObject loseScreen;

    [SerializeField] public GameObject PlayerNet { get; private set; }
    [SerializeField] private ResultsUI resultsUI;


    [SerializeField] private float gameTime;
    public bool hasGameEnded = false;
    public Action OnGameEnd;

    public float TotalGameTime { get; private set; }
    public int GameScore { get; private set; }

    private void Awake()
    {
        instance = this;
        fishingProgressBar.OnBarFilled.AddListener(GameOver);

    }

    private void Start()
    {

        GameScore = 0;
        loseScreen.SetActive(false);
        victoryScreen.SetActive(false);

        PlayerNet = fishingManager.GetPlayerObject();
        TotalGameTime = gameTime;
        fishingProgressBar.SetTime(TotalGameTime);
    }

    // GameOver Conditions
    public void GameOver()
    {
        if (victoryScreen.activeInHierarchy)
        {
            Debug.Log("Player aready won");
            return;
        }
        loseScreen.SetActive(true);
        PauseController.PauseGame();
        resultsUI.Show(false, GameScore, 400);

        OnGameEnd?.Invoke();
        hasGameEnded = true;
    }

    public void Victory()
    {
        if (loseScreen.activeInHierarchy)
        {
            Debug.Log("Player aready lost");
            return;
        }
        victoryScreen.SetActive(true);
        PauseController.PauseGame();
        resultsUI.Show(true, GameScore, 400);

        hasGameEnded = true;
        OnGameEnd?.Invoke();
    }

    public void AddScore(int scoreToAdd)
    {
        GameScore += scoreToAdd;
    }
}
