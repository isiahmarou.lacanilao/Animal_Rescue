using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner
{
    public class EndlessRunner_Goal : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.gameObject.CompareTag("Player")) return;
            
            EndlessRunner_Manager.OnMiniGameDone?.Invoke(true);
        }
    }
}
