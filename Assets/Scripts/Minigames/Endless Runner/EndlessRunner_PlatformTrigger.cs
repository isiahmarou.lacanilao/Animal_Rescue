using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner
{
    public class EndlessRunner_PlatformTrigger : MonoBehaviour
    {
        private EndlessRunner_Platform platform;
        private void Awake()
        {
            platform = GetComponentInParent<EndlessRunner_Platform>();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.gameObject.CompareTag("Player")) return;
            
            platform.OnTrigger();
        }
    }
}
