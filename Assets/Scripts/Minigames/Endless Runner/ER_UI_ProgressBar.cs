using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EndlessRunner
{
    public class ER_UI_ProgressBar : MonoBehaviour
    {
        public Image progress_Bar;
        private EndlessRunner_Manager manager;

        private float max;
        private void Start()
        {
            manager = EndlessRunner_Manager.Instance;
            max = manager.totalDist - manager.start_X;
        }

        private void LateUpdate()
        {
            progress_Bar.fillAmount = (manager.player_PosX - manager.start_X) / manager.totalDist;
        }
    }
}
