using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner
{
    public class EndlessRunner_Obstacle : MonoBehaviour
    {

        [SerializeField] private float damage = 10f;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.gameObject.CompareTag("Player")) return;
            if (!col.gameObject.TryGetComponent(out HealthComponent healthComponent)) return;

            healthComponent.DamageHealth(damage, transform.gameObject);
        }
    }
}
