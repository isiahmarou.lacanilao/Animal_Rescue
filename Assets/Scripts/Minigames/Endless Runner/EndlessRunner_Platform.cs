using System;
using System.Collections.Generic;
using HelperClass;
using UnityEngine;

namespace EndlessRunner
{
    public class EndlessRunner_Platform : MonoBehaviour
    {
        [field:SerializeField] public Transform endpoint { get; private set; }
        [SerializeField] private bool hasObstacles = true;
        [SerializeField] private List<SpriteRenderer> obstaclesContainer = new List<SpriteRenderer>();
        [Range(0, 4)] [SerializeField] private int minObstacles = 2;

        private int maxObstacles => obstaclesContainer.Count;
        private float obstaclePos_Y;

        private List<GameObject> obstaclesSpawned = new List<GameObject>();
        
        private void OnDisable()
        {
            obstaclesSpawned.DestroyGameObjects();
        }

        public void OnTrigger()
        {
            EndlessRunner_Manager.OnPlatformTrigger?.Invoke(this);
        }
        
        public void SpawnObstacles()
        {
            if (!hasObstacles) return;
            int obsCount = UnityEngine.Random.Range(minObstacles, maxObstacles);

            for (int i = 0; i < obsCount; i++)
            {
                int rand = UnityEngine.Random.Range(0, obstaclesContainer.Count);
                GameObject obstacle = GetObstacle(obstaclesContainer[rand]);
                obstaclesSpawned.Add(obstacle);
                obstaclesContainer.Remove(obstaclesContainer[rand]);
            }
        }
        
        private GameObject GetObstacle(SpriteRenderer rend)
        {
            Bounds bound = rend.bounds;
            GameObject obstacle = Instantiate(
                EndlessRunner_Manager.Instance.GetRandomObstacle(), 
                rend.transform, 
                true);
            
            float offsetX = UnityEngine.Random.Range(-bound.extents.x, bound.extents.x);

            offsetX += bound.center.x;
            obstacle.transform.position = new Vector3(offsetX,bound.center.y);
            return obstacle;
        }
    }
}
