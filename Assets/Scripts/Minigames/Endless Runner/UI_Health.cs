using UnityEngine;
using UnityEngine.UI;

public class UI_Health : MonoBehaviour
{
    [SerializeField] private HealthComponent healthComponent;
    [SerializeField] private Image hp_bar;

    private void Reset()
    {
        healthComponent = GameObject.
            FindGameObjectWithTag("Player").
            GetComponent<HealthComponent>();

        hp_bar = GetComponent<Image>();
    }

    private void OnEnable()
    {
        hp_bar.fillAmount = 1;
        healthComponent.OnTakeDamage += SetBarFill;
    }
    
    private void OnDisable()
    {
        healthComponent.OnTakeDamage += SetBarFill;
    }
    
    private void SetBarFill(float damage, GameObject attacker)
    {
        float percent = healthComponent.HP_Percentage;
        
        hp_bar.fillAmount = percent;

        hp_bar.color = percent switch
        {
            < 0.2f => Color.red,
            < 0.5f => Color.yellow,
            <= 1f => Color.green,
            _ => hp_bar.color
        };
    }
}
