using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using Player;

namespace EndlessRunner
{
    public class EndlessRunner_Controller : MonoBehaviour
    {
        [Header("Movement Properties")] 
        [SerializeField] private float movementSpeed = 8;
        
        [Header("Jump Properties")]
        [SerializeField] private float jumpForce = 16;
        [SerializeField] private float fallMult = 4;
        [SerializeField] private float jumpVelFallOff = 8;

        [Header("Components")] 
        [SerializeField] private PlayerGrounded groundedComponent;
        private Rigidbody2D rb;
        private Animator animator;

        private int _isRunningHash;
        private int _ySpeedHash;
        private int _groundedHash;

        private bool isGrounded => groundedComponent.isGrounded;
        private void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
            
            _isRunningHash = Animator.StringToHash("isRunning");
            _ySpeedHash = Animator.StringToHash("ySpeed");
            _groundedHash = Animator.StringToHash("Grounded");
        }

        private void OnEnable()
        {
            EndlessRunner_Manager.OnMiniGameDone += OnGameEnd;
            rb.gravityScale = 1;
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            animator.enabled = true;
        }

        
        private void OnDisable()
        {
            EndlessRunner_Manager.OnMiniGameDone -= OnGameEnd;
            rb.gravityScale = 0;
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezePosition;
            animator.enabled = false;
        }
        private void FixedUpdate()
        {
            rb.velocity = new Vector2(movementSpeed, rb.velocity.y);
            JumpUpdate();
            AnimUpdate();
        }
        
        public void Jump(InputAction.CallbackContext context)
        {
            // Early returns
            if(!context.started) return;
            if (!isGrounded)return;
            
            rb.velocity = rb.velocity.Replace_Y(jumpForce);
        }
        
        private void JumpUpdate()
        {
            if(isGrounded) return;

            if (rb.velocity.y < jumpVelFallOff || rb.velocity.y > 0)
            {
                rb.velocity += Vector2.up * (fallMult * Physics2D.gravity.y * Time.deltaTime);
            }
        }

        private void AnimUpdate()
        {
            animator.SetBool(_isRunningHash, !Mathf.Approximately(rb.velocity.x, 0));
            animator.SetFloat(_ySpeedHash, rb.velocity.y);
            animator.SetBool(_groundedHash, isGrounded);
        }

        private void OnGameEnd(bool isWin) => this.enabled = false;
    }
}
