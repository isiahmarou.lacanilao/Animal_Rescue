using HelperClass;
using UnityEngine;

public class EndlessRunner_CameraController : MonoBehaviour
{
    [SerializeField] private RectTransform playerOffset;
    [SerializeField] private Transform player;
    [SerializeField] private UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera _pixelPerfectCamera;
    private float yPos;

    private float xOffset;

    
    private void Awake()
    {
        if (player == null) player = FindObjectOfType<Rigidbody2D>().transform;
        if(_pixelPerfectCamera == null) _pixelPerfectCamera = GetComponent<UnityEngine.Experimental.Rendering.Universal.PixelPerfectCamera>();
        // int ppu = ((Screen.width * Screen.height) / 48) / 360;
        // Debug.Log((Screen.width * Screen.height));
        // Debug.Log($"W:{Screen.width} H:{Screen.height}");
        // _pixelPerfectCamera.assetsPPU = ppu;
    }

    private void Start()
    {
        SetCameraPosition();
    }

    private void Update()
    {
        transform.position = new Vector3(player.position.x + xOffset, yPos, -10f);
    }

    [ContextMenu("Set Camera Position")]
    void SetCameraPosition()
    {
        Vector2 offset = gameObject.scene.
            GetFirstMainCameraInScene().
            GetWorldPositionOfCanvasElement(playerOffset);
        
        Vector2 position = transform.position;
        transform.position =  new Vector3(position.x - offset.x, position.y - offset.y, - 10f);
        transform.position += player.position;
        yPos = transform.position.y;

        xOffset = transform.position.x - player.position.x;
    }
}
