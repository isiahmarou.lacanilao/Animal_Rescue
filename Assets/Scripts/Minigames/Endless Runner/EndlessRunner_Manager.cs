using System;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunner
{
    public class EndlessRunner_Manager : Singleton<EndlessRunner_Manager>
    {
        [Header("Game")] 
        [SerializeField] private EndlessRunner_Platform platform_Prefab;
        [SerializeField] private EndlessRunner_Platform goal_Prefab;
        [SerializeField] private Transform platHolder;
        [SerializeField] private HelperClass.WeightedList<GameObject> obstacle_Prefabs;

        [Header("UI")]
        [SerializeField] private ResultsUI resultsUI;
        [SerializeField] private GameObject LoseUI;
        [SerializeField] private GameObject OnScreenControl;

        public static Action<bool> OnMiniGameDone;
        public static Action<EndlessRunner_Platform> OnPlatformTrigger;

        [SerializeField] private List<EndlessRunner_Platform> platformsList;
        [SerializeField] private int numPlatformToSpawn = 9;

        private int count;
        private Transform player;
        private HealthComponent player_HP;
        
        
        public float start_X { get; private set; }
        public float totalDist { get; private set; }
        public float player_PosX => player.transform.position.x;
        private bool isDone; 
        
        private void Awake()
        {
            foreach (EndlessRunner_Platform plat in platformsList)
            {
                plat.SpawnObstacles();
            }
            
            player = FindObjectOfType<EndlessRunner_Controller>().transform;
            player_HP = player.GetComponent<HealthComponent>();
            start_X = platformsList[0].transform.position.x;
            totalDist = platformsList[0].endpoint.position.x - start_X;
            totalDist *= (numPlatformToSpawn);
            totalDist += (goal_Prefab.endpoint.position.x);
        }

        private void OnEnable()
        {
            OnMiniGameDone += OnDone;
            OnPlatformTrigger += SpawnPlatform;
            OnScreenControl.SetActive(true);
            count = platformsList.Count;
            resultsUI.Hide();
        }
        
        private void OnDisable()
        {
            OnMiniGameDone -= OnDone;
            OnPlatformTrigger -= SpawnPlatform;
        }


        private void OnDone(bool isWin)
        {
            int score = Mathf.RoundToInt(player_HP.current_HP * 10);
            int maxScore = Mathf.RoundToInt(player_HP.max_HP * 10);
            resultsUI.Show(isWin,score ,maxScore);
            OnScreenControl.SetActive(false);
        }
        
        private void SpawnPlatform(EndlessRunner_Platform _platform)
        {
            if(isDone) return;

            EndlessRunner_Platform prefabToSpawn = platform_Prefab;
            
            if (count >= numPlatformToSpawn)
            {
                prefabToSpawn = goal_Prefab;
                isDone = true;
            }

            EndlessRunner_Platform newPlat = Instantiate(prefabToSpawn, platHolder, true);
            newPlat.SpawnObstacles();
            Vector3 newPos = platformsList[^1].endpoint.position;
            newPlat.transform.position = newPos;
            platformsList.Add(newPlat);
            count++;


            EndlessRunner_Platform firstPlat = platformsList[0];
            platformsList.Remove(firstPlat);
            Destroy(firstPlat.gameObject, 2f);
        }

        public GameObject GetRandomObstacle()
        {
            return obstacle_Prefabs.GetWeightedRandom();
        }
    }
}
