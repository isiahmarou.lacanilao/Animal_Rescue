using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace EndlessRunner
{
    public class EndlessRunner_Player : MonoBehaviour
    {
        [SerializeField] private HealthComponent healthComponent;
        [SerializeField] private SpriteRenderer _spriteRenderer;

        private Tween blinkingTween;
        private void OnEnable()
        {
            healthComponent.OnTakeDamage += OnDamaged;
            healthComponent.OnDeath += OnDeath;
        }

        private void OnDisable()
        {
            healthComponent.OnTakeDamage -= OnDamaged;
            healthComponent.OnDeath -= OnDeath;
            blinkingTween.Kill();
            blinkingTween = null;
        }

        private void OnDamaged(float dmg, GameObject Attacker)
        {
            healthComponent.SetInvincibility(true);
            blinkingTween = _spriteRenderer.DOFade(0, 0.1f).SetLoops(15, LoopType.Yoyo);
            
            blinkingTween.onKill += () =>
            {
                _spriteRenderer.color = Color.white;
                healthComponent.SetInvincibility(false);
            };
        }

        private void OnDeath() => EndlessRunner_Manager.OnMiniGameDone.Invoke(false);
    }
}
