using System;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [field: SerializeField] public bool isInvincible { get; private set; }
    [field: SerializeField] public float max_HP { get; private set; }
    public float current_HP { get; private set; }
    public float HP_Percentage => current_HP / max_HP;

    public Action OnDeath;
    public Action<float, GameObject> OnTakeDamage;
    public Action<float, GameObject> OnHeal;

    private void Awake()
    {
        current_HP = max_HP;
    }

    public void AddHealth(float value, GameObject healer)
    {
        current_HP = Mathf.Clamp(current_HP + value, 0, max_HP);
        OnHeal?.Invoke(value, healer);
    }

    public void DamageHealth(float dmg, GameObject attacker)
    {
        if (isInvincible) return;
        current_HP = Mathf.Clamp(current_HP - dmg, 0, max_HP);

        OnTakeDamage?.Invoke(dmg, attacker);

        if (current_HP <= 0) OnDeath?.Invoke();
    }

    public void SetInvincibility(bool willBeInvincible) => isInvincible = willBeInvincible;
}