using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoodleJump
{
    public class DoodleJump_Camera : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D player;
        [SerializeField] private float yOffset = -2;

        private Vector3 playerPos => player.position;

        
        private float worldHeight;
        private float worldWidth;
        
        private float min_X => transform.position.x - (worldWidth/2f);
        private float max_X => transform.position.x + (worldWidth / 2f);
        private float min_Y => transform.position.y - (worldHeight / 2f);

        private void Awake()
        {
            float playerY = playerPos.y + yOffset;
            float newY = playerY > transform.position.y ? playerY : transform.position.y;
            transform.position = new Vector3(0, newY, transform.position.z);
            
            float aspect = (float)Screen.width / Screen.height;
            
            worldHeight = GetComponentInChildren<Camera>().orthographicSize * 2;
            
            worldWidth = worldHeight * aspect;
        }

        private void LateUpdate()
        {
            float playerY = playerPos.y + yOffset;
            float newY = playerY > transform.position.y ? playerY : transform.position.y;
            transform.position = new Vector3(0, newY, transform.position.z);

            PlayerBounds();
        }

        void PlayerBounds()
        {
            if (playerPos.x > max_X)
            {
                player.position = new Vector2(min_X + 0.05f, playerPos.y);
            }
            else if (playerPos.x < min_X)
            {
                player.position = new Vector2(max_X - 0.05f, playerPos.y);
            }

            if (playerPos.y < min_Y)
            {
                DoodleJumpManager.OnMinigameDone?.Invoke(true);
            }
        }
    }
}
