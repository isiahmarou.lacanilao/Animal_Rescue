using System.Collections.Generic;
using UnityEngine;

namespace DoodleJump
{

    public class DJ_PlatformGenerator : MonoBehaviour
    {
        [System.Serializable]
        internal struct MinMax
        {
            public float min;
            public float max;
        }

        
        [SerializeField] private DJ_Plat platform_Prefab;
        [SerializeField] private HelperClass.WeightedList<MinMax> y_distanceChance;
        [SerializeField] private HelperClass.WeightedList<MinMax> x_distanceChance;
        [SerializeField] private HelperClass.WeightedList<int> platSize;
        [SerializeField] private Transform Trigger;

        private SpriteRenderer _spriteRenderer;
        public Vector2 minPos { get; private set; }
        public Vector2 maxPos { get; private set; }

        private List<DJ_Plat> platformList = new List<DJ_Plat>();
        // [ContextMenu("Initialize")]
        public void SetSize(Camera myCamera)
        {
            float aspect = (float)Screen.width / Screen.height;
            
            float worldHeight = myCamera.orthographicSize * 2;
            
            float worldWidth = worldHeight * aspect;

            transform.localScale = new Vector3(worldWidth, worldHeight, 1);
            
        }
        
        public void _Initialize(Vector2 prevPos)
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            
            Vector2 extents = _spriteRenderer.bounds.extents;

            transform.position = new Vector3(
                0, 
                prevPos.y + extents.y, 
                transform.position.z);
            

            Vector2 pos = transform.position;
            

            minPos = new Vector2(
                pos.x - extents.x,
                pos.y - extents.y);

            maxPos = new Vector2(
                pos.x + extents.x,
                pos.y + extents.y);

            Trigger.localScale = new Vector3(1.5f, 0.1f, 1);
            Trigger.position = maxPos.Replace_X(transform.position.x);
            Trigger.position = Trigger.position.Add_Y(-Trigger.localScale.y);

            InitializePlatforms();
        }

        void InitializePlatforms()
        {
            float PosY = minPos.y;

            do
            {
                MinMax minMax_X = x_distanceChance.GetWeightedRandom();
                MinMax minMax_Y = y_distanceChance.GetWeightedRandom();

                float posX = UnityEngine.Random.Range(minMax_X.min, minMax_X.max);

                float offsetX = UnityEngine.Random.Range(0, 1f) > 0.5f ? 1 : -1f;

                posX = (posX * offsetX) + transform.position.x;
                
                PosY += UnityEngine.Random.Range(minMax_Y.min, minMax_Y.max);

                posX = UnityEngine.Random.Range(minPos.x, maxPos.x);
                Vector2 pos = new Vector2(posX, PosY);
                
                SpawnPlatform(pos);
            } while (PosY < maxPos.y);
        }

        void SpawnPlatform(Vector2 pos)
        {
            DJ_Plat plat = Instantiate(platform_Prefab);

            plat.transform.position = pos;

            float scale_X = platSize.GetWeightedRandom();
            
            plat.transform.localScale = new Vector3(scale_X, 
                plat.transform.localScale.y, 
                plat.transform.localScale.z);
            
            plat.transform.SetParent(transform);
            platformList.Add(plat);
        }

        public void OnTrigger()
        {
            DoodleJumpManager.Instance.PlatformTrigger(this);
        }
    }

}