using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DoodleJump
{
    public class UI_Bar : MonoBehaviour
    {
        [SerializeField] private Image bar;
        private DoodleJumpManager _manager;

        private void Start()
        {
            _manager = DoodleJumpManager.Instance;
        }

        private void FixedUpdate()
        {
            float percent = Mathf.Clamp(_manager.percentToGoal, 0, 1f);
            bar.fillAmount = percent;
        }
    }
}
