using System;
using Baracuda.Monitoring;
using UnityEngine;
using DoodleJump;
using UnityEngine.InputSystem;

namespace Player
{
    public class DoodleJump_Controller : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private PlayerGrounded _grounded;
        [SerializeField] private Rigidbody2D rb;

        private SpriteRenderer _spriteRenderer;
        private Animator animator;
        
        [Header("Physics Properties")] 
        [SerializeField] private float jumpForce = 16;
        [SerializeField] private float fallMult = 4;
        [SerializeField] private float jumpVelFallOff = 8;
        [SerializeField] private float movementSpeed = 8;
        
        private int _speedAnimHash;
        private float newX;
        private float controllerX;

        // pointers
        /*[Monitor]*/ private Vector3 acceleration => Input.acceleration;
        private bool isGrounded => _grounded.isGrounded;
        private Vector2 playerVel
        {
            get => rb.velocity;
            set => rb.velocity = value;
        }

        private void Awake()
        {
            animator = GetComponent<Animator>();
            _speedAnimHash = Animator.StringToHash("ySpeed");
            _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }

        private void OnEnable()
        {
            DoodleJumpManager.OnMinigameDone += OnGameDone;
            rb.gravityScale = 1;
        }

        private void OnDisable()
        {
            DoodleJumpManager.OnMinigameDone -= OnGameDone;
        }

        private void Update()
        {
            newX = acceleration.x * movementSpeed;
            newX += controllerX;
            JumpUpdate();
        }

        private void FixedUpdate()
        {
            playerVel = new Vector2(newX, playerVel.y);
            animator.SetFloat(_speedAnimHash, playerVel.y);
            
            if(Mathf.Approximately(playerVel.x , 0)) return; // dont flip or unflip if velocity is at 0
            
            _spriteRenderer.flipX = playerVel.x < 0;
        }
        
        public void Move(InputAction.CallbackContext context)
        {
            controllerX = context.ReadValue<Vector2>().x * (movementSpeed / 5);
        }

        private void JumpUpdate()
        {
            if(!isGrounded)
            {
                if (playerVel.y < jumpVelFallOff || playerVel.y > 0)
                {
                    playerVel += Vector2.up * (fallMult * Physics2D.gravity.y * Time.deltaTime);
                }
                return;
            }
            if(playerVel.y > 0) return;
            
            playerVel = new Vector2(playerVel.x, jumpForce);
        }

        void OnGameDone(bool isWin)
        {
            DoodleJumpManager.OnMinigameDone -= OnGameDone;
            rb.gravityScale = 0;
            rb.velocity = Vector2.zero;
            rb.constraints = RigidbodyConstraints2D.FreezePosition;
            this.enabled = false;
        }

        void BoundsUpdate()
        {
            
        }
    }
}
