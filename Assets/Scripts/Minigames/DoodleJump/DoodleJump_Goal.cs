using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoodleJump
{
    public class DoodleJump_Goal : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.gameObject.CompareTag("Player")) return;

            DoodleJumpManager.OnMinigameDone?.Invoke(true);
        }
    }
}
