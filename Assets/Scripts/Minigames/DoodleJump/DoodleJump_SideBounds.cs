using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoodleJump_SideBounds : MonoBehaviour
{
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private Transform teleportTransform;

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (!col.gameObject.CompareTag("Player")) return;
        if(!col.gameObject.TryGetComponent(out Rigidbody2D rb)) return;

        rb.position = new Vector2(teleportTransform.position.x, rb.position.y);
    }
}
