using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using HelperClass;

namespace DoodleJump
{
    public class DoodleJumpManager : Singleton<DoodleJumpManager>
    {
        [Header("Camera")]
        [SerializeField] private Camera _camera;

        [Header("Level Generator")] 
        [SerializeField] private int batchNumToSpawn = 10;
        
        [Header("Prefabs")]
        [SerializeField] private DJ_PlatformGenerator PlatformGenerator_Prefab;
        [SerializeField] private GameObject Goal_Prefab;

        [Header("UI")]
        [SerializeField] private GameObject End_UI;
        [SerializeField] private TMPro.TMP_Text end_TXT;

        public static Action<bool> OnMinigameDone;
        public Transform player { get; private set; }
        public float distToFloor { get; private set; }
        public float floorToGoalDist { get; private set; }
        public float percentToGoal { get; private set; }

        private float Start_Y;

        private List<DJ_PlatformGenerator> _platformGeneratorsSpawned = new List<DJ_PlatformGenerator>();

        private void Awake()
        {
            player = FindObjectOfType<Rigidbody2D>().transform;
            Start_Y = player.position.y;
        }

        private void Start()
        {
            Vector2 batchPos = transform.position;

            if(_camera==null) _camera = gameObject.scene.GetFirstMainCameraInScene();
            
            PlatformGenerator_Prefab.SetSize(_camera);
            
            for (int i = 0; i < 2; i++)
            {
                DJ_PlatformGenerator pla = Instantiate(PlatformGenerator_Prefab, transform, true);
                pla._Initialize(batchPos);
                batchPos = new Vector2(0, pla.maxPos.y);
                _platformGeneratorsSpawned.Add(pla);
            }

            // Transform goal = Instantiate(Goal_Prefab, transform, true).transform;
            //
            // goal.localScale = new Vector3(PlatformGenerator_Prefab.transform.lossyScale.x,
            //     goal.localScale.y, goal.localScale.z);
            //
            // float newY = batchPos.y + (goal.lossyScale.y);
            // goal.position = new Vector3(goal.position.x, newY, goal.position.z);
            // floorToGoalDist = goal.position.y - Start_Y;
        }
        
        private void OnEnable()
        {
            OnMinigameDone += OnGameDone;
        }
        
        private void OnDisable()
        {
            OnMinigameDone -= OnGameDone;
        }

        private void FixedUpdate()
        {
            float temp_DistToFloor = player.position.y - Start_Y;
            distToFloor = distToFloor > temp_DistToFloor ? distToFloor : temp_DistToFloor;
            
            percentToGoal = (distToFloor / floorToGoalDist);
        }

        private void OnGameDone(bool isWin)
        {
            end_TXT.text = isWin ? "You Win!" : "You Lose!";
            End_UI.SetActive(true);
        }

        public void PlatformTrigger(DJ_PlatformGenerator platformGenerator)
        {
            DJ_PlatformGenerator pla = Instantiate(PlatformGenerator_Prefab, transform, true);
            pla._Initialize(_platformGeneratorsSpawned[^1].maxPos.Replace_X(0));
            _platformGeneratorsSpawned.Add(pla);
            _platformGeneratorsSpawned.Remove(platformGenerator);
            Destroy(platformGenerator.gameObject, 2f);
        }
    }

}