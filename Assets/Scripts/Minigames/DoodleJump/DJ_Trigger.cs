using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DoodleJump
{
    public class DJ_Trigger : MonoBehaviour
    {
        private DJ_PlatformGenerator _platformGenerator;
        
        
        private void Awake()
        {
            _platformGenerator = GetComponentInParent<DJ_PlatformGenerator>();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!col.gameObject.CompareTag("Player")) return;
            
            _platformGenerator.OnTrigger();
            gameObject.SetActive(false);
        }
    }

}