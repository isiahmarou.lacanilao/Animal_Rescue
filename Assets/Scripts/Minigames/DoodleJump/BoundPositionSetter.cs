using System;
using System.Collections;
using UnityEngine;
using HelperClass;

namespace DoodleJump
{
    public class BoundPositionSetter : MonoBehaviour
    {
        [SerializeField] private RectTransform canvasBound_L;
        [SerializeField] private RectTransform canvasBound_R;
        [SerializeField] private RectTransform canvasBound_B;
        [SerializeField] private RectTransform canvasBound_T;
        
        [SerializeField] private RectTransform canvasTeleport_L;
        [SerializeField] private RectTransform canvasTeleport_R;

        [SerializeField] private Transform Bound_L;
        [SerializeField] private Transform Bound_R;
        [SerializeField] private Transform Bound_B;
        [SerializeField] private Transform Bound_T;

        [SerializeField] private Transform Teleport_L;
        [SerializeField] private Transform Teleport_R;

        private Rigidbody2D playerRB;
        private Camera cam;

        // [Monitor] private string camName => cam.gameObject.scene.name;
        // [Monitor] private float boundL_X => Bound_L.position.x;
        // [Monitor] private float rectL_X => canvasBound_L.position.x;
        // [Monitor] private Vector2 screenSize => new Vector2(Screen.width, Screen.height);

        private bool hasSetup;
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(0.1f);
            cam = gameObject.scene.GetFirstMainCameraInScene();
            
            Bound_L.position = cam.GetWorldPositionOfCanvasElement(canvasBound_L);
            Bound_R.position = cam.GetWorldPositionOfCanvasElement(canvasBound_R);
            Bound_B.position = cam.GetWorldPositionOfCanvasElement(canvasBound_B);
            Bound_T.position = cam.GetWorldPositionOfCanvasElement(canvasBound_T);
            
            Teleport_L.position = cam.GetWorldPositionOfCanvasElement(canvasTeleport_L);
            Teleport_R.position = cam.GetWorldPositionOfCanvasElement(canvasTeleport_R);
            
            playerRB = DoodleJumpManager.Instance.player.GetComponent<Rigidbody2D>();

            hasSetup = true;
        }

        private void FixedUpdate()
        {
            if(!hasSetup) return;
            float newX = Mathf.Clamp(playerRB.position.x, Bound_L.position.x - 1f, Bound_R.position.x + 1f);
            float newY = Mathf.Clamp(playerRB.position.y, Bound_B.position.y - 1f, Bound_T.position.y + 1f);

            playerRB.position = new Vector2(newX, newY);
        }
    }
}
