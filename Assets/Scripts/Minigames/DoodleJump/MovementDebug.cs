using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DoodleJump
{
    public class MovementDebug : MonoBehaviour
    {
        [SerializeField] private float movementSpeed = 5;
        [SerializeField] private Rigidbody2D rb;

        private float newX = 0;

        private void FixedUpdate()
        {
            rb.velocity = new Vector2(newX, rb.velocity.y);
        }

        public void Move(InputAction.CallbackContext context)
        {
            newX = context.ReadValue<Vector2>().x * movementSpeed;
        }
    }
}
