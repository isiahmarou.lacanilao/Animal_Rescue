using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using HelperClass;

public class SwipeInputController : MonoBehaviour
{
    [SerializeField] private float swipeTimeOffset = 0.1f;
    [SerializeField] private float swipeDistanceOffset = 0.1f;

    [SerializeField] private Transform form;
    
    private Camera cam;
    
    public bool isTouching { get; set; }
    public bool isSwiping { get; set; }
    
    public float startTouchTime { get; set; }
    public float endTouchTime { get; set; }

    public Vector2 startPosition { get; set; }
    public Vector2 endPosition { get; set; }
    public Vector2 touchPosition { get; set; }
    
    
    private void Awake()
    {
        if(cam == null) cam = gameObject.scene.GetFirstMainCameraInScene();
    }

    private void LateUpdate()
    {
        if(!isTouching && !isSwiping) return;

        Vector2 worldPos = cam.ScreenToWorldPoint(touchPosition);
        
        Ray raycast = cam.ScreenPointToRay(worldPos);

        var hitInfo = Physics2D.Raycast(worldPos, touchPosition);
        if (hitInfo.transform)
        {
            print(hitInfo.transform);
        }
    }

    public void Swipe(InputAction.CallbackContext context)
    {
        if(!isTouching) return;
        if (startTouchTime + swipeTimeOffset > Time.time)
        {
            isSwiping = false;
            return;
        }
        
        touchPosition = context.ReadValue<Vector2>();

        float dist = Vector2.Distance(touchPosition, startPosition);
        isSwiping = dist >= swipeDistanceOffset;
    }

    public void Touch(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            startPosition = touchPosition;
            isTouching = true;
            startTouchTime = Time.time;
        }
        else if (context.canceled)
        {
            isTouching = false;
            endPosition = touchPosition;
            isSwiping = false;
            endTouchTime = Time.time;
        }
    }
}
