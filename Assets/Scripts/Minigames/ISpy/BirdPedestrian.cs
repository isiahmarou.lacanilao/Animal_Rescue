using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdPedestrian : Pedestrian
{
    protected override IEnumerator Co_Idling()
    {
        Debug.Log("HUH");
        currentDestinationIndex++;
        if (currentDestinationIndex < destinations.Count)
        {
            Debug.Log("HUH 22");

            anim.SetFloat("UpAxis", destinations[currentDestinationIndex].y);
            anim.SetFloat("RightAxis", destinations[currentDestinationIndex].x);
            if (idlingOptions.Count > 0)
            {
                currentIdlingOptions = new List<IdlingData>(idlingOptions);
                yield return StartCoroutine(Co_DoingSomething());
            }

        }
        else
        {
            //yield return new WaitForSeconds(1f);
            BirdPedestrianPool.pool.Release(this);
        }


    }
}
