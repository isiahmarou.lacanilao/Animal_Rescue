using HelperClass;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Animations;
using UnityEditor.Rendering;
using UnityEngine;

public enum IdlingType
{ 
Talking,
Yelling,
Calling,
Looking,
Waiting,
None,
}

public class Pedestrian : MonoBehaviour
{
    protected List<IdlingData> idlingOptions = new List<IdlingData>();
    protected List<IdlingData> currentIdlingOptions = new List<IdlingData>();
    //public List<IdlingType> idlingOptions = new List<IdlingType>();
    protected int currentDestinationIndex;
    protected List<Vector2> destinations = new List<Vector2>();
    [HideInInspector] public Rigidbody2D rb;
    protected Animator anim;

    protected Coroutine handle;

    protected IdlingData id;
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

    }
    public void Initialize(float p_speed,List<Vector2> p_destinations, List<IdlingData> p_idlingOptions, Vector3 p_startPosition)
    {
        currentDestinationIndex = 0;
        


        destinations = p_destinations;

        idlingOptions = p_idlingOptions;
        transform.position = p_startPosition;
        for (int i = 0; i < idlingOptions.Count; i++)
        {
            idlingOptions[i].waitTimes.maxChance = MathHelpers.AddAll(idlingOptions[i].waitTimes);
        }
        anim.SetFloat("Speed", p_speed);
        anim.SetFloat("UpAxis", destinations[currentDestinationIndex].y);
        anim.SetFloat("RightAxis", destinations[currentDestinationIndex].x);
        anim.SetTrigger("Moving");
    }
    public void Idling()
    {
        if (handle != null) StopCoroutine(handle);
 
        handle = StartCoroutine(Co_Idling());
    }
    protected virtual IEnumerator Co_Idling()
    {
        Debug.Log("HUH");
        currentDestinationIndex++;
        if (currentDestinationIndex < destinations.Count)
        {
            Debug.Log("HUH 22" );
           
            anim.SetFloat("UpAxis", destinations[currentDestinationIndex].y);
            anim.SetFloat("RightAxis", destinations[currentDestinationIndex].x);
            if (idlingOptions.Count > 0)
            {
                currentIdlingOptions = new List<IdlingData>(idlingOptions);
                yield return StartCoroutine(Co_DoingSomething());
            }
   
        }
        else
        {
            //yield return new WaitForSeconds(1f);
            PedestrianPool.pool.Release(this);
        }

       
    }

    protected IEnumerator Co_DoingSomething()
    {

        if (id == null && currentIdlingOptions.Count > 0|| 
            id != null && id.idlingType != IdlingType.None && currentIdlingOptions.Count > 0)
        {
            Debug.Log(gameObject.name + " - " + currentIdlingOptions.Count);
            int rnd = Random.Range(0, currentIdlingOptions.Count);
            IdlingData id = currentIdlingOptions[rnd];
            
            anim.SetTrigger(id.idlingType.ToString());

            float wf = MathHelpers.ChooseWeightedFloat(currentIdlingOptions[rnd].waitTimes);
            yield return new WaitForSeconds(wf);
            currentIdlingOptions.Remove(id);
            id = null;
            StartCoroutine(Co_DoingSomething());
        }
        else
        {
            anim.SetTrigger("Moving");
        }
       
    }
 
}
