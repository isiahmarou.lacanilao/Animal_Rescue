using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class LostAnimal : MonoBehaviour
{
    public Action onClickEvent;
    void OnClick()
    {
        onClickEvent.Invoke();
        Debug.Log("CLICKED");
    }
}
