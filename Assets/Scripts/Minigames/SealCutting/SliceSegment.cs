using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceSegment : MonoBehaviour
{
    public bool sliced;
    public Vector2 direction;
    public float distance;
    public SpriteRenderer sr;
    public Transform srTransform;
    public Sprite uncut;
    public Sprite cut;

    public Transform pointA;
    public Transform pointB;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        srTransform = sr.transform;
    }
    public void Initialize()
    {
        sliced = false;
        gameObject.SetActive(true);
        sr.sprite = uncut;
        direction = (pointA.position- pointB.position).normalized;
        //direction = new Vector2(direction.x / direction.x * Math.Sign(direction.x), direction.y / direction.y * Math.Sign(direction.y));
        distance = Vector2.Distance(pointA.position, pointB.position);

    }

    public void Cut()
    {
        sliced = true;
        sr.sprite = cut;
    }
  
}
