using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pet : MonoBehaviour
{
    public List<DirtSpot> washingDirtSpots = new List<DirtSpot>();
    public List<DirtSpot> dryingDirtSpots = new List<DirtSpot>();
    public List<ParticleSystem> sparkleParticles = new List<ParticleSystem>();
}
