using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
public class DirtSpot : MonoBehaviour
{
    [HideInInspector] public int currentHealth;
    public int maxHealth;
    private Vector2 lastPos;
    private Vector2 lastDirection;
    private SpriteRenderer sr;
    [SerializeField] private float startingOpacity;
    bool canClean;
    [SerializeField] private float cooldownRate;
    //private float timer;
    //[SerializeField] private float maxTimer;
    [SerializeField] private float distanceOffset;
    IEnumerator runningCoroutine;
    public Action<int> onCleanEvent;
    private void Awake()
    {
        currentHealth = maxHealth;
        canClean = true;
        sr = GetComponent<SpriteRenderer>();

    }
    public void OnHit(Vector2 p_position)
    {
        //Debug.Log("Phase 0" + timer);

        if (canClean)
        {
            //Debug.Log("Phase 0.1");
            if (currentHealth > 0)
            {
                //Debug.Log("Phase 1");
                if (lastPos != p_position)
                {
                    //Debug.Log("Phase 2");
                    Vector2 currentDir = ((Vector2)transform.position - p_position).normalized;
                    currentDir = new Vector2(currentDir.x / currentDir.x * Math.Sign(currentDir.x), currentDir.y / currentDir.y * Math.Sign(currentDir.y));
                    if (lastDirection != currentDir * -1 || lastDirection == Vector2.zero) //If Multiplied by -1 makes it same direction, and its not the same, its opposite direction
                    {
                      //  Debug.Log("Phase 3");
                        if (Vector2.Distance(p_position, lastPos) > distanceOffset || lastPos == Vector2.zero)
                        {
                        //    Debug.Log("Phase 4");
                            lastDirection = currentDir;
                            lastPos = p_position;
                            Clean();
                        }

                    }
                }
            }


        }
      


    }

    void Clean()
    {
        canClean = false;
        currentHealth--;
        onCleanEvent.Invoke(1);
       // Debug.Log("currentHealth " + currentHealth + " " + (float)currentHealth / (float)maxHealth);

        if (currentHealth > 0)
        {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, startingOpacity + ((float)currentHealth / (float)maxHealth) * (1 - startingOpacity));
            runningCoroutine = Co_CoolDown();
            StartCoroutine(runningCoroutine);
        }
        else
        {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b,0f);
            //var pulseOutSequence = DOTween.Sequence();

            //pulseOutSequence.Append(sr.DOFade(0, 0.5f));
            //pulseOutSequence.Play();
        }

    }


    IEnumerator Co_CoolDown()
    {
        
        yield return new WaitForSeconds(cooldownRate);
        canClean = true;



    }
}
