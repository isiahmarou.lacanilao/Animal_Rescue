using System;
using PersistentManager;
using UnityEngine;
using EventManager;

namespace Animal
{
    public class LobbyPet : MonoBehaviour
    {
        [SerializeField] private PetType petType;
        private PetStatus petStatus;
        private DateTime timeLastFed;
        private DateTime timeLastCleaned;

        public void Start()
        {
            petStatus = PlayerDataManager.Instance.GetPetStatus(petType);
            bool isActive = petStatus != null;

            gameObject.SetActive(isActive);

            PlayerDataEvents.OnCatchSuccess.AddListener(OnMiniGameDone);
            PlayerDataEvents.OnPetCleaned.AddListener(OnCleaned);
            PlayerDataEvents.OnPetFed.AddListener(OnFed);
        }

        private void OnDestroy()
        {
            PlayerDataEvents.OnCatchSuccess.RemoveListener(OnMiniGameDone);
            PlayerDataEvents.OnPetCleaned.RemoveListener(OnCleaned);
            PlayerDataEvents.OnPetFed.RemoveListener(OnFed);
        }

        private bool IsThisPet(PetType type)
        {
            if (petType != type) return false;

            // double check
            petStatus = PlayerDataManager.Instance.GetPetStatus(type);
            gameObject.SetActive(petStatus.petType != PetType.NULL);

            if (!gameObject.activeSelf) return false;

            return petStatus != null;
        }

        private void OnMiniGameDone(PetType type, int score)
        {
            if (petType != type) return;
            gameObject.SetActive(true);
        }

        private void OnFed(PetType type, int score)
        {
            if (!IsThisPet(type)) return;

            timeLastFed = DateTime.Today;

            // change sprite or something on lobby
        }

        private void OnCleaned(PetType type, int score)
        {
            if (!IsThisPet(type)) return;

            timeLastCleaned = DateTime.Today;

            // change sprite or something on lobby
        }
    }
}
