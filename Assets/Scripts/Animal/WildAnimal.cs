using System;
using EventManager;
using UnityEngine;
using PersistentManager;
using UnityEngine.SceneManagement;

namespace Animal
{
    public class WildAnimal : MonoBehaviour
    {
        [SerializeField] private PetType petType;
        private bool isUnlocked;

        private void Start()
        {
            PlayerDataEvents.OnCatchSuccess.AddListener(OnMiniGameDone);
            SceneManager.activeSceneChanged += OnActiveSceneChange;

            isUnlocked = PlayerDataManager.Instance.IsPetUnlocked(petType);

            if (isUnlocked) Destroy(this.gameObject);
        }

        private void OnDestroy()
        {
            PlayerDataEvents.OnCatchSuccess.AddListener(OnMiniGameDone);
            SceneManager.activeSceneChanged -= OnActiveSceneChange;
        }


        private void OnActiveSceneChange(Scene current, Scene next)
        {
            print(PlayerDataManager.Instance);

            if (!PlayerDataManager.Instance) return;

            isUnlocked = PlayerDataManager.Instance.IsPetUnlocked(petType);

            if (isUnlocked) Destroy(this.gameObject);
        }

        private void OnMiniGameDone(PetType type, int score)
        {
            if (type == petType)
            {
                print("SHOULD DESTROY");
                Destroy(this.gameObject);
            }
        }
    }
}
