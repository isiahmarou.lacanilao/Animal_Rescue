using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using System;
public class FactPopUpUI : MonoBehaviour
{
    RectTransform rt;
    bool isOut = false;
    float origin;
    [SerializeField] float target;
    [SerializeField] TMP_Text titleText;
    [SerializeField] TMP_Text contentText;

    [SerializeField] float time;
    [SerializeField] float delay;
    List<Sequence> runningTweens = new List<Sequence>();
    protected Coroutine handle;
 
    private void Awake()
    {
        rt = GetComponent<RectTransform>();
        origin = rt.anchoredPosition.x;
    }
    public void Initialize(string p_title, string p_contentText)
    {
        if (runningTweens.Count > 0)
        {
            for (int i=0; i < runningTweens.Count; i++)
            {
                runningTweens[i].Kill();
            }
        }
        titleText.text = p_title;
        contentText.text = p_contentText;
        if (handle != null) StopCoroutine(handle);
        if (isOut)
        {
            handle = StartCoroutine(Repeat());
        }
        else
        {
            handle = StartCoroutine(In());
        }

    }

    public void Deinitialize()
    {
        handle = StartCoroutine(Out());

    }
    IEnumerator Repeat()
    {
        yield return handle = StartCoroutine(Out());
        yield return new WaitForSeconds(delay);
        yield return handle = StartCoroutine(In());
    }
    IEnumerator In()
    {
        isOut = true;
        var pulseOutSequence = DOTween.Sequence();
        pulseOutSequence.Join(rt.DOAnchorPosX(target, time));

        runningTweens.Add(pulseOutSequence);
        pulseOutSequence.Play();
        yield return pulseOutSequence.WaitForCompletion();
        runningTweens.Remove(pulseOutSequence);
      
        
    }

    IEnumerator Out()
    {
        isOut = false;
        var pulseOutSequence2 = DOTween.Sequence();
        pulseOutSequence2.Join(rt.DOAnchorPosX(origin, time));

        runningTweens.Add(pulseOutSequence2);
        pulseOutSequence2.Play();
        yield return pulseOutSequence2.WaitForCompletion();
        runningTweens.Remove(pulseOutSequence2);
    }
}
