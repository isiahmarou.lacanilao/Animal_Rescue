using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;

public class MoveState : StateMachineBehaviour
{
    Pedestrian pedestrian;
    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (pedestrian == null)
        {
            if (animator.gameObject.TryGetComponent(out Pedestrian ped))
            {
                pedestrian = ped;
            }
        }
        
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (pedestrian != null)
        {
            Vector2 dir = (new Vector2(animator.GetFloat("RightAxis"), animator.GetFloat("UpAxis")) - (Vector2)animator.transform.position).normalized;
            pedestrian.rb.MovePosition((Vector2)pedestrian.rb.position + (dir * animator.GetFloat("Speed") * Time.deltaTime));
            animator.SetFloat("Distance", Vector2.Distance(pedestrian.rb.position, new Vector2(animator.GetFloat("RightAxis"), animator.GetFloat("UpAxis"))));
        }
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
