using System;
using System.Collections;
using EventManager;
using Unity.VisualScripting;
using UnityEngine;

public class CameraSwitchEventCaller : MonoBehaviour
{
    [SerializeField] private Cinemachine.CinemachineBrain camBrain;

    private float prevTimeScale;
    private bool prevIsBlending;
    private int prevCamId;
    
    private bool isCoroutineRunning;

    private bool isPlayerInteracting;
    private float timeStarted;
    private void Reset()
    {
        camBrain = GetComponent<Cinemachine.CinemachineBrain>();
    }

    private void Awake()
    {
        if(camBrain==null) camBrain = GetComponent<Cinemachine.CinemachineBrain>();
        camBrain.m_IgnoreTimeScale = true;
    }

    private void OnEnable()
    {
        GlobalEvents.OnPlayerInteracting.AddListener(OnPlayerInteract);
        isCoroutineRunning = false;
        timeStarted = Time.time;
    }

    private void OnDisable()
    {
        GlobalEvents.OnPlayerInteracting.RemoveListener(OnPlayerInteract);
        StopCoroutine(WhileSwitching());
        isCoroutineRunning = false;
        Time.timeScale = 1;
    }
    
    private void OnPlayerInteract(bool _isInteracting) => isPlayerInteracting = _isInteracting;

    private void Update()
    {
        if(Time.time < timeStarted + 0.5f) return;
        if(isPlayerInteracting) return;
        if(!camBrain.ActiveVirtualCamera.IsValid) return;
        

        //check if cam has changed
        if(camBrain.
               ActiveVirtualCamera.
               VirtualCameraGameObject.
               GetInstanceID() == prevCamId) return;
        
        // camera switched
        prevCamId = camBrain.
                    ActiveVirtualCamera.
                    VirtualCameraGameObject.
                    GetInstanceID();

        StartCoroutine(WhileSwitching());
    }

    IEnumerator WhileSwitching()
    {
        if(isCoroutineRunning) yield break;
        if(!camBrain.IsBlending) yield break;
        if(isPlayerInteracting) yield break;
        
        // switching Start
        isCoroutineRunning = true;
        
        prevTimeScale = Time.timeScale;
        Time.timeScale = 0;
        EventManager.GlobalEvents.OnCameraSwitchStart.Invoke(camBrain);
        
        yield return new WaitUntil(() => !camBrain.IsBlending || isPlayerInteracting);
        
        // switching End
        Time.timeScale = prevTimeScale;
        prevTimeScale = Time.timeScale;
        EventManager.GlobalEvents.OnCameraSwitchEnd.Invoke(camBrain);
        
        isCoroutineRunning = false;
    }
}
