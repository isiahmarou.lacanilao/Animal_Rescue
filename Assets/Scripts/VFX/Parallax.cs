using System.Collections;
using System.Collections.Generic;
using HelperClass;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float length, xPos, yPos, offset;
    private Transform cam;
    public float parallaxEffect;

    //Object nearer from player faster - Object farther from player slower
    //origin + (travel * parallax)
    
    /* Explanation
     * type name  - description - asset name - sprite layer - parallax effect
     * background - farthest    - highest    - lowest       - highest
     * midground  - farther     - higher     - lower        - higher
     * ground     - player      - normal     - normal       - normal
     * foreground - nearest     - lowest     - highest      - lowest / negative
    */



    /* Example
     * type name  - description - asset name - sprite layer - parallax effect
     * background - farthest    - 5          - 0            - 1
     * midground  - farther     - 4          - 1            - 0.7
     * midground  - farther     - 3          - 2            - 0.5
     * midground  - farther     - 2          - 3            - 0.3
     * ground     - player      - 1          - 4            - 0
     * foreground - nearest     - 0          - 5            - -0.3
    */


    // Start is called before the first frame update
    void Start()
    {
        xPos = transform.position.x;
        yPos = transform.position.y;
        length = GetComponent<SpriteRenderer>().bounds.size.x;

        cam = gameObject.scene.GetFirstMainCameraInScene().transform;

    }

    // Update is called once per frame
    void Update()
    {
        //Null check
        if (cam == null) return;   

        float temp = (cam.position.x * (1 - parallaxEffect));
        float xDist = (cam.position.x * parallaxEffect);
        float yDist = (cam.position.y * parallaxEffect);

        transform.position = new Vector3(xPos + xDist, yPos + yDist, transform.position.z);
        if (temp > xPos + (length - offset))
        {
            xPos += length;

        }
        else if (temp < xPos - (length - offset))
        {
            xPos -= length;
        }



    }
}
