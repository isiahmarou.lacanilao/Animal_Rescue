using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_Outline : InteractableEffects
{
    [SerializeField] private SpriteRenderer spriteToOutline;
    [SerializeField] private Material outlineMaterial;
    [Range(0,1f)][SerializeField] private float thickness = 0.7f;

    private Material defaultMaterial;
    private int thickness_ID;
    private void Awake()
    {
        if (spriteToOutline == null)
        {
            if (TryGetComponent(out spriteToOutline)) return;
            spriteToOutline = GetComponentInChildren<SpriteRenderer>();
        }

        defaultMaterial = spriteToOutline.material;
        
        outlineMaterial.SetFloat("_OutlineThickness", thickness);
    }
    
    protected override void Effect(bool isNear)
    {
        base.Effect(isNear);
        
        if(isNear) outlineMaterial.SetFloat("_OutlineThickness", thickness);
        spriteToOutline.material = isNear ? outlineMaterial : defaultMaterial;
    }
}
