using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableEffects : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.GlobalEvents.OnPlayerNearInteractable.AddListener(OnNear);
    }

    private void OnDisable()
    {
        EventManager.GlobalEvents.OnPlayerNearInteractable.RemoveListener(OnNear);
    }

    void OnNear(bool isNear, GameObject interactableObject)
    {
        if(interactableObject != gameObject) return;
        Effect(isNear);
    }
    
    protected virtual void Effect(bool isNear) {}
}
