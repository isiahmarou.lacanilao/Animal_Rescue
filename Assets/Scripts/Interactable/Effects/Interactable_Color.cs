using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable_Color : InteractableEffects
{
    [SerializeField] private SpriteRenderer spriteToChangeColor;
    [SerializeField] private Color spriteColor = Color.white;
    [SerializeField] private bool willOutline = true;
    
    private Color defaultSpriteColor;
    private void Awake()
    {
        if (spriteToChangeColor == null)
        {
            if (TryGetComponent(out spriteToChangeColor)) return;
            spriteToChangeColor = GetComponentInChildren<SpriteRenderer>();
        }

        defaultSpriteColor = spriteToChangeColor.color;
    }
    
    protected override void Effect(bool isNear)
    {
        base.Effect(isNear);
        spriteToChangeColor.color = isNear ? spriteColor : defaultSpriteColor;
    }
}
