using PersistentManager;
using UnityEngine;

namespace Interactable
{
    public class SceneLoader_Interactable : InteractableBase
    {
        [SerializeField] private LoaderType type = LoaderType.LoadSingle;
        
        [Header("Only for loading a scene, Can be null if unloading or reloading")] 
        [SerializeField] private string sceneName;

        //[Scene] public string ads;
        public override void OnInteract()
        {
            base.OnInteract();
            if(type != LoaderType.LoadAdditive && type != LoaderType.LoadSingle)
            {
                sceneName = gameObject.scene.name;
            }
            
            EventManager.GlobalEvents.OnLoadScene.Invoke(sceneName, type);
        }
    }
}
