using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Interactable
{
    public class Ladder_Interactable : InteractableBase
    {
        [SerializeField] Transform endPoint;
        [SerializeField] private bool isGoingUp;
        [FormerlySerializedAs("speed")] [SerializeField] private float speedModifier;
        [SerializeField] private bool isLadder = true;
        

        public Transform EndPoint => endPoint;
        public bool IsGoingUp => isGoingUp;
        public float SpeedModifier => speedModifier;
        public bool IsLadder => isLadder;
        public override void OnInteract()
        {
            EventManager.GlobalEvents.OnPlayerClimbLadder.Invoke(this);
        }
    }
}
