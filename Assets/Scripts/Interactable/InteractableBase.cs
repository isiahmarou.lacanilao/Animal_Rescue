using UnityEngine;

namespace Interactable
{
    public class InteractableBase : MonoBehaviour
    {
        public virtual void OnInteract()
        {
        
        }
    }
}
