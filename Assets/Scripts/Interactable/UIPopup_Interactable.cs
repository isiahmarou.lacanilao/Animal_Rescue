using UnityEngine;


namespace Interactable
{
    public class UIPopup_Interactable : InteractableBase
    {
        [SerializeField] private GameObject uiToPop;
        public override void OnInteract()
        {
            base.OnInteract();
            uiToPop.SetActive(true);
        }
    }
}