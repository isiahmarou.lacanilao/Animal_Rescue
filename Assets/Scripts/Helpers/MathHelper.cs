using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HelperClass
{
    public static class MathHelpers
    {

        public static float ConvertSpeedToTime(Vector2 p_targetSize, Vector2 p_currentSize, float p_speed)
        {
            return (Vector3.Distance(p_targetSize, p_currentSize)) / p_speed;
        }
        public static int FlattenTheNumber(int p_digits, int p_multipleOf = 10)
        {
            int remainder = p_digits % p_multipleOf;
            if (remainder * 2 >= p_multipleOf)
            {
                // Round up
                return p_digits - remainder + p_multipleOf;
            }
            else
            {
                // Round down
                return p_digits - remainder;
            }
        }



        public static float AddAll(WeightedFloatDatas list)
        {
            float total = 0;
            for (int i = 0; i < list.weightedFloat.Length; i++)
            {
                if (list.weightedFloat[i].chance > 0)
                {
                    total += list.weightedFloat[i].chance;
                }

            }
            return total;
        }
        
        public static float ChooseWeightedFloat(WeightedFloatDatas list, float p_lastValue = 0)
        {
            if (p_lastValue != 0)
            {
                while (true)
                {
                    float currentTargetSize = Roll(list);
                    if (currentTargetSize != p_lastValue)
                    {
                        return currentTargetSize;
                    }
                }
            }
            else
            {
                return Roll(list);
            }
        }

        public static float Roll(WeightedFloatDatas list)
        {
            float currentTargetSize = 0;
            float currentRolledChance = UnityEngine.Random.Range(0, list.maxChance);
            float currentCount = 0;
            float defaultTargetSize = list.weightedFloat[0].value;

            for (int i = 0; i < list.weightedFloat.Length; i++)
            {
                if (currentCount < currentRolledChance)
                {
                    currentCount += list.weightedFloat[i].chance;
                }
                else
                {
                    return currentTargetSize = list.weightedFloat[i].value;
                }
            }
            return defaultTargetSize;


        }

        public static bool IsApproximatelyTo(this float a, float b)
        {
            return Mathf.Approximately(a, b);
        }
    
        
        public static bool IsLayerInLayerMask(int layer, LayerMask mask)
        {
            return ((1 << layer) & mask) != 0;
        }


    }
}
