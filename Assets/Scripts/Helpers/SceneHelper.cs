using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

namespace HelperClass
{
    public static class SceneHelper
    {
        /// <summary>
        /// returns all given component in a specific scene
        /// Very expensive method. only use for caching
        /// </summary>
        /// <param name="includeInactive"></param>
        /// <param name="scene"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> FindComponentInScene<T>(this Scene scene, bool includeInactive)
        {
            List<T> results = new List<T>();
        
            foreach (GameObject root in scene.GetRootGameObjects())
            {
                results.AddRange(root.GetComponents<T>());
                results.AddRange(root.GetComponentsInChildren<T>(includeInactive));
            }

            return results;
        }
    
        /// <summary>
        /// returns first found component in a specific scene
        /// Very expensive method. only use for caching
        /// </summary>
        /// <param name="includeInactive"></param>
        /// <param name="scene"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T FindFirstComponentInScene<T>(this Scene scene, bool includeInactive)
        {
            return scene.FindComponentInScene<T>(includeInactive).FirstOrDefault();
        }
    }
}
