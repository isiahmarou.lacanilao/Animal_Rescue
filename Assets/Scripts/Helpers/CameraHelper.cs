using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HelperClass
{
    public static class CameraHelpers
    {
        /// <summary>
        /// Get All main cameras in a given scene
        /// Expensive method. only use for caching
        /// </summary>
        /// <param name="includeInActive"></param>
        /// <returns></returns>
        public static List<Camera> GetAllMainCamerasInScene(this Scene scene, bool includeInActive)
        {
            return includeInActive ? 
                Camera.allCameras.Where(c => c.gameObject.scene == scene && c.gameObject.CompareTag("MainCamera")).ToList() : 
                Camera.allCameras.Where(c =>
                {
                    GameObject gameObject;
                    return (gameObject = c.gameObject).scene == scene && gameObject.activeSelf && c.gameObject.CompareTag("MainCamera");
                }).ToList();
        }
        
        /// <summary>
        /// Get first found main camera in a given scene
        /// Expensive method. only use for caching
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="includeInActive"></param>
        /// <returns></returns>
        public static Camera GetFirstMainCameraInScene(this Scene scene, bool includeInActive = true)
        {
            return scene.GetAllMainCamerasInScene(includeInActive).FirstOrDefault();
        }

        private static Camera _camera;
        public static Camera Camera
        {
            get
            {
                if (_camera == null) _camera = Camera.main;
                return _camera;
            }

        }
        
        public static Vector3 MouseWorldPosition(Vector3 p_position)
        {
            var mouseScreenPos = Input.mousePosition;
            mouseScreenPos.z = Camera.main.WorldToScreenPoint(p_position).z;
            return Camera.main.ScreenToWorldPoint(mouseScreenPos);
        }
        
        public static IEnumerator Co_Shake(Vector2 p_origin, float shakePositionDuration =0f, float shakePositionPower = 0f, int shakePositionVibrato = 0, float shakePositionRandomRange = 0f, bool shakePositionCanFade =false)
        {
            //INDIVIDUAL SHAKE
            var shakeSequence = DOTween.Sequence()
                .Append(Camera.DOShakePosition(shakePositionDuration, shakePositionPower, shakePositionVibrato, shakePositionRandomRange, shakePositionCanFade));

            shakeSequence.Play();
            yield return shakeSequence.WaitForCompletion();
            Camera.transform.position = p_origin;
            //GROUP SHAKE
            //var sequence = DOTween.Sequence()
            //    .Append(rectTransforms[0].DOShakePosition(shakePositionDuration, shakePositionPower, shakePositionVibrato, shakePositionRandomRange, shakePositionCanFade));
            //for (int i = 1; i < rectTransforms.Count; i++)
            //{
            //    sequence.Join(rectTransforms[i].DOShakePosition(shakeRotationDuration, shakeRotationPower, shakeRotationVibrato, shakeRotationRandomRange, shakeRotationCanFade));
            //}
            //sequence.Play();
            //yield return sequence.WaitForCompletion();
            //for (int i = 0; i < rectTransforms.Count; i++)
            //{
            //    rectTransforms[i].anchoredPosition = savedPositions[i];
            //}
         

        }
        
        public static IEnumerator Co_ZoomCamera(Camera p_cam, float p_zoomSizeModifier, float p_zoomTime)
        {
            var zoomInSequence = DOTween.Sequence()
        .Append(p_cam.DOOrthoSize(p_cam.orthographicSize - p_zoomSizeModifier, p_zoomTime));
            zoomInSequence.Play();
            yield return zoomInSequence.WaitForCompletion();
        }

        public static IEnumerator Co_MoveForwardCamera(Transform p_objectToMove,
            Vector2 previousPosition, Vector2 targetPosition,
            float targetMoveUpPosition = 0.75f, float moveUpTime = 0.35f, float moveUpHoverTime = 0.05f,
            float targetMoveDownPosition = -0.25f, float moveDownTime = 0.45f, float moveDownHoverTime = 0.05f, float moveResetTime = 0.15f)
        {
            var moveUpSequence = DOTween.Sequence()
                    .Append(p_objectToMove.DOMove(previousPosition + new Vector2(0, targetMoveUpPosition) + targetPosition / 2, moveUpTime));
            moveUpSequence.Play();
            yield return moveUpSequence.WaitForCompletion();
            yield return new WaitForSeconds(moveUpHoverTime);


            var moveDownSequence = DOTween.Sequence()

            .Append(p_objectToMove.DOMove(targetPosition + new Vector2(0, targetMoveDownPosition), moveDownTime));
            moveDownSequence.Play();
            yield return moveDownSequence.WaitForCompletion();
            yield return new WaitForSeconds(moveDownHoverTime);

            var moveResetSequence = DOTween.Sequence()
                .Append(p_objectToMove.DOMove(targetPosition, moveResetTime));
            moveResetSequence.Play();
            yield return moveResetSequence.WaitForCompletion();

            p_objectToMove.position = targetPosition;
        }
    }
}
