using System.Collections.Generic;
using System.Linq;

namespace HelperClass
{
    [System.Serializable]
    public class WeightedList<T>
    {
        [System.Serializable]
        public class WeightedItem<T>
        {
            public WeightedItem(T _item, float _weight)
            {
                Item = _item;
                Weight = _weight;
            }

            public T Item;
            public float Weight;
        }

        [UnityEngine.SerializeField] private List<WeightedItem<T>> itemList = new List<WeightedItem<T>>();

        private Dictionary<float, T> fixedChances;

        private System.Random rand;

        private bool hasInitialized = false;
        private float totalWeight;

        private T itemWithLargestWeight;

        public void Initialize()
        {
            if (hasInitialized) return;

            rand = new System.Random();
            fixedChances = new Dictionary<float, T>();
            RecalculateChances();

            hasInitialized = true;
        }

        public void RecalculateChances()
        {
            if (fixedChances == null) fixedChances = new Dictionary<float, T>();
            else fixedChances.Clear();

            float largestWeight = 0;
            foreach (WeightedItem<T> w in itemList)
            {
                if (w.Weight > largestWeight) itemWithLargestWeight = w.Item;

                totalWeight += w.Weight;
                fixedChances.Add(totalWeight, w.Item);

                // UnityEngine.Debug.Log($"Value: {w.Item} Weight: {totalWeight}");
            }
            
            // UnityEngine.Debug.Log($"total weight: {totalWeight}");
        }

        public T GetWeightedRandom()
        {
            // init class if it hasn't already
            Initialize();
            
            // check if the dictionary size the item list count is the same. recalculate chances if it is not 
            if(fixedChances.Count != itemList.Count) RecalculateChances();

            // early returns
            if (fixedChances.Count == 0) return default;

            // random number
            float rngVal = ((float)rand.NextDouble() * totalWeight);

            // UnityEngine.Debug.Log($"Has Init: {hasInitialized}");
            // UnityEngine.Debug.Log($"RNG: {rngVal}");

            foreach (KeyValuePair<float, T> w in fixedChances)
            {
                if (w.Key > rngVal)
                {
                    // UnityEngine.Debug.Log($"Return: {w.Value}");
                    return w.Value;
                }
            }

            // fallback option, returns item with largest weight. this should not happen tho
            return itemWithLargestWeight;
        }

        public void AddItem(WeightedItem<T> itemToAdd)
        {
            itemList.Add(itemToAdd);
            fixedChances.Add(itemToAdd.Weight + totalWeight , itemToAdd.Item);
        }

        public void AddItem(T item, int weight)
        {
            WeightedItem<T> weightedItem = new WeightedItem<T>(item, weight);
            AddItem(weightedItem);
        }

        public void AddItems(IEnumerable<WeightedItem<T>> collectionToAdd)
        {
            itemList.AddRange(collectionToAdd);
            RecalculateChances();
        }

        public void RemoveItem(WeightedItem<T> itemToRemove)
        {
            itemList.Remove(itemToRemove);
            RecalculateChances();
        }

        public void RemoveItem(T itemToRemove)
        {
            WeightedItem<T> wcToRemove = itemList.FirstOrDefault(w => w.Item.Equals(itemToRemove));

            RemoveItem(wcToRemove);
        }

        public void RemoveItems(IEnumerable<WeightedItem<T>> collectionToRemove)
        {
            foreach (WeightedItem<T> w in collectionToRemove)
            {
                itemList.Remove(w);
            }

            RecalculateChances();
        }

        public float GetWeightOfItem(T itemToCompare)
        {
            return itemList.
                FirstOrDefault(w => w.Item.Equals(itemToCompare)).
                Weight;
        }
        
        public bool TryGetWeightOfItem(T itemToCompare, out float weight)
        {
            weight = 0;
            WeightedItem<T> wc = itemList.FirstOrDefault(w => w.Item.Equals(itemToCompare));

            if (wc == null) return false;

            weight = wc.Weight;
            return true;
        }

        public bool Contains(T item)
        {
            WeightedItem<T> wc = itemList.FirstOrDefault(w => w.Item.Equals(item));

            return wc != null;
        }

        public List<T> GetCopyOfItems()
        {
            return itemList.Select(i => i.Item).ToList();
        }
    }
}

