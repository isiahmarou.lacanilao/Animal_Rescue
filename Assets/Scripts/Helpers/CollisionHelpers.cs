using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HelperClass
{
    public static class CollisionHelpers
    {
        public static bool CheckRayHit(Vector2 position, Vector2 direction, string p_destinationTag = "Untagged",
            string p_returnTag = "Untagged")
        {
            bool cache = false;
            Collider2D[] hitInfo = Physics2D.OverlapBoxAll(position, direction, 1f);
            Debug.Log(hitInfo.Length);
            for (int i = 0; i < hitInfo.Length; i++)
            {
                Debug.Log(i);
                if (hitInfo[i])
                {
                    Debug.Log(hitInfo[i].gameObject.name + " - " + hitInfo[i].gameObject.tag);

                    if (hitInfo[i].transform.tag == p_destinationTag)
                    {
                        Debug.Log("TRUE " + p_destinationTag);
                        cache = true;

                        //transform.position = hitInfo.transform.position;
                    }

                    if (p_returnTag != "Untagged")
                    {
                        // Debug.Log("UNTAG " + p_returnTag);
                        if (hitInfo[i].transform.tag == p_returnTag)
                        {
                            // Debug.Log("FALSE " + p_returnTag);
                            cache = false;
                            break;
                            //transform.position = hitInfo.transform.position;
                        }
                    }
                }
            }

            if (cache)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckCircleHit(Vector2 position, float p_radius, string p_destinationTag = "Untagged",
            string p_returnTag = "Untagged")
        {
            Collider2D[] hitInfo = Physics2D.OverlapCircleAll(position, p_radius);
            Debug.Log(hitInfo.Length);
            for (int i = 0; i < hitInfo.Length; i++)
            {
                Debug.Log(i);
                if (hitInfo[i])
                {
                    Debug.Log(hitInfo[i].gameObject.name + " - " + hitInfo[i].gameObject.tag);


                    if (p_returnTag != "Untagged")
                    {
                        if (hitInfo[i].transform.tag == p_returnTag)
                        {
                            return false;

                            //transform.position = hitInfo.transform.position;
                        }
                    }

                    if (hitInfo[i].transform.tag == p_destinationTag)
                    {
                        return true;

                        //transform.position = hitInfo.transform.position;
                    }
                }
            }

            return false;
        }
    }
}
