using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityGetter : MonoBehaviour
{
    public Vector3 velocity;
    private Vector3 lastPosition;
    private Transform _transform => transform;
    
    private void Update()
    {
        if (!_transform.hasChanged) return;
        
        velocity = (transform.position - lastPosition) / Time.deltaTime;
        lastPosition = _transform.position;
    }
}
