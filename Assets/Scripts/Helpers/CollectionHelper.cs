using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace HelperClass
{
    public static class CollectionHelper
    {
        #region Random Generic

        public static T GetRandomFromArray<T>(this T[] array)
        {
            float rng = Random.Range(0, float.MaxValue) / float.MaxValue;
            int i = Mathf.RoundToInt(rng * (array.Length - 1));
            return array[i];
        }
        
        
        public static T GetRandomFromList<T>(this List<T> list)
        {
            return list.ToArray().GetRandomFromArray();
        }

        public static T GetRandom<T>(this IEnumerable<T> collection)
        {
            return GetRandomFromArray(collection.ToArray());
        }
        
        #endregion

        #region Destroy Collection

        
        public static void DestroyGameObjects(this GameObject[] arr)
        {
            for ( int i = arr.Length - 1; i > -1; --i)
            {
                Object.Destroy(arr[i]);
            }
        }

        public static void DestroyGameObjects(this List<GameObject> list)
        {
            for ( int i = list.Count - 1; i > -1; --i)
            {
                Object.Destroy(list[i]);
                list.RemoveAt(i);
            }
            list.Clear();
        }

        public static void DestroyGameObjects(this List<Component> list)
        {
            for ( int i = list.Count - 1; i > -1; --i)
            {
                Object.Destroy(list[i].gameObject);
                list.RemoveAt(i);
            }
            list.Clear();
        }
        
        public static void DestroyGameObjects(this Component[] list)
        {
            for ( int i = list.Length - 1; i > -1; --i)
            {
                Object.Destroy(list[i].gameObject);
            }
        }
        
        public static void DeleteChildren(this Transform t)
        {
            foreach (Transform child in t) Object.Destroy(child.gameObject);
        }

        #endregion
    }
}