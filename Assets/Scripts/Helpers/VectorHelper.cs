using UnityEngine;

public static class VectorHelper
{
    public static Vector3 Add_X(this Vector3 position, float value)
    {
        return new Vector3(position.x + value, position.y, position.z);
    }
    
    public static Vector3 Add_Y(this Vector3 position, float value)
    {
        return new Vector3(position.x, position.y + value, position.z);
    }
    
    public static Vector2 Add_X(this Vector2 position, float value)
    {
        return new Vector2(position.x + value, position.y);
    }
    
    public static Vector2 Add_Y(this Vector2 position, float value)
    {
        return new Vector2(position.x, position.y + value);
    }
    
    public static Vector3 Replace_X(this Vector3 position, float value)
    {
        return new Vector3(value, position.y, position.z);
    }
    
    public static Vector3 Replace_Y(this Vector3 position, float value)
    {
        return new Vector3(position.x, value, position.z);
    }
    
    public static Vector2 Replace_X(this Vector2 position, float value)
    {
        return new Vector2(value, position.y);
    }
    
    public static Vector2 Replace_Y(this Vector2 position, float value)
    {
        return new Vector2(position.x, value);
    }

    public static Vector3 Add(this Vector3 position, float x_ToAdd, float y_ToAdd, float z_ToAdd = 0)
    {
        return new Vector3(position.x + x_ToAdd, position.y + y_ToAdd, position.z + z_ToAdd);
    }
    
    public static Vector2 Add(this Vector2 position, float x_ToAdd, float y_ToAdd)
    {
        return new Vector2(position.x + x_ToAdd, position.y + y_ToAdd);
    }

    public static Vector3 Add(this Vector3 position, float toAdd)
    {
        return new Vector3(position.x + toAdd, position.y + toAdd, position.z + toAdd);
    }

    public static Vector3 Replace_Z(this Vector3 position, float value)
    {
        return new Vector3(position.x, position.y, value);
    }
    
    public static Vector3 Replace_Z(this Vector2 position, float value)
    {
        return new Vector3(position.x, position.y, value);
    }
}
