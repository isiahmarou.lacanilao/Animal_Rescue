using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using HelperClass;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public static class UIHelpers
    {
        private static PointerEventData _eventDataCurrentPosition;
        
        private static List<RaycastResult> _results;
        
        public static bool IsOverUI()
        {
            _eventDataCurrentPosition = new PointerEventData(EventSystem.current) { position = Input.mousePosition };
            _results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(_eventDataCurrentPosition, _results);
            return _results.Count > 0;
        }

        public static IEnumerator Co_SlotMachineTextEffect(TMP_Text p_modifyText, int p_newValue, float p_duration, string p_format = "", int p_Value = 0, int p_fps = 60)
        {
            WaitForSeconds wait = new WaitForSeconds(1f / p_fps);
            int previousValue = p_Value;
            int stepAmount;
            if (p_newValue - previousValue < 0)
            {
                stepAmount = Mathf.FloorToInt((p_newValue - previousValue) / (p_fps * p_duration));
            }
            else
            {
                stepAmount = Mathf.CeilToInt((p_newValue - previousValue) / (p_fps * p_duration));

            }

            if (previousValue < p_newValue)
            {
                //Back up counter

                while (previousValue < p_newValue)
                {
                    previousValue += stepAmount;
                    if (previousValue > p_newValue)
                    {
                        previousValue = p_newValue;
                    }

                    //Update Text to new Value
                    p_modifyText.text = previousValue.ToString(p_format);

                    yield return wait;

                }

            }
            else if (previousValue > p_newValue)
            {
                while (previousValue > p_newValue)
                {
                    previousValue += stepAmount;
                    if (previousValue < p_newValue)
                    {
                        previousValue = p_newValue;
                    }

                    //Update Text to new Value
                    p_modifyText.text = previousValue.ToString(p_format);

                    yield return wait;

                }

            }
            else if (previousValue == p_newValue)
            {
                p_modifyText.text = previousValue.ToString(p_format);
            }
        }
        public static IEnumerator Co_BouncingExpandTextEffect(RectTransform p_textRT, float p_expandOffset = 0.25f, float p_sizeTweenSpeed = 0.25f, float p_duration = 0.2f)
        {

            Vector3 originalScale = p_textRT.localScale;
            Vector3 modifiedScale;
            modifiedScale = new Vector3(originalScale.x + p_expandOffset, originalScale.y + p_expandOffset, originalScale.z + p_expandOffset);
            p_textRT.DORewind();
            var pulseInNOutSequence = DOTween.Sequence();
            pulseInNOutSequence.Append(p_textRT.DOScale(modifiedScale, p_sizeTweenSpeed).SetEase(Ease.Linear));
            pulseInNOutSequence.Append(p_textRT.DOScale(originalScale, p_duration).SetEase(Ease.Linear));
            pulseInNOutSequence.Play();
            yield return pulseInNOutSequence.WaitForCompletion();
        }
        
        /// <summary>
        /// returns world size base on the screen and camera size
        /// </summary>
        /// <param name="cam"></param>
        /// <returns></returns>
        public static Vector2 GetScreenWorldSize(this Camera cam)
        {
            float aspect = (float)Screen.width / Screen.height;
            
            float worldHeight = cam.orthographicSize * 2;
            
            float worldWidth = worldHeight * aspect;

            return new Vector2(worldWidth, worldHeight);
        }
        
        /// <summary>
        /// returns world size base on the screen and camera size
        /// </summary>
        /// <param name="scene"></param>
        /// <returns></returns>
        public static Vector2 GetScreenWorldSize(this Scene scene)
        {
            Camera cam = scene.GetFirstMainCameraInScene();

            return GetScreenWorldSize(cam);
        }

        /// <summary>
        /// returns a world space position (Vector 2) from a given position of a canvas and camera
        /// </summary>
        /// <param name="element"></param>
        /// <param name="cam"></param>
        /// <returns></returns>
        public static Vector2 GetWorldPositionOfCanvasElement(this Camera cam, RectTransform element)
        {
            RectTransformUtility.ScreenPointToWorldPointInRectangle(
                element, 
                element.position, 
                cam,
                out var result);
            return result;
        }
    }
